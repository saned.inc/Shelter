﻿/// <reference path="../js/jquery-2.1.0.js" />
/// <reference path="../js/framework7.js" />


var Confirm = 'تم';
var Cancel = 'إلغاء';


if (localStorage.getItem('Language') == 'en') {
    Confirm = 'Ok';
    Cancel = 'Cancel';
}

// Initialize your app
var myApp = new Framework7({
    pushState: true,
    swipeBackPage: false,
    swipePanel: false,
    panelsCloseByOutside: true,
    init: false,
    animateNavBackIcon: true,
    modalButtonOk: Confirm,
    modalButtonCancel: Cancel
});

// Export selectors engine
var $$ = Dom7;
var initSideMenu = true;
var popupTitle = "";
var serviceURL = 'http://api.rfq-sa.com/';
//var serviceURL = 'http://shelterapi.saned-projects.com/';
//'http://localhost:15165/';
//

var hostUrl = 'http://api.rfq-sa.com/';

var userId = localStorage.getItem("UserId");
var CurrentLanguage = localStorage.getItem('Language');
var PageNameAddanmial;
var videoMedia;
var initloginPage = true;
var initsignupPage = true;
var initactivation = true;
var initIndexPage = true;
var initProfilePage = true;
var initAnimalDetailsPage = true;
var initrefugePage = true;
var initrefugeDetails = true;
var initvolunteer = true;
var initusers = true;
var inituserDetails = true;
var initaddAnimal = true;
var initlostAnimals = true;
var initneedCareAnimals = true;
var initsuccessStory = true;
var initStoryDetailsPage = true;
var initmapPage = true;
var initChat = true;
var initmessages = true;
var initforgetPass = true;
var initchangePassword = true;
var initresetPassword = true;
var initAdoptPopupPage = true;
var initEditProfilePopup = true;
var initreportPopupPage = true;
var initLeaveReques = true;
var initadoptedRequestPopup = true;
var initcommentPopup = true;
var initSearch = true;
var initaddSuccessStory = true;
var initNotificationPage = true;
var initAdoptRequestPopupPage = true;
var initsearchResultPage = true;
var HomeTitle;
var SearchResultTitle;
var AnimalCategory;
var MaleGender;
var FemaleGender;
var City;
var DefultGender;
var AdaptionRequestsTitle;
var AbandonRequestsTitle;
var ReportsTitle;
var ddlvolunteerForTxt;
var ddlvolunteerTypeTxt;
var GiveUpForTxt;
var AddAnimalTitleEmergancy;
var AddAnimalTitleAboption;
var AddAnimalTitleLost;
var AddAnimalReportBtn;
var AddAnimalsubmitBtn;
var loaderMSg;
var nO;
var YES;
var scrollLoadsBefore = false;
var animalAdopImages = [];
var animalType;
var UserAnimalTypeId;
var ShelterId;
var reporteduser;
var AdoptionRequestAnimal;
var AnimalMobile;
var AnimalIdComent;
var StoryIdComent;
var animalMassageid;
var UserMassageid;
var chatId;
var clientId = '';
var clientSecret = '';

var searchCatId;
var animalName;
var ownerName;
var subscribeforShelter;
var UnsubscribeforShelter;
var PushUserId;
var Wrong;
var cantFindData;
var warning;
var LoginError;
var WrongActivationNo;
var ErrorOccured;
var Sucess;
var CodeSendSucess;
var CodeSend;
var PasswordChanged;
var NoNotAvilable;
var NoAge;
var notFound;
var NotAuthenticated;
var RequestAdded;
var storyAdded;
var FiledsEmpty;
var OneImageAtLeast;
var AnimalAdded;
var CommentAdded;
var WrongOldPassWord;
var pushAppId = "2b3580ec-1951-49ef-9d14-17a1219fa75e";
var notificationOpenedCallback;
var notificationReceivedCallback;
var closeTxt;
var ChoseImage;
var LargNoOfImage;
var UserMobile;
var NotAuthenticatedreport;
var noAnimalName;
var isSocial = localStorage.getItem("isSocial");
var animalShelterId;
var animalUserId;
var EmailExistError;
var PhoneExistError;
var UserNameExistError;
var PasswordError;
var ArabicUserNameError;
var AddAnimalEmergancy;
var AddAnimalAboption;
var AddAnimalLost;
var AddAnimalGiveup;
var PasswordEmptyError;
var VolunterAddedPefor;
var YouOWnAnimal;
var LoginDataError;
var MonthesLbl;
var Visitor;
var fristIndex = true;
var fromLogin = false;
var reqShelterMobile;
var reportValueRequired;
var ShowInfo = true;
var ShowInformation;
var currentUserId;
var UserShowInfoAnimal;
var UserShowInforUser;
// Add view
var mainView = myApp.addView('.view-main', {
    dynamicNavbar: true,
    domCache: true
});


$$(document).on('pageInit', function (e) {
    page = e.detail.page;
    setAppLanguage(CurrentLanguage);
    if (page.name === 'landing') {
        setTimeout(function () {
            if (localStorage.getItem('UserEntersCode') == "false") {
                mainView.router.loadPage({ pageName: 'activation' });
            }
            else if (localStorage.getItem('UserId') != null) {

                mainView.router.loadPage({ pageName: 'index' });
            }
            else {
                mainView.router.loadPage({ pageName: 'login' });
            }
            setAppLanguage(CurrentLanguage);
            var message = '';
            if (CurrentLanguage == 'ar')
                message = 'LandingMessage';
            else
                message = 'LandingMessageEn';
            CallService("", "GET", "/api/Setting/GetLandingMessage/" + message, "", function (res) {
                myApp.alert(res.Value, '', function () { });
            });

        }, 1000);


    }
    else if (page.name === 'index') {
        initIndexPage = true;
    }
    else if (page.name === 'profile') {
        initProfilePage = true;
    }
    else if (page.name === 'activation') {
        initactivation = true;
    }
    else if (page.name === 'login') {
        initloginPage = true;
    }
    else if (page.name === 'mapPage') {
        initmapPage = true;
    }
    else if (page.name === 'AddAnimal') {
        initaddAnimal = true;
    }
    else if (page.name === 'refuge') {
        initrefugePage = true;
    }
});


window.document.addEventListener('backbutton', function (event) {

    var currentPage = mainView.activePage.name;
    if (device.platform.indexOf('dr') > 0 && (currentPage == 'index' || currentPage == 'login')) {
        if (CurrentLanguage == "en") {
            //navigator.notification.confirm(
            //          'Do you want to exit the application?',
            //          onConfirmAr,
            //          'Confirm',
            //          ['Yes', 'Cancel']);
            myApp.confirm('Do you want to exit the application?', 'Confirm',
              function () {
                  if (device.platform.indexOf('dr') > 0)
                      navigator.app.exitApp();
              },
              function () {

              });

        } else {
            //navigator.notification.confirm(
            //  'هل تريد الخروج من التطبيق ؟',
            //  onConfirmAr,
            //  'تأكيد الخروج',
            //  ['نعم', 'إلغاء']);

            myApp.confirm('هل تريد الخروج من التطبيق ؟', 'تاكيد',
              function () {
                  if (device.platform.indexOf('dr') > 0)
                      navigator.app.exitApp();
              },
              function () {

              });
        }
    }
    else if (currentPage == 'addAnimal') {
        $$(".smart-select .item-after").html("");
        document.getElementById('imgContainerDiv').innerHTML = "";
        $('#txtAnimalAge').val('');
        $("#txtAnimalName").val('');
        $("#txtAnimalVaccine").val('');
        $("#divAnimalAddress").val('');
        $$("#selectedAnimalType").html(AnimalCategory);
        $$("#selectedAnimalCity").html(City);
        $$("#selectedAnimalGender").html(DefultGender);
        $$("#selectedLeaveAnimalType").html(AnimalCategory);
        $$("#selectedLeaveAnimalCity").html(City);
        $$("#seletedLeaveAnimalGender").html(DefultGender);
        $$("#seletedLeaveAnimalLleaveFor").html(GiveUpForTxt);
        mainView.router.back();
    }
    else if (currentPage == 'refugeDetails') {
        $$(".smart-select .item-after").html("");

        $('#txtLeaveAnimalName').val('');
        $('#txtLeaveAnimalAge').val('');
        $('#txtAnimalLeaveVaccine').val('');
        $('#txtAnimalLeaveReason').val('');
        $('#divLeaveAnimalAddress').val('');
        $('#TxtGiveUpLeave').val('');
        $('#txtLeaveAnimalDescription').val('');


        //$('#ddlLeaveAnimalType').val('');
        //$('#ddlLeaveAnimalCity').val('');
        //$('#ddlLeaveAnimalGender').val('');
        //$('#DDLleaveFor').val('');

        $$("#selectedLeaveAnimalType").html(AnimalCategory);
        $$("#selectedLeaveAnimalCity").html(City);
        $$("#seletedLeaveAnimalGender").html(DefultGender);
        $$("#seletedLeaveAnimalLleaveFor").html(GiveUpForTxt);
        $$("#selectedAnimalType").html(AnimalCategory);
        $$("#selectedAnimalCity").html(City);
        $$("#selectedAnimalGender").html(DefultGender);
        document.getElementById('imgLeaveAnimalContainerDiv').innerHTML = "";
        mainView.router.back();
    }
    else if (currentPage == 'addSuccessStory') {
        document.getElementById("SucessImgUploaded").src = '';
        $("#txt_SucessTitle").val('');
        $("#txt_SucessDetails").val('');
        mainView.router.back();
    }
    else if (currentPage == 'activation') {
        mainView.router.loadPage({ pageName: 'login' });
    }
    else {
        mainView.router.back();
    }
});
document.addEventListener("offline", onOffline, false);
function GoToLoginPage(page) {
    if (typeof page != 'undefined') {
        loadSideMenuLinks();
        fromLogin = true;
        if (initloginPage == true) {
            initloginPage = false;
            $("#btnLogin").on("click", function () {
                var loginEmail = $("#LoginEmail").val();
                var loginPass = $("#LoginPassword").val();
                loginEmail = loginEmail.trim();

                if ((loginEmail != '' && loginEmail != ' ' && loginEmail != null) && loginPass != null) {
                    GetToken('login', "POST", "token", loginEmail, loginPass, function (res) {
                        if (res != null) {
                            localStorage.setItem('appToken', res.access_token);
                            // localStorage.setItem('UserName', res.userName);
                            localStorage.setItem('refreshToken', res.refresh_token);
                            localStorage.setItem('userTypeId', res.AccountTypeId);
                            localStorage.setItem('UserEntersCode', true);
                            localStorage.setItem('Visitor', false);
                            localStorage.setItem("isSocial", false);
                            window.plugins.OneSignal
                            .startInit(pushAppId)
                            .handleNotificationOpened(notificationOpenedCallback)
                            .handleNotificationReceived(notificationReceivedCallback)
                            .endInit();
                            window.plugins.OneSignal.getIds(function (ids) {
                                console.log('getIds: ' + JSON.stringify(ids));

                                var obj = { "DeviceId": ids.userId, "IsOnline": true };
                                CallService('', "POST", "/api/NotificationSetting/SaveDeviceSetting/", obj, function (res) {
                                    if (res != null) {
                                        if (typeof res.responseText != 'undefined' && typeof res.responseText != undefined) {
                                        } else {
                                        }

                                    }
                                });
                            });
                            CallService('login', "POST", "/api/Account/GetUserInfo/", "", function (res) {
                                if (res != null) {
                                    if (typeof res.responseText == 'undefined' && typeof res.status == 'undefined') {

                                        localStorage.setItem('UserId', res.Id);
                                        localStorage.setItem('userLoggedIn', JSON.stringify(res));

                                        mainView.router.loadPage({ pageName: 'index' });


                                    } else if (res.status == 403) {
                                        myApp.alert(cantFindData, Wrong, function () { $("#LoginPassword").val(""); });
                                    } else {
                                        myApp.alert(cantFindData, Wrong, function () { $("#LoginPassword").val(""); });
                                    }
                                }
                            });
                            $("#LoginEmail").val("");
                            $("#LoginPassword").val("");
                        }
                        else if (res.status == 403) {
                            myApp.alert(cantFindData, Wrong, function () { $("#LoginPassword").val(""); });
                        }
                        else {
                            myApp.alert(cantFindData, Wrong, function () { $("#LoginPassword").val(""); });
                        }
                    });
                }
                else {
                    myApp.alert(LoginError, warning, function () { });
                }
            });


            $('#btnSignup').on("click", function () {
                mainView.router.loadPage({ pageName: 'signup' });
            });

            $('#btnSkip').on("click", function () {
                mainView.router.loadPage({ pageName: 'index' });
            });

            $('#btnFb').on("click", function () {

                //facebookConnectPlugin.getLoginStatus(function (response) {
                //    if (response.status === "connected") {
                //        var param = { "Provider": "Facebook", "ExternalAccessToken": response.authResponse.accessToken, "userId": response.authResponse.userID }
                //        CallService('login', "POST", "/api/Account/RegisterExternal", param, function (res) {
                //            if (res != null) {
                //                localStorage.setItem('appToken', res.access_token);
                //                localStorage.setItem('refreshToken', res.refresh_token);
                //                localStorage.setItem('userTypeId', res.AccountTypeId);
                //                localStorage.setItem('Visitor', false);
                //                window.plugins.OneSignal
                //                .startInit(pushAppId)
                //                .handleNotificationOpened(notificationOpenedCallback)
                //                .handleNotificationReceived(notificationReceivedCallback)
                //                .endInit();
                //                window.plugins.OneSignal.getIds(function (ids) {
                //                    console.log('getIds: ' + JSON.stringify(ids));
                //                    var obj = { "DeviceId": ids.userId, "IsOnline": true };
                //                    CallService('', "POST", "/api/NotificationSetting/SaveDeviceSetting/", obj, function (res) {
                //                        if (res != null) {
                //                            if (typeof res.responseText != 'undefined' && typeof res.responseText != undefined) {
                //                            } else {
                //                            }
                //                        }
                //                    });
                //                });
                //                CallService('login', "POST", "/api/Account/GetUserInfo/", "", function (res) {
                //                    if (res != null) {
                //                        if (typeof res.responseText != 'undefined' && typeof res.responseText != undefined) {
                //                            mainView.router.loadPage({ pageName: 'index' });
                //                        } else {
                //                            localStorage.setItem('UserId', res.Id);
                //                            localStorage.setItem("isSocial", true);
                //                            localStorage.setItem('userLoggedIn', JSON.stringify(res));
                //                            mainView.router.loadPage({ pageName: 'index' });
                //                        }
                //                    }
                //                });
                //            }
                //        });
                //    } else {

                //    }
                //});
                var fbLoginFailed = function (error) {

                    //  alert(ErrorOccured);
                }
                var fbLoginSuccess = function (userData) {

                    if (userData.authResponse) {


                        facebookConnectPlugin.api(userData.authResponse.userID + "/?fields=id,email,first_name,last_name", ["public_profile"], function (result) {
                            userFullName = result.first_name + ' ' + result.last_name;
                            if (result.error) {

                                alert(result.error);
                            } else {
                                var param = { "Provider": "Facebook", "ExternalAccessToken": userData.authResponse.accessToken, "userId": userData.authResponse.userID, "name": userFullName }
                                CallService('login', "POST", "/api/Account/RegisterExternal", param, function (res) {
                                    if (res != null) {

                                        localStorage.setItem('appToken', res.access_token);
                                        localStorage.setItem('refreshToken', res.refresh_token);
                                        localStorage.setItem('userTypeId', res.AccountTypeId);
                                        localStorage.setItem('Visitor', false);
                                        localStorage.setItem("isSocial", true);

                                        window.plugins.OneSignal
                                        .startInit(pushAppId)
                                        .handleNotificationOpened(notificationOpenedCallback)
                                        .handleNotificationReceived(notificationReceivedCallback)
                                        .endInit();
                                        window.plugins.OneSignal.getIds(function (ids) {
                                            console.log('getIds: ' + JSON.stringify(ids));

                                            var obj = { "DeviceId": ids.userId, "IsOnline": true };
                                            CallService('', "POST", "/api/NotificationSetting/SaveDeviceSetting/", obj, function (res) {
                                                if (res != null) {
                                                    if (typeof res.responseText != 'undefined' && typeof res.responseText != undefined) {
                                                    } else {
                                                    }

                                                }
                                            });
                                        });
                                        CallService('login', "POST", "/api/Account/GetUserInfo/", "", function (res) {
                                            if (res != null) {
                                                if (typeof res.responseText != 'undefined' && typeof res.responseText != undefined) {
                                                    mainView.router.loadPage({ pageName: 'index' });

                                                } else {
                                                    localStorage.setItem('UserId', res.Id);
                                                    localStorage.setItem('userLoggedIn', JSON.stringify(res));
                                                    mainView.router.loadPage({ pageName: 'index' });

                                                }

                                            }
                                        });

                                    }
                                });
                            }
                        });
                    }
                }
                facebookConnectPlugin.login(["public_profile", "email"], fbLoginSuccess, fbLoginFailed);
            });


            $("#btnTwitter").on("click", function () {
                TwitterConnect.login(function (data) {
                    var accessToken = data.token;
                    CallService('login', "POST", "api/Account/RegisterExternal", { "Provider": "Twitter", "ExternalAccessToken": accessToken, "userId": data.userId, "name": data.userName }, function (res) {
                        if (res != null) {

                            localStorage.setItem('appToken', res.access_token);
                            localStorage.setItem('refreshToken', res.refresh_token);
                            localStorage.setItem('userTypeId', res.AccountTypeId);
                            localStorage.setItem('Visitor', false);


                            //  localStorage.setItem("isSocial", true);
                            window.plugins.OneSignal
                          .startInit(pushAppId)
                          .handleNotificationOpened(notificationOpenedCallback)
                          .handleNotificationReceived(notificationReceivedCallback)
                          .endInit();
                            window.plugins.OneSignal.getIds(function (ids) {
                                console.log('getIds: ' + JSON.stringify(ids));
                                var obj = { "DeviceId": ids.userId, "IsOnline": true };
                                CallService('', "POST", "/api/NotificationSetting/SaveDeviceSetting/", obj, function (res) {
                                    if (res != null) {
                                        if (typeof res.responseText != 'undefined' && typeof res.responseText != undefined) {
                                        } else {
                                        }
                                    }
                                });
                            });

                            CallService('login', "POST", "/api/Account/GetUserInfo/", "", function (res) {
                                if (res != null) {
                                    if (typeof res.responseText != 'undefined' && typeof res.responseText != undefined) {
                                        mainView.router.loadPage({ pageName: 'index' });

                                    } else {

                                        localStorage.setItem('UserId', res.Id);
                                        localStorage.setItem("isSocial", true);
                                        localStorage.setItem('userLoggedIn', JSON.stringify(res));
                                        mainView.router.loadPage({ pageName: 'index' });

                                    }

                                }
                            });
                            // mainView.router.loadPage({ pageName: 'index' });
                        }
                    });
                }, function (error) {
                    console.log('Error in Login');
                });
            });


            $('#btngoogleplus').on("click", function () {

                window.plugins.googleplus.login(
                    {
                        'scopes': 'profile email', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
                        'webClientId': '751438422710-kf0hovu47agr8nspafk4n5ji72428tse.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
                        'offline': false // optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
                    },
                    function (obj) {
                        var userName = obj.displayName;
                        var userEmail = obj.email;
                        var userId = obj.userId;
                        var userToken = obj.idToken;

                        localStorage.setItem('socialEmail', userEmail);

                        var registerObj = {};
                        //if (localStorage.getItem('GoogleObject')) {
                        //    registerObj = JSON.parse(localStorage.getItem('GoogleObject'));
                        //}
                        //else {
                        registerObj = {
                            "Provider": "Google",
                            "userId": userId,
                            "name": userName,
                            "email": userEmail,
                            "ExternalAccessToken": userToken
                        };
                        //}

                        CallService('login', "POST", "api/Account/RegisterExternal", registerObj, function (res) {
                            if (res != null) {
                                localStorage.setItem('USName', res.userName);
                                localStorage.setItem('appToken', res.access_token);
                                localStorage.setItem('Visitor', false);
                                localStorage.setItem('loginUsingSocial', true);
                                localStorage.setItem('GoogleObject', JSON.stringify(registerObj));
                                CallService('login', "POST", "/api/Account/GetUserInfo/", '', function (res) {
                                    if (res != null) {

                                        res.userName = res.name;
                                        //  localStorage.setItem('userLoggedIn', JSON.stringify(res1));
                                        if (typeof res.photoUrl != 'undefined' && res.photoUrl != null && res.photoUrl != '' && res.photoUrl != ' ') {
                                            localStorage.setItem('usrPhoto', res.photoUrl);
                                        }
                                        else {
                                            localStorage.removeItem('usrPhoto');
                                        }

                                        localStorage.setItem('UserId', res.Id);
                                        localStorage.setItem("isSocial", true);
                                        localStorage.setItem('userLoggedIn', JSON.stringify(res));
                                        window.plugins.OneSignal
                                            .startInit(pushAppId)
                                            .handleNotificationOpened(notificationOpenedCallback)
                                            .handleNotificationReceived(notificationReceivedCallback)
                                            .endInit();
                                        window.plugins.OneSignal.getIds(function (ids) {
                                            console.log('getIds: ' + JSON.stringify(ids));

                                            var obj = { "DeviceId": ids.userId, "IsOnline": true };
                                            CallService('', "POST", "/api/NotificationSetting/SaveDeviceSetting/", obj, function (res) {
                                                if (res != null) {
                                                    if (typeof res.responseText != 'undefined' && typeof res.responseText != undefined) {
                                                    } else {
                                                    }

                                                }
                                            });
                                        });
                                        mainView.router.loadPage({ pageName: 'index' });

                                    }
                                });

                            }
                        });
                    },
                    function (msg) {
                        alert('error: ' + msg);
                    }
                );
                //callGoogle();
            });
            $('#LblIsOwnerForShelter').on('click', function () {

              
                var message = '';

               
                if (CurrentLanguage == 'ar')
                    message = 'UserHaveShelter';
                else
                    message = 'UserHaveShelterEn';
                

                    CallService("", "GET", "/api/Setting/GetLandingMessage/" + message, "", function (res) {
                        myApp.alert(res.Value, '', function () { });
                    });
                
            });
        }
    }
}
function GoTosignupPage(page) {
    if (typeof page != 'undefined') {
        loadSideMenuLinks();
        ClearRegister();
        CallService("", "GET", "/api/City/GetAllCities/" + CurrentLanguage, "", function (resCiti) {
            document.getElementById('CitiesDll').innerHTML = "";
            $("#CitiesDll").append('<option value="" selected disabled>' + City + '</option>');
            $.each(resCiti, function (i, option) {
                $("#CitiesDll").append($('<option/>').attr("value", option.Id).text(option.Name));
            });

            document.getElementById("hrefCityDll").setAttribute("data-picker-close-text", closeTxt);
        });

        if (initsignupPage == true) {
            initsignupPage = false;
            $("#btnSbmitRegistration").on("click", function () {
                var name = $("#txtName").val();
                var userName = $("#txtUserName").val();
                var phone = $("#txtMobile").val();
                var email = $("#txtEmail").val();
                var password = $("#txtPassword").val();
                var confirmPassword = $("#txtConfirmPassword").val();
                var selectedCity = $("#CitiesDll").val();
                var age = $("#txtUserAge").val();
                var isShelterOwner=false;
                FValidation.ValidateAll('User', function (res) {

                    if (res == true) {
                        if (password.length >= 6) {
                            if (password.trim().length > 0) {
                                var userObj = { "UserName": userName, "Email": email, "PhoneNumber": phone, "Name": name, "Role": "User", "Password": password, "ConfirmPassword": confirmPassword, "CityId": selectedCity, "Age": age, showInfo: true, IsShelterOwner: isShelterOwner }
                                CallService("signup", "POST", "/api/Account/Register", userObj, function (result) {
                                    //if (result.responseText == undefined)
                                    if (typeof result.responseText != 'undefined') {
                                        var responseText = JSON.parse(result.responseText);

                                        var errortextMsg = "";
                                        if (typeof responseText.ModelState != undefined) {
                                            if (typeof responseText.ModelState != undefined) {

                                                if (typeof responseText.ModelState["Email"] != "undefined" && typeof responseText.ModelState["Phone"] != "undefined" && typeof responseText.ModelState["UserName"] != "undefined") {
                                                    errortextMsg = EmailExistError
                                                    errortextMsg += PhoneExistError
                                                    errortextMsg += UserNameExistError;

                                                }
                                                else if (typeof responseText.ModelState["UserName"] != "undefined" && typeof responseText.ModelState["Phone"] != "undefined") {
                                                    errortextMsg = UserNameExistError;
                                                    errortextMsg += PhoneExistError;

                                                } else if (typeof responseText.ModelState["Email"] != "undefined" && typeof responseText.ModelState["Phone"] != "undefined") {
                                                    errortextMsg = EmailExistError;
                                                    errortextMsg += PhoneExistError;

                                                }
                                                else if (typeof responseText.ModelState["Email"] != "undefined" && typeof responseText.ModelState["UserName"] != "undefined") {
                                                    errortextMsg = EmailExistError;
                                                    errortextMsg += UserNameExistError;

                                                }
                                                else if (typeof responseText.ModelState["Email"] != "undefined") {
                                                    errortextMsg = EmailExistError;

                                                } else if (typeof responseText.ModelState["Phone"] != "undefined") {
                                                    errortextMsg = PhoneExistError;

                                                } else if (typeof responseText.ModelState["UserName"] != "undefined") {
                                                    errortextMsg = UserNameExistError;

                                                }
                                                if (typeof responseText.ModelState["userViewModel.CityId"] != "undefined") {
                                                    errortextMsg += responseText.ModelState["userViewModel.CityId"][0];
                                                }

                                                if (typeof responseText.ModelState["userViewModel.Age"] != "undefined") {
                                                    errortextMsg += AgeError;
                                                }
                                                if (typeof responseText.ModelState[""] != 'undefined' && responseText.ModelState[""][0].indexOf("can only contain letters or digits") >= 0) {
                                                    errortextMsg += ArabicUserNameError;
                                                }
                                                myApp.alert(errortextMsg, Wrong, function () { });

                                            } else {
                                                myApp.alert(responseText.Message, Wrong, function () { });
                                            }
                                        }



                                    } else {
                                        localStorage.setItem('UserId', result);
                                        localStorage.setItem('UserEntersCode', false);
                                        mainView.router.loadPage({ pageName: 'activation' });

                                    }


                                });

                            } else {
                                myApp.alert(PasswordEmptyError, Wrong, function () {
                                    $("#txtPassword").val('');
                                    $("#txtConfirmPassword").val('');
                                });
                            }
                        }
                        else { myApp.alert(PasswordError, Wrong, function () { }); }
                    }
                });
            });
            $("#btnBackToLogin").on("click", function () {
                $("#txtName").val('');
                $("#txtUserName").val('');
                $("#txtMobile").val('');
                $("#txtEmail").val('');
                $("#txtPassword").val('');
                $("#txtConfirmPassword").val('');
                $("#CitiesDll").val();
                $$(".smart-select .item-after").html("");
                $$("#SelectedCity").html(City);
                mainView.router.loadPage({ pageName: 'login' });
            });


    


        }
    }

}
function GoToaCtivationPage(page) {
    if (typeof page != 'undefined') {
        loadSideMenuLinks();
        if (initactivation == true) {
            initactivation = false;
            $("#btnActivation").on("click", function () {
                var activationCode = $("#txtActivationCode").val();
                if (activationCode != null && activationCode != "") {
                    var userId = localStorage.getItem('UserId');
                    var activateObj = { "UserId": userId, "Code": activationCode }
                    CallService("activation", "POST", "/api/Account/ConfirmEmail", activateObj, function (result) {
                        if (result != null) {

                            if (typeof result.responseText != 'undefined' && typeof result.responseText != undefined) {
                                var responseText = JSON.parse(result.responseText);

                                var errortextMsg = "";
                                if (typeof responseText.ModelState != undefined) {
                                    if (typeof responseText.ModelState != undefined) {
                                        for (var i = 0; i < responseText.ModelState[""].length; i++) {

                                            errortextMsg += responseText.ModelState[""][i];
                                        }
                                        //  myApp.alert(errortextMsg, Wrong, function () { });
                                        myApp.alert(WrongActivationNo, Wrong, function () { });
                                    } else {
                                        // myApp.alert(responseText.Message, Wrong, function () { });
                                        myApp.alert(ErrorOccured, Wrong, function () { });
                                    }
                                }
                            } else {
                                localStorage.setItem('Visitor', false);
                                localStorage.setItem('UserEntersCode', true);
                                mainView.router.loadPage({ pageName: 'login' });
                            }

                        } else {
                            myApp.alert(WrongActivationNo, Wrong, function () { });
                            // mainView.router.loadPage({ pageName: 'activation' });
                        }
                    });
                } else {
                    localStorage.setItem('UserEntersCode', false);
                    myApp.alert(WrongActivationNo, Wrong, function () { mainView.router.loadPage({ pageName: 'activation' }); });

                }
            });


            $("#btn_ReSendPassword").on("click",
                function () {
                    userId = localStorage.getItem("UserId");

                    CallService("forgetPass", "POST",
                        "/api/Account/ReSendConfirmationCode/" + userId,
                        "",
                        function (result) {
                            if (result != null && typeof result.status == 'undefined') {
                                myApp.alert(CodeSendSucess, Sucess, function () { });
                                mainView.router.loadPage({ pageName: 'login' });
                            } else if (result.status == 400) {
                                myApp.alert(ErrorOccured, Wrong, function () { });
                            }
                        });



                });


            $("#btnActivtionBack").on("click", function () {
                //localStorage.setItem('UserEntersCode', null);
                mainView.router.loadPage({ pageName: 'login' });
            });

        }
    }

}
function GoToforgetPassPage(page) {
    if (typeof page != 'undefined') {
        if (initforgetPass == true) {
            initforgetPass = false;
            $("#btn_SendPassword").on("click",
                function () {
                    var forgetPassEmail = $("#ForgetPassEmail").val();
                    if (forgetPassEmail != null && forgetPassEmail != "") {
                        var forgetObj = { 'Email': forgetPassEmail.trim() }
                        CallService("forgetPass",
                            "POST",
                            "/api/Account/ForgetPassword",
                            forgetObj,
                            function (result) {
                                if (result != null && typeof result.status == 'undefined') {
                                    myApp.alert(CodeSend,
                                   '',
                                   function () {
                                       $("#ForgetPassEmail").val('')
                                       mainView.router.loadPage({ pageName: 'resetPassword', query: { Email: forgetPassEmail.trim() } });
                                   });


                                } else if (result.status == 400) {
                                    myApp.alert(WrongEmail, Wrong, function () { });
                                }
                            });

                    } else {
                        myApp.alert(WrongEmail, Wrong, function () { });
                    }

                });

        }
    }
}
function GoToresetPasswordPage(page) {
    if (typeof page != 'undefined') {
        var email = page.query.Email;
        if (initresetPassword == true) {
            initresetPassword = false;
            $("#btnResetPassword").on('click', function () {
                FValidation.ValidateAll('resetPassword', function (res) {
                    if (res == true) {
                        var params = {

                            'Email': email,
                            'Code': $('#txtResetCode').val(),
                            'Password': $('#txtResetPassword').val(),
                            'ConfirmPassword': $('#txtResetConfirmPassword').val()
                        }

                        CallService('resetPassword', "POST", "/Api/Account/ResetPassword", params, function (res) {
                            if (res != null && typeof res.status == 'undefined') {
                                myApp.alert(PasswordChanged, Sucess, function () { mainView.router.loadPage({ pageName: 'login' }); });
                            }
                            else {
                                myApp.alert(WrongActivationNo, Wrong, function () { });
                            }
                        });


                    }
                });
            });
            $("#btnResetPasswordBackToHome").on('click', function () {
                mainView.router.loadPage({ pageName: 'login' });
            });

        }
    }
}
function GoToindexPage(page) {
    loadSideMenuLinks();
    notificationOpenedCallback = function (jsonData) {
        var result = JSON.stringify(jsonData);
        myApp.alert(jsonData.notification.payload.body, jsonData.notification.payload.title, function () { });
        if (jsonData.notification.payload.additionalData.ShelterId != null ) {
            CallService("", "GET", "/api/AnimalShelter/view/" + jsonData.notification.payload.additionalData.ShelterId, "", function(reqShelter) {

                if (reqShelter != null && typeof reqShelter.message == 'undefined') {
                    mainView.router.loadPage({ pageName: 'refugeDetails', query: { Shelter: reqShelter } });
                }

            });

        }
        else if (jsonData.notification.payload.additionalData.UserId != null && jsonData.notification.payload.additionalData.AnimalId != null) {
            mainView.router.loadPage({ pageName: 'AdoptRequestPopup' });
        }
        else if (jsonData.notification.payload.additionalData.RequestId != null && jsonData.notification.payload.additionalData.AnimalId != null) {
              var obj = {
                  'AnimalId': jsonData.notification.payload.additionalData.AnimalId,
                    'Language': CurrentLanguage
                };
                CallService("", "POST", "/api/Animal/GetAnimlById",obj, function (animal) {
                    mainView.router.loadPage({ pageName: 'animalDetails', query: { animal: animal, PrevName: 'index' } });
                });
        }
        else if (jsonData.notification.payload.additionalData.ChatId != null) {
            mainView.router.loadPage({ pageName: 'messages', query: { UserChat: jsonData.notification.payload.additionalData.ChatId } });
        }
        else {
            CallService("", "GET", "/api/Users/GetUserById/" + jsonData.notification.payload.additionalData.UserId, "", function (reqShelter) {

                if (reqShelter != null && typeof reqShelter.message == 'undefined') {
                    mainView.router.loadPage({ pageName: 'refugeDetails', query: { Shelter: reqShelter } });
                }

            });

        }
    
       };
    if (typeof page != 'undefined') {
        document.getElementById('IndexTitle').innerHTML = HomeTitle;
        var HomelastIndex = $$('#divAnimalContainer div.anmIndx-block').length;
        var HomemaxNumber = 0;
        var IsLoading = true;
        var IsFristLoading = true;
        var divInexAnimalContainer = document.getElementById('divAnimalContainer');
        var Indexobj = { "PageNumber": 1, "PageSize": 8, 'Language': CurrentLanguage };
        if (IsFristLoading == true) {
            IsFristLoading = false;
            CallService("index", "POST", "/api/Animal/GetAnimls", Indexobj, function (IndexAnimalRes) {
                if (IndexAnimalRes != null && IndexAnimalRes.length > 0) {
                    HomemaxNumber = IndexAnimalRes[0].OverAllCount;
                    document.getElementById('divAnimalContainer').innerHTML = '';
                    if ($$('#divAnimalContainer div.anmIndx-block').length != 0) {
                        document.getElementById('divAnimalContainer').innerHTML = '';

                    }
                    DrawanimalInIndex(IndexAnimalRes, 0, divInexAnimalContainer, 8, "", page.name);
                    $('#divAnimalContainer').show();
                    $('#divNoAnimalIndex').hide();
                    if ($$('#divAnimalContainer div.anmIndx-block').length < 8) {
                        $('.loading img').css('display', 'none');
                    } else {
                        $('.loading img').css('display', '');
                    }
                }
                else {

                    $('#divAnimalContainer').hide();
                    $('#divNoAnimalIndex').show();
                    $('.loading img').css('display', 'none');

                }
            });

        }


        if (initIndexPage == true) {
            initIndexPage = false;
            myApp.attachInfiniteScroll($$('#index-infinite-scroll'));
            $$('#index-infinite-scroll').on('infinite', function () {
                if ($$('#divAnimalContainer div.anmIndx-block').length < HomemaxNumber && IsLoading == true && $$('#divAnimalContainer div.anmIndx-block').length >= 8) {
                    IsLoading = false;
                    var pagingIndxobj = { "PageNumber": parseInt(parseInt($$('#divAnimalContainer div.anmIndx-block').length / 8) + 1), "PageSize": 8, 'Language': CurrentLanguage };
                    CallService("index", "POST", "/api/Animal/GetAnimls", pagingIndxobj, function (PagingIndexAnimalRes) {
                        if (PagingIndexAnimalRes != null && PagingIndexAnimalRes.length > 0) {
                            HomemaxNumber = PagingIndexAnimalRes[0].OverAllCount;
                            DrawanimalInIndex(PagingIndexAnimalRes, 0, divInexAnimalContainer, 8, "", page.name);
                            IsLoading = true;
                            $('.loading img').css('display', '');
                        }
                        else {

                        }

                    });
                } else {
                    $('.loading img').css('display', 'none');
                    // IsLoading = false;
                    //  IsFristLoading = true;
                }
            });
        }
    }
}
function GoTosearchResultPage(page) {
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        document.getElementById('divAnimalContainer').innerHTML = '';
        var Indexanimals = [];
        var loading = false;
        var lastIndex = 8;
        var maxItems = Indexanimals.length;
        var itemsPerLoad = 8;
        scrollLoadsBefore = false;
        document.getElementById('SearchPageTitle').innerHTML = SearchResultTitle;
        var pagingobj = {
            "PageNumber": 1,
            "PageSize": 8,
            'CategoryId': searchCatId,
            'AnimalName': animalName,
            'OwnerName': ownerName,
            'Language': CurrentLanguage

        };
        var divAnimalContainer = document.getElementById('divAnimalSearchContainer');
        CallService("index", "POST", "/api/Animal/GetAnimls", pagingobj, function (animalList) {
            if (animalList != null && animalList.length > 0) {
                document.getElementById('divAnimalSearchContainer').innerHTML = "";
                $('#divAnimalSearchContainer').show();
                $('#divAnimalSearch').hide();
                $('.loading img').css('display', '');
                Indexanimals = animalList;
                maxItems = animalList[0].OverAllCount;
                DrawanimalInIndexSearch(Indexanimals, 0, divAnimalContainer, itemsPerLoad, "", page.name);
                if (Indexanimals.length < itemsPerLoad) {
                    $('.loading img').css('display', 'none');
                    myApp.detachInfiniteScroll($$('#search-result-infinite-scroll'));
                    $$('.infinite-scroll-preloader').remove();
                    return;

                }
                lastIndex = $$('#divAnimalSearchContainer div.anmIndxSrc-block').length;
            } else {
                if ($$('#divAnimalSearchContainer div.anmIndxSrc-block').length == 0) {
                    $('#divAnimalSearchContainer').hide();
                    $('#divAnimalSearch').show();
                    $('.loading img').css('display', 'none');
                    myApp.detachInfiniteScroll($$('#search-result-infinite-scroll'));
                    $$('.infinite-scroll-preloader').remove();

                }

            }
        });
        //$('#divAnimalSearchContainer').hide();
        //$('#divAnimalSearch').show();
        //$('.loading img').css('display', 'none');
        if (initsearchResultPage == true) {
            initsearchResultPage = false;
            myApp.attachInfiniteScroll($$('#search-result-infinite-scroll'));
            $$('#search-result-infinite-scroll').on('infinite', function () {
                lastIndex = $$('#divAnimalSearchContainer div.anmIndxSrc-block').length;
                setTimeout(function () {
                    var pagingobj = { "PageNumber": parseInt(parseInt(lastIndex / 8) + 1), "PageSize": 8, 'CategoryId': searchCatId, 'AnimalName': animalName, 'OwnerName': ownerName, 'Language': CurrentLanguage };
                    CallService("index", "POST", "/api/Animal/GetAnimls", pagingobj, function (animalList) {
                        if (animalList != null && animalList.length > 0) {
                            lastIndex = $$('#divAnimalSearchContainer div.anmIndxSrc-block').length;
                            if (lastIndex >= maxItems || scrollLoadsBefore == true) {
                                myApp.detachInfiniteScroll($$('#search-result-infinite-scroll'));
                                $$('.infinite-scroll-preloader').remove();
                                return;
                            }

                            $('#divAnimalSearchContainer').show();
                            $('#divAnimalSearch').hide();
                            $('.loading img').css('display', '');
                            var indexanimalList = animalList;
                            maxItems = indexanimalList[0].OverAllCount;
                            DrawanimalInIndexSearch(indexanimalList, 0, divAnimalContainer, itemsPerLoad, "", page.name);
                            // loading = false;
                            if (indexanimalList.length < itemsPerLoad) {
                                $('.loading img').css('display', 'none');
                            }
                            lastIndex = $$('#divAnimalSearchContainer div.anmIndxSrc-block').length;
                            if (lastIndex >= maxItems || scrollLoadsBefore == true) {
                                myApp.detachInfiniteScroll($$('#search-result-infinite-scroll'));
                                $$('.infinite-scroll-preloader').remove();
                                return;
                            }

                        } else {
                            if ($$('#divAnimalSearchContainer div.anmIndxSrc-block').length == 0) {
                                $('#divAnimalSearchContainer').hide();
                                $('#divAnimalSearch').show();
                                $('.loading img').css('display', 'none');
                                myApp.detachInfiniteScroll($$('#search-result-infinite-scroll'));
                                $$('.infinite-scroll-preloader').remove();
                            }

                        }
                    });
                    $('.loading img').css('display', '');


                },
                    1000);
            });
        }
    }
}
function GoToAnimalDetailsPage(page) {
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        $("#LoadImageDetailsSlider").css('display', 'block');
        $("#ImageDetailsSlider").css('display', 'none');
        var reqAnimal = page.query.animal;
        AdoptionRequestAnimal = JSON.stringify(reqAnimal);
        document.getElementById('CommentsDiv').innerHTML = '';
        animalMassageid = reqAnimal.Id;
        AnimalIdComent = reqAnimal.Id;
        UserShowInfoAnimal= reqAnimal.ShowInfo
        if (reqAnimal.ShelterMobile != null) {
            AnimalMobile = reqAnimal.ShelterMobile;
        } else {
            AnimalMobile = reqAnimal.Mobile;
            
        }


        LoadComments(reqAnimal.Id);

        if (reqAnimal.TypeId == 4) {
            document.getElementById("animalaTypeTxt").innerHTML = AddAnimalLost;
            $("#btnAdoptedRequest").css('display', 'none');
        }
        else if (reqAnimal.TypeId == 3) {
            $("#btnAdoptedRequest").css('display', '');
            document.getElementById("animalaTypeTxt").innerHTML = AddAnimalEmergancy;
        }
        else if (reqAnimal.TypeId == 2) {
            $("#btnAdoptedRequest").css('display', '');
            document.getElementById("animalaTypeTxt").innerHTML = AddAnimalGiveup;
        }
        else if (reqAnimal.TypeId == 1) {
            $("#btnAdoptedRequest").css('display', '');
            document.getElementById("animalaTypeTxt").innerHTML = AddAnimalAboption;
        }
        else {
            $("#btnAdoptedRequest").css('display', '');

        }
        if (reqAnimal.Shelter != null) {
            document.getElementById("animalaOwner").innerHTML = reqAnimal.Shelter;
            animalShelterId = reqAnimal.ShelterId;
            animalUserId = null;
        }
        else {
            document.getElementById("animalaOwner").innerHTML = reqAnimal.UserFullName;
            animalUserId = reqAnimal.ApplicationUserId;
            animalShelterId = null;

        }


        if (animalUserId != null && localStorage.getItem("UserId") != null && animalUserId == localStorage.getItem("UserId")) {
            $("#EditAnimal").css('display', 'block');
            $("#DeleteAnimal").css('display', 'block');
        } else {
            $("#EditAnimal").css('display', 'none');
            $("#DeleteAnimal").css('display', 'none');
        }


        CallService("", "GET", "/api/Animal/" + reqAnimal.Id+"/"+CurrentLanguage, "", function (animal) {

            var divImageContainer = document.getElementById("ImageDetailsSlider");

            CallService("", "GET", "/api/Animal/GetAnimalImages?id=" + reqAnimal.Id, "",
                function (animalImages) {

                    if (animalImages.length > 0) {
                        $("#LoadImageDetailsSlider").css('display', 'none');
                        divImageContainer.innerHTML = "";
                        $("#ImageDetailsSlider").css('display', 'block');
                        for (var i = 0; i < animalImages.length; i++) {
                            var obj = animalImages[i];
                            var divImage = document.createElement('div');
                            divImage.className = 'item';
                            var image = document.createElement('img');
                            image.setAttribute('onerror', 'onImgError(this);');
                            divImage.appendChild(image);
                            if (animalImages[i].ImageUrl != null && animalImages[i].ImageUrl != "") {
                                image.src = animalImages[i].ImageUrl;
                            } else {
                                image.src = "img/NoImg.jpg";
                            }
                            divImageContainer.appendChild(divImage);


                        }
                    }
                    else {
                        $("#LoadImageDetailsSlider").css('display', 'none');
                        divImageContainer.innerHTML = "";
                        $("#ImageDetailsSlider").css('display', 'block');
                        var divImage = document.createElement('div');
                        divImage.className = 'item';
                        var image = document.createElement('img');
                        image.setAttribute('onerror', 'onImgError(this);');
                        divImage.appendChild(image);
                        image.src = "img/NoImg.jpg";
                        divImageContainer.appendChild(divImage);

                    }
                    $("#ImageDetailsSlider").owlCarousel();
                    $("#ImageDetailsSlider").data('owlCarousel').reinit({
                        items: 1,
                        itemsDesktop: false,
                        itemsDesktopSmall: false,
                        itemsTablet: [600, 1],
                        itemsMobile: false,
                        navigation: false,
                        pagination: false,
                        autoPlay: true,
                        stopOnHover: true
                    });

                });

            if (animal.Name != null) {
                document.getElementById("animalName").innerHTML = animal.Name;

            }
            else {
                document.getElementById("animalName").innerHTML = noAnimalName;

            }
            document.getElementById("animalCity").innerHTML = '<i class="ionicons ion-ios-home"></i> ' + reqAnimal.City;
            var gender = "";
            if (animal.Gendre == "0") {
                gender = MaleGender;
            }
            else {
                gender = FemaleGender;
            }
            document.getElementById("animalType").innerHTML = '<i class="ionicons ion-android-list"></i> ' + gender;
            if (animal.Age != null) {
                document.getElementById("animalaAge").innerHTML = '<i class="ionicons ion-arrow-graph-up-right"></i> ' + animal.Age;
            }
            else {
                document.getElementById("animalaAge").innerHTML = '<i class="ionicons ion-arrow-graph-up-right"></i> ' + NoAge;

            }
            if (animal.Vaccinations != null) {
                document.getElementById("animalaVaccine").innerHTML = '<i class="ionicons ion-medkit"></i> ' + animal.Vaccinations;
            }
            if (animal.VedioUrl != null) {
                $("#ShowAnimalvedioDiv").css('display', 'block');
                document.getElementById("ShowAnimalvedio").src = animal.VedioUrl;

            } else {
                $("#ShowAnimalvedioDiv").css('display', 'none');
                document.getElementById("ShowAnimalvedio").src = "";
            }

            if (animal.Notes != null) {
                $("#AnimalNotes").css('display', 'block');
                document.getElementById("AnimalNotes").innerHTML = animal.Notes;
            } else {
                document.getElementById("AnimalNotes").innerHTML = "";
                $("#AnimalNotes").css('display', 'none');
            }
            if (animal.Address != null) {
                document.getElementById("animalaAdressTxt").innerHTML = '<i class="ionicons ion-location"></i> ' + animal.Address;
            }
        });
        if (initAnimalDetailsPage == true) {
            initAnimalDetailsPage = false;

            $("#btnAdoptedRequest").on("click", function () {
                if (localStorage.getItem('UserId') == null) {
                    myApp.alert(NotAuthenticated, Wrong, function () {

                    });

                } else {

                    if (animalUserId != null && localStorage.getItem("UserId") != null && animalUserId == localStorage.getItem("UserId"))
                        myApp.alert(YouOWnAnimal, Wrong, function () { });
                    else

                        //  myApp.popup('#adoptedRequestPopupPage');
                        mainView.router.loadPage({ pageName: 'adoptedRequestPopup' });
                }

            });

            $("#btnChat").on("click", function () {
                //if (animalUserId ==  localStorage.getItem("UserId"))
                //    myApp.alert(YouOWnAnimal, Wrong, function () { });
                //else
                mainView.router.loadPage({ pageName: 'messages', query: { animalId: animalMassageid } });
            });

            $("#btnComment").on("click", function () {
                // AnimalIdComent = reqAnimal.Id;
                myApp.popup('#commentPopupPage');
            });

            $("#btnCall").on("click", function () {
                if (AnimalMobile != null) {
                    if (!UserShowInfoAnimal) {
                        myApp.alert(NoNotAvilable, warning, function () { });
                    } else {
                        window.plugins.CallNumber.callNumber(onSuccess, onError, AnimalMobile, true);
                    }
                   
                }
                else {

                    myApp.alert(NoNotAvilable, warning, function () { });
                }
            });

            $("#btnShare").on("click", function () {
                window.plugins.socialsharing.share('رفق', null, null, 'http://rfq-sa.com/Animal/Animal?id=' + AnimalIdComent + '&lang=' + CurrentLanguage);
                // window.plugins.socialsharing.share('رفق', null, null, 'https://play.google.com/store/apps');
            });

            $("#animalaOwner").on("click", function () {
                if (animalShelterId != null) {
                    CallService("animalDetails", "GET", "/api/AnimalShelter/view/" + animalShelterId, "", function (reqShelter) {

                        if (reqShelter != null && typeof reqShelter.message == 'undefined') {
                            animalShelterId = null;
                            mainView.router.loadPage({ pageName: 'refugeDetails', query: { Shelter: reqShelter } });
                        }

                    });
                }
                else {

                    CallService("animalDetails", "GET", "/api/Users/GetUserById/" + animalUserId, "", function (reqUser) {

                        if (reqUser != null && typeof reqUser.message == 'undefined') {
                            mainView.router.loadPage({ pageName: 'userDetails', query: { User: reqUser } });
                        }

                    });
                }
            });

            $("#EditAnimal").on('click', function () {
                mainView.router.loadPage({ pageName: 'addAnimal', query: { AnimalId: AnimalIdComent } });
            });
            $("#DeleteAnimal").on('click', function () {
                navigator.notification.confirm(
                    DeleteAnimalConfirmMsg,
                    function (buttonIndex) {
                        if (buttonIndex == 1) {
                            CallService("animalDetails", "Delete", "/api/Animal/delete/" + AnimalIdComent, "", function (animal) {
                                myApp.alert(SucessAnimalDelete, Sucess, function () { });
                                mainView.router.loadPage({ pageName: 'index' });
                            });
                        }
                    },
                    warning,
                    [Confirm, Cancel]
                );
            });

        }
    }
}
function GoToProfilePage(page) {
    $(window).scrollTop(0);
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        if (localStorage.getItem('userLoggedIn')) {
            var user = JSON.parse(localStorage.getItem('userLoggedIn'));
            if (user.PhotoUrl != null && user.PhotoUrl != "") {
                document.getElementById("txtuserImage").src = user.PhotoUrl;
            } else {
                document.getElementById("txtuserImage").src = "img/NoImg.jpg";
            }
            document.getElementById("txtuserProfileName").innerHTML = ' <i class="ionicons ion-ios-person"></i>' + "  " + user.Name;
            if (user.Email != null && user.Email != "") {
                document.getElementById("txtuserMail").innerHTML = '<i class="ionicons ion-android-mail"></i>' + "  " + user.Email;

            } else {
                document.getElementById("txtuserMail").innerHTML = '<i class="ionicons ion-android-mail"></i>' + notFound;

            }
            if (user.PhoneNumber != null && user.PhoneNumber != "") {
                document.getElementById("txtuserPhone").innerHTML = '<i class="ionicons ion-android-phone-portrait"></i>' + "  " + user.PhoneNumber;


            } else {
                document.getElementById("txtuserPhone").innerHTML = '<i class="ionicons ion-android-phone-portrait"></i>' + notFound;

            }
            if (user.Age != null && user.Age != "") {
                document.getElementById("txtuserAge").innerHTML = '<i class="ionicons ion-arrow-graph-up-right"></i>' + "  " + user.Age;
            } else {
                document.getElementById("txtuserAge").innerHTML = '<i class="ionicons ion-arrow-graph-up-right"></i>' + "  " + notFound;
            }

            
                if (user.ShowInfo == true) {
                    document.getElementById("ShowInfoDiv").innerHTML = showInfoDivMsg;

                } else {
                    document.getElementById("ShowInfoDiv").innerHTML = HideInfoDivMsg ;
                }
            
        }

        if (initProfilePage == true) {
            initProfilePage = false;
            $("#btnAdopt").on("click", function () {
                UserAnimalTypeId = 1;
                // myApp.popup('#AdoptPopupPage');

                mainView.router.loadPage({ pageName: 'AdoptPopup' });
            });
            $("#btnLeave").on("click", function () {
                UserAnimalTypeId = 2;
                // myApp.popup('#AdoptPopupPage');
                mainView.router.loadPage({ pageName: 'AdoptPopup' });
            });
            $("#btnVolunteer").on("click", function () {
                // myApp.popup('#VolunteerPopupPage');
                mainView.router.loadPage({ pageName: 'VolunteerPopup' });
            });
            $("#btnuserReport").on("click", function () {
                UserAnimalTypeId = 4;
                // myApp.popup('#AdoptPopupPage');
                mainView.router.loadPage({ pageName: 'AdoptPopup' });
            });
            $("#btnAdoptRequest").on("click", function () {
                // myApp.popup('#AdoptRequestPopupPage');

                mainView.router.loadPage({ pageName: 'AdoptRequestPopup' });
            });
            $("#btnEditProfil").on("click", function () {
                //  myApp.popup('#EditProfilePopupPage');
                mainView.router.loadPage({ pageName: 'EditProfilePopup' });
            });
            $("#txtuserImage").error(function (e) {
                document.getElementById("txtuserImage").src = "img/NoImg.jpg";
            });
        }

    }
}
function GoToEditProfilePage(page) {

    $(window).scrollTop(0);
    loadSideMenuLinks();
   
    if (typeof page != 'undefined') {
        CallService("EditProfilePopup", "GET", "/api/City/GetAllCities/" + CurrentLanguage, "", function (resCiti) {
            document.getElementById('EditUserCitiesDll').innerHTML = '';
            $("#EditUserCitiesDll").append('<option value="" selected disabled>' + City + '</option>');
            $.each(resCiti, function (i, option) {
                $("#EditUserCitiesDll").append($('<option/>').attr("value", option.Id).text(option.Name));
            });

            document.getElementById("hrefEditUserCitiesDll").setAttribute("data-picker-close-text", closeTxt);
            if (localStorage.getItem('userLoggedIn')) {
                var user = JSON.parse(localStorage.getItem('userLoggedIn'));
                loadUserImage('imgUserEdit', user.PhotoUrl);

                $("#txtEditUserName").val(user.Name);
                $("#txtEditUserEmail").val(user.Email);
                $("#TxtEditUserMobile").val(user.PhoneNumber);
                $("#txtEditUserAge").val(user.Age);
                if (user.CityId != "") {
                    $('#EditUserCitiesDll option[value="' + user.CityId + '"]').attr('selected', 'selected');
                    var text = $('#EditUserCitiesDll option[value="' + user.CityId + '"]').text();
                    $("#EditUserSelectedCity").text("");
                    $("#EditUserSelectedCity").text(text);
                }

                var gendertext = "";
                if (user.Gender == false) {
                    $('#EditUserGender option[value=0]').attr('selected', 'selected');
                    gendertext = $('#EditUserGender option[value=0]').text();
                    $("#EditUserGenderText").text(gendertext);
                } else {
                    $('#EditUserGender option[value=1]').attr('selected', 'selected');
                    gendertext = $('#EditUserGender option[value=1]').text();
                    $("#EditUserGenderText").text(gendertext);
                }
                debugger;
                ShowInfo = user.ShowInfo;
                if (user.ShowInfo == true) {
                    $('input[name="ShowInfo-checkbox"]').prop('checked', true);
                   
                } else {
                    $('input[name="ShowInfo-checkbox"]').prop('checked', false);
                }

            }
        });
        var genderDDL = $("#EditUserGender");
        document.getElementById('EditUserGender').innerHTML = '';
        document.getElementById("hrefEditUserGender").setAttribute("data-picker-close-text", closeTxt);
        //   document.getElementById("hrefEditUserCitiesDll").setAttribute("data-picker-close-text", closeTxt);
        var gendertxt = document.getElementById('EditUserGenderText');
        //  document.getElementById('EditUserGenderText').innerHTML = '';
        //  document.getElementById('EditUserGenderText').innerHTML = DefultGender;
        fillGenderDDL(genderDDL, gendertxt);
        
        if (localStorage.getItem("isSocial") == 'true')
            $('#liTxtEditUserMobile').css('display', 'block');
        else
            $('#liTxtEditUserMobile').css('display', 'none');

        if (initEditProfilePopup == true) {
            initEditProfilePopup = false;
            $("#btnUserProfileEdit").on("click",
                function () {

                    var photoUrl = $("#imgUserEdit").attr('src');
                    var name = $("#txtEditUserName").val();
                    var cityId = $("#EditUserCitiesDll").val();
                    var gendertxt = $("#EditUserGender").val();
                    var phone = $('#TxtEditUserMobile').val();
                    var gender;
                    if (gendertxt == "0") {
                        gender = false;
                    } else {
                        gender = true;
                    }
                    var age = $("#txtEditUserAge").val();
                    var param = { 'Name': name, 'CityId': cityId, 'Gender': gender, 'PhotoUrl': photoUrl, 'Age': age, 'PhoneNumber': phone, 'ShowInfo': ShowInfo }


                    FValidation.ValidateAll('EditPRofile', function (res) {
                        if (res == true) {
                            CallService("EditProfilePopup", "POST", "/api/Users/EditUser", param,
                  function (res) {
                      if (res != null && typeof res.status == 'undefined') {

                          CallService('EditProfilePopup',
                              "POST",
                              "/api/Account/GetUserInfo/",
                              "",
                              function (res) {
                                  if (res != null) {
                                      if (typeof res.responseText != 'undefined') {
                                          mainView.router
                                              .loadPage({ pageName: 'index' });

                                      } else {
                                          localStorage.setItem('UserId', res.Id);
                                          localStorage.setItem('userLoggedIn', JSON.stringify(res));
                                          myApp.alert(PasswordChanged, '', function () {
                                              mainView.router.loadPage({ pageName: 'profile' });
                                              GoToProfilePage();
                                          });

                                          $("#EditUserSelectedCity").text("");


                                      }

                                  }
                              });

                      } else {
                          if (res.status == 400) {
                              if (res.responseText != "") {
                                  var responseText = JSON.parse(res.responseText);
                                  if (typeof responseText.ModelState["userViewModel.Age"] != "undefined") {
                                      myApp.alert(AgeError, Wrong, function () { });
                                  } else if (typeof responseText.ModelState[""] != "undefined") {
                                      myApp.alert(PhoneExistError, Wrong, function () { });
                                  }
                              } else {
                                  myApp.alert(ErrorOccured, Wrong, function () { });
                              }


                          } else if (res.status == 401) {
                              myApp.alert(NotAuthenticated, Wrong, function () { });

                          }
                      }
                  });
                        }
                    });


                });
            $("#btnUserProfileClose").on("click",
                function () {
                    // myApp.closeModal('#EditProfilePopupPage');
                    mainView.router.loadPage({ pageName: 'profile' });
                });
            $("#btnUploadImage").on("click",
                function () {
                    getProfileImage();
                });


            $$('input[name="ShowInfo-checkbox"]').on('change', function() {
                debugger;
                var check = this.checked;
                ShowInfo = check;
                //if (check) {
                //    document.getElementById("btnShowInfo").innerHTML = showMsg;

                //} else {

                //}

            });
        }
    }

}
function GoToAdoptPopupPage() {
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        if (UserAnimalTypeId == 1) {
            document.getElementById('PopupTitle').innerHTML = '<i class="ionicons ion-ios-paw"></i>' + AdaptionRequestsTitle;

        }
        else if (UserAnimalTypeId == 2) {
            document.getElementById('PopupTitle').innerHTML = '<i class="ionicons ion-ios-paw"></i>' + AbandonRequestsTitle;

        }
        else if (UserAnimalTypeId == 4) {
            document.getElementById('PopupTitle').innerHTML = '<i class="ionicons ion-ios-paw"></i> ' + ReportsTitle;

        }
        var adoptlastIndex = $$('#divUserAnimalContainer div.anm-block').length;
        var adoptmaxNumber = 0;
        var isLoadingadopt = true;
        var isFristLoadingadopt = true;
        var divAnimalContainer = document.getElementById('divUserAnimalContainer');
        var pagingobj = { "PageNumber": 1, "PageSize": 8, "TypeId": UserAnimalTypeId, 'Language': CurrentLanguage };
        if (isFristLoadingadopt == true) {
            isFristLoadingadopt = false;
            CallService("AdoptPopup", "POST", "/api/Animal/AnimalsByLoggedInUser", pagingobj, function (animalList) {
                if (animalList != null && animalList.length > 0) {
                    adoptmaxNumber = animalList[0].OverAllCount;
                    $('#NoAnimalUserAnimal').hide();
                    $('#divUserAnimalContainer').show();
                    document.getElementById('divUserAnimalContainer').innerHTML = "";
                    if ($$('#divUserAnimalContainer div.anm-block').length != 0) {
                        document.getElementById('divUserAnimalContainer').innerHTML = '';
                    }
                    DrawanimalInHome(animalList, 0, divAnimalContainer, 8, "AdoptPopupPage", page.name);
                    if ($$('#divUserAnimalContainer div.anm-block').length < 8) {
                        $('.loading img').css('display', 'none');
                    } else {
                        $('.loading img').css('display', '');
                    }

                }
                else {
                    $('#divUserAnimalContainer').hide();
                    $('#NoAnimalUserAnimal').show();
                    $('.loading img').css('display', 'none');
                }
            });
        }



        if (initAdoptPopupPage == true) {
            initAdoptPopupPage = false;

            myApp.attachInfiniteScroll($$('#adopt-popup-infinite-scroll'));
            $$('#adopt-popup-infinite-scroll').on('infinite', function () {
                if ($$('#divUserAnimalContainer div.anm-block').length < adoptmaxNumber && isLoadingadopt == true && $$('#divUserAnimalContainer div.anm-block').length >= 8) {
                    isLoadingadopt = false;
                    var pagingobj = { "PageNumber": parseInt(parseInt($$('#divUserAnimalContainer div.anm-block').length / 8) + 1), "PageSize": 8, "TypeId": UserAnimalTypeId, 'Language': CurrentLanguage };

                    CallService("AdoptPopup", "POST", "/api/Animal/AnimalsByLoggedInUser", pagingobj, function (animalList) {
                        if (animalList != null && animalList.length > 0) {
                            adoptmaxNumber = animalList[0].OverAllCount;
                            DrawanimalInHome(animalList, 0, divAnimalContainer, 8, "AdoptPopupPage", page.name);
                            isLoadingadopt = true;
                            $('.loading img').css('display', '');

                        } else {

                        }
                    });
                }
                else {
                    $('.loading img').css('display', 'none');
                }



            });



            $("#btncloseAdoptPopup").on("click",
                function () {
                    //myApp.closeModal('#AdoptPopupPage');
                    mainView.router.loadPage({ pageName: 'profile' });
                });
        }
    }
}
function GoToAdoptRequestPopupPage() {
    $(window).scrollTop(0);
    loadSideMenuLinks();
    if (typeof page != 'undefined') {

        var AdoptRequests = [];
        var loading = false;
        var lastIndex = 8;
        var maxItems = AdoptRequests.length;
        var itemsPerLoad = 8;
        scrollLoadsBefore = false;
        var divAdoptRequestContainer = document.getElementById('divAdoptRequestContainer');

        var pagingobj = { "PageNumber": 1, "PageSize": 8, 'Language': CurrentLanguage };
        CallService("AdoptPopup", "POST", "/api/AdoptionRequest/AdoptionRequestForUser", pagingobj, function (AdoptionRequestList) {
            if (AdoptionRequestList != null && AdoptionRequestList.length > 0) {
                document.getElementById('divAdoptRequestContainer').innerHTML = "";
                $('#divAdoptRequestContainer').show();
                $('#NoAdoptRequest').hide();
                $('.loading img').css('display', '');
                AdoptRequests = AdoptionRequestList;
                maxItems = AdoptionRequestList[0].OverAllCount;
                DrawanigAdoptRequest(AdoptionRequestList, 0, divAdoptRequestContainer, itemsPerLoad);
                if (AdoptRequests.length < itemsPerLoad) {
                    $('.loading img').css('display', 'none');
                }
                lastIndex = $$('#divAdoptRequestContainer div.AdbtReq-block').length;
            } else {
                $('#divAdoptRequestContainer').hide();
                $('#NoAdoptRequest').show();
                $('.loading img').css('display', 'none');
            }
        });

        $('#divAdoptRequestContainer').hide();
        $('#NoAdoptRequest').show();
        $('.loading img').css('display', 'none');
        myApp.attachInfiniteScroll($$('#adopt-request-popup-infinite-scroll'));
        $$('#adopt-request-popup-infinite-scroll').on('infinite', function () {

            lastIndex = $$('#divAdoptRequestContainer div.AdbtReq-block').length;
            if (lastIndex >= maxItems || scrollLoadsBefore == true) {
                myApp.detachInfiniteScroll($$('#adopt-request-popup-infinite-scroll'));
                $$('.infinite-scroll-preloader').remove();
                return;
            }
            if (loading) return;

            loading = true;

            setTimeout(function () {


                var pagingobj = { "PageNumber": parseInt(parseInt(lastIndex / 8) + 1), "PageSize": 8, 'Language': CurrentLanguage };

                CallService("AdoptPopup", "POST", "api/AdoptionRequest/AdoptionRequestForUser", pagingobj, function (RequestList) {
                    if (RequestList != null && RequestList.length > 0) {
                        lastIndex = $$('#divAdoptRequestContainer div.AdbtReq-block').length;
                        if (lastIndex >= maxItems || scrollLoadsBefore == true) {
                            myApp.detachInfiniteScroll($$('#adopt-request-popup-infinite-scroll'));
                            $$('.infinite-scroll-preloader').remove();
                            return;
                        }

                        loading = false;
                        $('#divAdoptRequestContainer').show();
                        $('#NoAdoptRequest').hide();
                        $('.loading img').css('display', '');
                        var RequestsList = RequestList;
                        maxItems = RequestList[0].OverAllCount;
                        DrawanigAdoptRequest(RequestsList, 0, divAdoptRequestContainer, itemsPerLoad);
                        if (RequestsList.length < itemsPerLoad) {
                            $('.loading img').css('display', 'none');
                        }
                        lastIndex = $$('#divAdoptRequestContainer div.AdbtReq-block').length;

                    } else {
                        $('#divAdoptRequestContainer').hide();
                        $('#NoAdoptRequest').show();
                        $('.loading img').css('display', 'none');
                    }
                });
                $('.loading img').css('display', '');


            }, 1000);
        });
        if (initAdoptRequestPopupPage == true) {
            initAdoptRequestPopupPage = false;
            $("#btncloseAdoptRequestPopup").on("click",
                function () {
                    //  myApp.closeModal('#AdoptRequestPopupPage');

                    mainView.router.loadPage({ pageName: 'profile' });
                });
        }
    }
}
function GoToVolunteerPopupPage(page) {
    $(window).scrollTop(0);
    loadSideMenuLinks();

    //if (fromLogin == true) {
    //    fromLogin = false;
    //    DrawUseruserShelter(UserSheltersList, 0, divVolunteerContainer);
    //}
    if (typeof page != 'undefined') {
        var animals = [];
        var loading = false;
        var lastIndex = 8;
        var maxItems = animals.length;
        var itemsPerLoad = 8;
        scrollLoadsBefore = false;


        var divAnimalContainer = document.getElementById('divVolunteerContainer');

        document.getElementById('divUserAnimalContainer').innerHTML = "";
        document.getElementById('divneedCareAnimalContainer').innerHTML = "";
        //  document.getElementById('divAnimalContainer').innerHTML = "";
        //document.getElementById('divLostAnimalContainer').innerHTML = "";
        document.getElementById('divRefugeContainer').innerHTML = "";


        var pagingobj = { "PageNumber": 1, "PageSize": 8, "Language": CurrentLanguage };
        CallService("VolunteerPopup", "POST", "/api/Volunteer/VolunteerUserRequest", pagingobj, function (animalList) {
            if (animalList != null && animalList.length > 0) {
                document.getElementById('divVolunteerContainer').innerHTML = "";
                $('#divVolunteerContainer').show();
                $('#NoVolunteerUserAnimal').hide();
                $('.loading img').css('display', '');

                animals = animalList;
                maxItems = animalList[0].OverAllCount;
                DrawUseruserShelter(animals, 0, divAnimalContainer, itemsPerLoad, "VolunteerPopupPage");
                if (animals.length < itemsPerLoad) {
                    $('.loading img').css('display', 'none');
                }
                lastIndex = $$('#divVolunteerContainer div.anm-block').length;
            } else {
                $('#divVolunteerContainer').hide();
                $('#NoVolunteerUserAnimal').show();
                $('.loading img').css('display', 'none');
            }
        });

        $('#divVolunteerContainer').hide();
        $('#NoVolunteerUserAnimal').show();
        $('.loading img').css('display', 'none');

        myApp.attachInfiniteScroll($$('#volunteer-popup-infinite-scroll'));
        $$('#volunteer-popup-infinite-scroll').on('infinite', function () {

            lastIndex = $$('#divVolunteerContainer div.anm-block').length;
            if (lastIndex >= maxItems || scrollLoadsBefore == true) {
                myApp.detachInfiniteScroll($$('#volunteer-popup-infinite-scroll'));
                $$('.infinite-scroll-preloader').remove();
                return;
            }
            if (loading) return;

            loading = true;

            setTimeout(function () {


                var pagingobj = { "PageNumber": parseInt(parseInt(lastIndex / 8) + 1), "PageSize": 8, "Language": CurrentLanguage };

                CallService("VolunteerPopup", "POST", "/api/Volunteer/VolunteerUserRequest", pagingobj, function (animalList) {
                    if (animalList != null && animalList.length > 0) {
                        lastIndex = $$('#divVolunteerContainer div.anm-block').length;
                        if (lastIndex >= maxItems || scrollLoadsBefore == true) {
                            myApp.detachInfiniteScroll($$('#volunteer-popup-infinite-scroll'));
                            $$('.infinite-scroll-preloader').remove();
                            return;
                        }

                        loading = false;
                        $('#divVolunteerContainer').show();
                        $('#NoVolunteerUserAnimal').hide();
                        $('.loading img').css('display', '');
                        var animal = animalList;
                        maxItems = animalList[0].OverAllCount;

                        DrawUseruserShelter(animal, 0, divAnimalContainer, itemsPerLoad, "VolunteerPopupPage");
                        if (animal.length < itemsPerLoad) {
                            $('.loading img').css('display', 'none');
                        }
                        lastIndex = $$('#divVolunteerContainer div.anm-block').length;

                    } else {
                        $('#divVolunteerContainer').hide();
                        $('#NoVolunteerUserAnimal').show();
                        $('.loading img').css('display', 'none');
                    }
                });
                $('.loading img').css('display', '');


            }, 1000);
        });
        $("#btnCloseVolunteer").on("click", function () {
            //myApp.closeModal('#VolunteerPopupPage');
            mainView.router.loadPage({ pageName: 'profile' });
        });
    }
}
function GoToRefugePage(page) {
    loadSideMenuLinks();

    if (typeof page != 'undefined') {


        var RefugemaxNumber = 0;
        var IsRefugeLoading = true;
        var IsRefugeFristLoading = true;
        var divRefugeContainer = document.getElementById('divRefugeContainer');
        var Indexobj = { "PageNumber": 1, "PageSize": 8, 'Language': CurrentLanguage };
        if (IsRefugeFristLoading == true) {
            IsRefugeFristLoading = false;
            CallService("refuge", "POST", "/api/AnimalShelter/GetShelter", Indexobj, function (RefugeRes) {
                if (RefugeRes != null && RefugeRes.length > 0) {
                    RefugemaxNumber = RefugeRes[0].OverAllCount;
                    document.getElementById('divRefugeContainer').innerHTML = '';
                    if ($$('#divRefugeContainer div.anm-block').length != 0) {
                        document.getElementById('divRefugeContainer').innerHTML = '';
                    }
                    DrawShelters(RefugeRes, 0, divRefugeContainer, 8);
                    $('#divRefugeContainer').show();
                    $('#divNoRefugeFound').hide();
                    if ($$('#divRefugeContainer div.anm-block').length < 8) {
                        $('.loading img').css('display', 'none');
                    } else {
                        $('.loading img').css('display', '');
                    }

                }
                else {

                    $('#divRefugeContainer').hide();
                    $('#divNoRefugeFound').show();
                    $('.loading img').css('display', 'none');

                }
            });

        }


        if (initrefugePage == true) {
            initrefugePage = false;
            myApp.attachInfiniteScroll($$('#refuge-infinite-scroll'));
            $$('#refuge-infinite-scroll').on('infinite', function () {
                if ($$('#divRefugeContainer div.anm-block').length < RefugemaxNumber && IsRefugeLoading == true) {
                    IsRefugeLoading = false;
                    var pagingIndxobj = { "PageNumber": parseInt(parseInt($$('#divRefugeContainer div.anm-block').length / 8) + 1), "PageSize": 8, 'Language': CurrentLanguage };
                    CallService("refuge", "POST", "/api/AnimalShelter/GetShelter", pagingIndxobj, function (PagingRefugeRes) {
                        if (PagingRefugeRes != null && PagingRefugeRes.length > 0) {
                            RefugemaxNumber = PagingRefugeRes[0].OverAllCount;

                            DrawShelters(PagingRefugeRes, 0, divRefugeContainer, 8);
                            IsRefugeLoading = true;
                            $('.loading img').css('display', '');
                        }
                        else {

                        }

                    });
                } else {
                    $('.loading img').css('display', 'none');
                }
            });
        }
    }
}
function GoTorefugeDetailsPage(page) {
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        $('#AnimaltabTitle').removeClass('active');
        $('#AnimalInfotabTitle').removeClass('active');
        $('#AnimaltabTitle').addClass('active');
        myApp.showTab('#Refugetab3');

        var reqShelter = page.query.Shelter;
        ShelterId = reqShelter.Id;
        reqShelterMobile = reqShelter.Mobile;
        document.getElementById('AnimalsinRefuge').innerHTML = "";
        var divAnimalsinRefuge = document.getElementById('AnimalsinRefuge');

        /////////////////////////
        var animals = [];
        var loading = false;
        var lastIndex = 8;
        var maxItems = animals.length;
        var itemsPerLoad = 8;
        scrollLoadsBefore = false;


        document.getElementById('AnimalsinUser').innerHTML = "";
        document.getElementById('divUserAnimalContainer').innerHTML = "";
        document.getElementById('divneedCareAnimalContainer').innerHTML = "";
        //document.getElementById('divAnimalContainer').innerHTML = "";
        //  document.getElementById('divLostAnimalContainer').innerHTML = "";
        document.getElementById('divRefugeContainer').innerHTML = "";


        var param = { 'ShelterId': reqShelter.Id, 'PageNumber': 1, 'PageSize': 8, 'Language': CurrentLanguage }
        CallService("refugeDetails", "POST", "/api/Animal/AnimalsByShelter", param, function (animalList) {
            if (animalList != null && animalList.length > 0) {
                document.getElementById('AnimalsinRefuge').innerHTML = "";
                $('#AnimalsinRefuge').show();
                $('#divNoRAnimalsinRefugeFound').hide();
                $('.loading img').css('display', '');

                animals = animalList;
                maxItems = animalList[0].OverAllCount;
                DrawUseranimal(animals, 0, divAnimalsinRefuge, "", page.name);
                if (animals.length < itemsPerLoad) {
                    $('.loading img').css('display', 'none');
                }
                lastIndex = $$('#AnimalsinRefuge div.anm-block').length;
            } else {
                $('#AnimalsinRefuge').hide();
                $('#divNoRAnimalsinRefugeFound').show();
                $('.loading img').css('display', 'none');
            }
        });

        $('#AnimalsinRefuge').hide();
        $('#divNoRAnimalsinRefugeFound').show();
        $('.loading img').css('display', 'none');
        document.getElementById("refugeName").innerHTML = '<i class="ionicons ion-android-list"></i>' + reqShelter.Name;
        document.getElementById("ShelterDetails").innerHTML = reqShelter.Name;
        if (reqShelter.Email != null)
            document.getElementById("refugeEmail").innerHTML = '<i class="ionicons ion-android-mail"></i> ' + reqShelter.Email;
        else
            document.getElementById("refugeEmail").innerHTML = '<i class="ionicons ion-android-mail"></i> ' + notFound;


        if (reqShelter.Mobile != null) {
            //window.plugins.CallNumber.callNumber(onSuccess, onError, reqShelter.Mobile, true);
            document.getElementById("refugePhone").innerHTML = '<i class="ionicons ion-android-phone-portrait"></i> ' + reqShelter.Mobile;
        }
        else {
            document.getElementById("refugePhone").innerHTML = '<i class="ionicons ion-android-phone-portrait"></i> ' + notFound;
        }




        if (reqShelter.IsNotify == true) {
            document.getElementById("btnNotification").innerHTML = subscribeforShelter;
            $('input[name="Notify-checkbox"]').prop('checked', true);
        }
        else if (reqShelter.IsNotify == false) {
            document.getElementById("btnNotification").innerHTML = subscribeforShelter;
            $('input[name="Notify-checkbox"]').prop('checked', false);
        }
        else {
            document.getElementById("btnNotification").innerHTML = subscribeforShelter;
            $('input[name="Notify-checkbox"]').prop('checked', false);
        }
        if (reqShelter.PhotoUrl != null && reqShelter.PhotoUrl != "") {
            document.getElementById("refugeImg").src = reqShelter.PhotoUrl;
        } else {
            document.getElementById("refugeImg").src = "img/NoImg.jpg";
        }
        myApp.attachInfiniteScroll($$('#tab3'));
        $$('#tab3').on('infinite', function () {

            lastIndex = $$('#AnimalsinRefuge div.anm-block').length;
            if (lastIndex >= maxItems || scrollLoadsBefore == true) {
                myApp.detachInfiniteScroll($$('#tab3'));
                $$('.infinite-scroll-preloader').remove();
                return;
            }
            if (loading) return;

            loading = true;

            setTimeout(function () {

                var param = { 'ShelterId': reqShelter.Id, 'PageNumber': parseInt(parseInt(lastIndex / 8) + 1), 'PageSize': 8, 'Language': CurrentLanguage }
                CallService("refugeDetails", "POST", "/api/Animal/AnimalsByShelter", param, function (animalList) {
                    if (animalList != null && animalList.length > 0) {
                        lastIndex = $$('#AnimalsinRefuge div.anm-block').length;
                        if (lastIndex >= maxItems || scrollLoadsBefore == true) {
                            myApp.detachInfiniteScroll($$('#tab3'));
                            $$('.infinite-scroll-preloader').remove();
                            return;
                        }

                        loading = false;
                        $('#AnimalsinRefuge').show();
                        $('#divNoRAnimalsinRefugeFound').hide();
                        $('.loading img').css('display', '');
                        var animal = animalList;
                        maxItems = animalList[0].OverAllCount;

                        DrawUseranimal(animal, 0, divAnimalsinRefuge, "", page.name);
                        if (animal.length < itemsPerLoad) {
                            $('.loading img').css('display', 'none');
                        }
                        lastIndex = $$('#AnimalsinRefuge div.anm-block').length;

                    } else {
                        $('#AnimalsinRefuge').hide();
                        $('#divNoRAnimalsinRefugeFound').show();
                        $('.loading img').css('display', 'none');
                    }
                });
                $('.loading img').css('display', '');


            }, 1000);
        });

        if (initrefugeDetails == true) {
            initrefugeDetails = false;

            $("#tab3").click(function () {
                if (!$('#tab3').hasClass('active')) {
                    $('#tab3').addClass('active');
                }

                $('#tab4').removeClass('active');

            });
            $("#tab4").click(function () {
                if (!$('#tab4').hasClass('active')) {
                    $('#tab4').addClass('active');
                }
                $('#tab3').removeClass('active');
            });

            $("#refugePhone").on("click", function () {



                if (reqShelterMobile != null) {
                    window.plugins.CallNumber.callNumber(onSuccess, onError, reqShelterMobile, true);
                }
                else {
                    myApp.alert(NoNotAvilable, warning, function () { });
                }
            });

            $("#btnvolunteer").on("click", function () {
                mainView.router.loadPage({ pageName: 'volunteer', query: { CurrentShelterId: ShelterId } });

            });
            $("#btnleaveRequest").on("click", function () {
                //myApp.popup('#LeaveRequestPage');
                mainView.router.loadPage({ pageName: 'LeaveRequest' });
            });
            $$('input[name="Notify-checkbox"]').on('change', function () {

                var pagingobj = { "AnimalShelterId": ShelterId, "IsNotify": this.checked }
                CallService("refugeDetails", "POST", "/api/NotificationSetting/Save", pagingobj, function (res) {
                    if (res != null && typeof res.status == 'undefined') {
                        if (res.IsNotify == true) {
                            myApp.alert(subscribeforShelter, '', function () {
                                document.getElementById("btnNotification").innerHTML = subscribeforShelter;

                            });


                        } else {
                            myApp.alert(UnsubscribeforShelter, '', function () {
                                document.getElementById("btnNotification").innerHTML = subscribeforShelter;
                            });

                        }

                    } else if (res.status == 401) {
                        myApp.alert(NotAuthenticated, Wrong, function () {
                            $('input[name="Notify-checkbox"]').prop('checked', false);
                        });
                    } else {
                        myApp.alert(ErrorOccured, Wrong, function () { });
                    }

                });

            })


            $$('#AnimaltabTitle').on('click', function () {
                var tab33 = myApp.showTab('#Refugetab3');
                $('#AnimaltabTitle').removeClass('active');
                $('#AnimaltabTitle').addClass('active');
                $('#AnimalInfotabTitle').removeClass('active');
            });
            $$('#AnimalInfotabTitle').on('click', function () {
                var tab4 = myApp.showTab('#Refugetab4');
                $('#AnimalInfotabTitle').removeClass('active');
                $('#AnimalInfotabTitle').addClass('active');
                $('#AnimaltabTitle').removeClass('active');

            });
        }


    }
}
function GoTovolunteerPage(page) {
    $(window).scrollTop(0);
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        $$(".smart-select .item-after").html("");
        $$("#selectedvolunteerFor").html(ddlvolunteerForTxt);
        $$("#selectedvolunteerType").html(ddlvolunteerTypeTxt);
        var reqShelterId = page.query.CurrentShelterId;
        CallService("volunteer", "GET", "/api/AnimalShelter/" + CurrentLanguage, "", function (Shelter) {
            document.getElementById('ddlvolunteerFor').innerHTML = "";
            $("#ddlvolunteerFor").append('<option value="" selected disabled>' + ddlvolunteerForTxt + '</option>');
            $.each(Shelter, function (i, option) {
                $("#ddlvolunteerFor").append($('<option/>').attr("value", option.Id).text(option.Name));
            });

        });
        //CallService("volunteer", "GET", "/api/TypeOfVolunteer/" + CurrentLanguage, "", function (Shelter) {
        //    document.getElementById('ddlvolunteerType').innerHTML = "";
        //    $("#ddlvolunteerType").append('<option value="" selected disabled>' + ddlvolunteerTypeTxt + '</option>');
        //    $.each(Shelter, function (i, option) {
        //        $("#ddlvolunteerType").append($('<option/>').attr("value", option.Id).text(option.Value));
        //    });
        //});

        CallService("volunteer", "GET", "/api/TypeOfVolunteer/" + CurrentLanguage, "", function (Shelter) {
            document.getElementById('volunteerTypGroup').innerHTML = "";

            $.each(Shelter, function (i, option) {
                var volunteerTypLi   =document.createElement("li");
                var volunteerTypLabel = document.createElement("label");
                volunteerTypLabel.className = "label-checkbox item-content";
                var volunteerTypinput = document.createElement("input");
                volunteerTypinput.type = "checkbox";
                volunteerTypinput.name = "volunteer-checkbox";
                volunteerTypinput.className = "label-checkbox item-content";
                volunteerTypinput.value = option.Id;

                var volunteerTypDiv= document.createElement("div");
                volunteerTypDiv.className = "item-media";
                var volunteerTypi = document.createElement("i");
                volunteerTypi.className = "icon icon-form-checkbox";

                var volunteerTypDivTitleContainer = document.createElement("div");
                volunteerTypDivTitleContainer.className = "item-inner";

                var volunteerTypDivTitle = document.createElement("div");
                volunteerTypDivTitle.className = "item-title";
                volunteerTypDivTitle.innerHTML = option.Value;

                var volunteerTypGroup = document.getElementById('volunteerTypGroup');

                volunteerTypDiv.appendChild(volunteerTypi);
                volunteerTypDivTitleContainer.appendChild(volunteerTypDivTitle);

                volunteerTypLabel.appendChild(volunteerTypinput);
                volunteerTypLabel.appendChild(volunteerTypDiv);
                volunteerTypLabel.appendChild(volunteerTypDivTitleContainer);
                volunteerTypLi.appendChild(volunteerTypLabel);
                volunteerTypGroup.appendChild(volunteerTypLi);
            });
        });

        if (initvolunteer == true) {
            initvolunteer = false;
            $("#btnSaveVolunteer").on('click', function () {
                var selectedvolunteerFor = $("#ddlvolunteerFor").val();
                //var selectedvolunteerType = $("#ddlvolunteerType").val();

                var checkboxes = document.getElementsByName("volunteer-checkbox");

                var checkboxesChecked =[];
                for (var i=0; i<checkboxes.length; i++) {
                    if (checkboxes[i].checked) {
                        checkboxesChecked.push(checkboxes[i].value);
                    }
                }
            //    $('input[type=radio][name=]').change(function () {
            //   // if (this.innerText == YES) {
            //});

                if (selectedvolunteerFor != null ) {
                    userId = localStorage.getItem("UserId");
                    if (userId != null && typeof userId != 'undefined') {
                        //var param = {
                        //    'TypeOfVolunteerId': parseInt(selectedvolunteerType), 'ShelterId': parseInt(selectedvolunteerFor), 'ApplicationUserId': userId
                        //}

                        var param = { "ShelterId": parseInt(selectedvolunteerFor), "ApplicationUserId": userId, "TypeOfVolunteerId": checkboxesChecked }

                        
                            CallService("volunteer", "GET", "/api/Volunteer/GetbyShelter/" + parseInt(selectedvolunteerFor), "", function(res) {
                                if (res != null && typeof res.status == 'undefined') {

                                    myApp.alert(VolunterAddedPefor, Wrong, function() {});
                                } else {
                                    if (checkboxesChecked.length > 0) {
                                        CallServiceJson("volunteer", "POST", "/api/Volunteer/save", JSON.stringify(param), function (res) {
                                            if (res != null && typeof res.status == 'undefined') {
                                                myApp.alert(RequestAdded, '', function() {
                                                    // GoTovolunteerPage(page);
                                                    mainView.router.loadPage({ pageName: 'index' });
                                                });

                                            } else if (res.status == 401) {
                                                myApp.alert(NotAuthenticated, Wrong, function() {});
                                            } else {
                                                myApp.alert(ErrorOccured, Wrong, function () { });
                                            }
                                        });
                                    } else {
                                        myApp.alert(FiledsEmpty, Wrong, function() {});
                                    }
                                }
                            });

                        
                  


                    } else {
                        myApp.alert(NotAuthenticated, Wrong, function () { });
                    }

                } else {
                    myApp.alert(FiledsEmpty, Wrong, function () { });
                }


            });
        }

    }
}
function GoToLeaveRequestPage() {
    $(window).scrollTop(0);
    loadSideMenuLinks();
    if (typeof page != 'undefined') {

        ClearLeaveRequest();
        document.getElementById('imgLeaveAnimalContainerDiv').innerHTML = "";
        CallService("", "GET", "/api/Category/GetCategories/" + CurrentLanguage, "", function (resCiti) {
            document.getElementById('ddlLeaveAnimalType').innerHTML = "";
            document.getElementById('selectedLeaveAnimalType').innerHTML = AnimalCategory;

            $("#ddlLeaveAnimalType").append('<option value="" selected disabled>' + AnimalCategory + '</option>');
            $.each(resCiti, function (i, option) {
                $("#ddlLeaveAnimalType").append($('<option/>').attr("value", option.Id).text(option.Name));
            });
        });
        CallService("", "GET", "/api/City/GetAllCities/" + CurrentLanguage, "", function (resCiti) {
            document.getElementById('ddlLeaveAnimalCity').innerHTML = "";
            document.getElementById('selectedLeaveAnimalCity').innerHTML = City;
            $("#ddlLeaveAnimalCity").append('<option value="" selected disabled>' + City + '</option>');
            $.each(resCiti, function (i, option) {
                $("#ddlLeaveAnimalCity").append($('<option/>').attr("value", option.Id).text(option.Name));
            });
        });
        CallService("", "GET", "/api/AnimalShelter/" + CurrentLanguage, "", function (resCiti) {
            document.getElementById('DDLleaveFor').innerHTML = "";

            document.getElementById('seletedLeaveAnimalLleaveFor').innerHTML = ShelterTxt;
            $("#DDLleaveFor").append('<option value="" selected disabled>' + ShelterTxt + '</option>');
            $.each(resCiti, function (i, option) {
                $("#DDLleaveFor").append($('<option/>').attr("value", option.Id).text(option.Name));
            });
        });
        var LeaveAnimalGender = $("#ddlLeaveAnimalGender");
        document.getElementById("ddlLeaveAnimalGender").innerHTML = '';
        var seletedGender = $("#seletedLeaveAnimalGender");
        document.getElementById('seletedLeaveAnimalGender').innerHTML = DefultGender;
        fillGenderDDL(LeaveAnimalGender, seletedGender);
        if (localStorage.getItem('FacilityAddress')) {
            $('#divLeaveAnimalAddress').html(localStorage.getItem('FacilityAddress'));
        }

        if (initLeaveReques == true) {
            initLeaveReques = false;


            //$("#btnLeaveAnimalLocation").on('click', function () {
            //    var mapDiv = document.getElementById('map');
            //    var input = document.getElementById('pac-input');
            //    input = document.createElement('input');
            //    var divMapContainer = document.getElementById('divMapContainer');
            //    input.setAttribute('id', 'pac-input');
            //    input.setAttribute('placeholder', 'أدخل العنوان');
            //    input.setAttribute('type', 'text');
            //    input.className += 'controls';
            //    divMapContainer.insertBefore(input, mapDiv);
            //    mainView.router.loadPage({ pageName: 'mapPage', query: { fullAddress: '', addressLat: '', addressLong: '', fromPage: 'LeaveRequestPage' } });
            //});
            $("#UploadLeaveAnimalPhoto").on('click', function () {

                getLeaveAnimalImage();
            });

            $('input[type=radio][name=LeaveRequest_UploadAnimalVedio]').change(function () {
                $("#vedioGiveUpPage").css('display', 'block');
                $("#videoGiveUpPlayer").css('display', 'block');
                if (this.value == "0") {
                    captureGiveUpVideo(0);
                } else {
                    captureGiveUpVideo(1);
                }

            });

            $("#btnsaveLeaveRequest").on('click', function () {
                var AnimalType = $('#ddlLeaveAnimalType').val();
                var AnimalCity = $('#ddlLeaveAnimalCity').val();
                var AnimalGender = $('#ddlLeaveAnimalGender').val();
                var AnimalName = $('#txtLeaveAnimalName').val();
                var AnimalAge = $('#txtLeaveAnimalAge').val();
                var AnimalVaccine = $('#txtAnimalLeaveVaccine').val();
                var LeaveReason = $('#txtAnimalLeaveReason').val();
                var AnimalLocation = $('#divLeaveAnimalAddress').val();
                var leaveFor = $('#DDLleaveFor').val();
                var GiveUpLeave = $('#TxtGiveUpLeave').val();
                var AnimalBrief = $('#txtLeaveAnimalDescription').val();
                var notes = $("#LeaveAnimalNotes").val();
                var vedioUrl = $("#videoGiveUpPlayer").attr("src");
                FValidation.ValidateAll('LeaveAnimal', function (res) {
                    if (res == true) {
                        var user = localStorage.getItem('UserId');
                        var gender = true;
                        if ($('#ddlLeaveAnimalGender option:selected').val() == 0) {
                            gender = false;
                        }
                        var param = {
                            Name: AnimalName, Gendre: gender, Age: AnimalAge, Vaccinations: AnimalVaccine, Address: AnimalLocation, TypeId: 2, CityId: AnimalCity, CategoryId: AnimalType, Notes: notes, VedioUrl: vedioUrl
                        };
                        if (animalAdopImages.length > 0) {
                            CallService("LeaveRequest",
                                "POST",
                                "/api/Animal/save",
                                param,
                                function (animal) {
                                    if (animal != null && typeof animal.status == 'undefined') {
                                        var GiveUpParame =
                                        {
                                            AnimalBrief: AnimalBrief,
                                            GiveUpReason: GiveUpLeave,
                                            Address: AnimalLocation,
                                            ShelterId: leaveFor,
                                            AnimalId: animal,
                                            ApplicationUserId: user
                                        }
                                        CallService("LeaveRequest", "POST", "/api/GiveUpRequest/save", GiveUpParame,
                                            function (LeaveReq) {
                                                if (LeaveReq != null) {
                                                    myApp.alert(RequestAdded, '', function () {
                                                        mainView.router.loadPage({ pageName: 'index' });
                                                    });
                                                }
                                            });

                                        var animalImageParam = [];


                                        for (var i = 0; i < animalAdopImages.length; i++) {
                                            item = {
                                            }
                                            item["ImageUrl"] = animalAdopImages[i];
                                            item["AnimalId"] = animal;
                                            animalImageParam.push(item);

                                        }


                                        CallServiceJson("LeaveRequest", "POST", "/api/AnimalImage/SaveListOfImages", JSON.stringify(animalImageParam),
                                            function (res) {
                                                animalAdopImages = [];
                                                document.getElementById('imgLeaveAnimalContainerDiv').innerHTML = "";
                                            });


                                    } else {
                                        myApp.alert(NotAuthenticated, Wrong, function () {
                                            mainView.router.loadPage({ pageName: 'index' });
                                        });
                                    }
                                });
                        } else {
                            myApp.alert(OneImageAtLeast, Wrong, function () { });
                        }
                    }
                });
            });
            $("#btnCloseLeaveRequest").on('click', function () {

                mainView.router.loadPage({ pageName: 'index' });

            });
        }
    }
}
function GoTousersPage(page) {
    loadSideMenuLinks();

    if (typeof page != 'undefined') {
        var UsersmaxNumber = 0;
        var IsUsersLoading = true;
        var IsUsersFristLoading = true;
        var divUsers = document.getElementById('devUsers');
        var Indexobj = { "PageNumber": 1, "PageSize": 8, 'Language': CurrentLanguage };
        if (IsUsersFristLoading == true) {
            IsUsersFristLoading = false;
            CallService("users", "POST", "/api/Users/GetFilterdUsers", Indexobj, function (UsersRes) {
                if (UsersRes != null && UsersRes.length > 0) {
                    UsersmaxNumber = UsersRes[0].OverAllCount;
                    document.getElementById('devUsers').innerHTML = '';
                    if ($$('#devUsers div.anm-block').length != 0) {
                        document.getElementById('devUsers').innerHTML = '';
                    }
                    DrawUser(UsersRes, 0, divUsers, 8);
                    $('#devUsers').show();
                    $('#divNoUsers').hide();
                    if ($$('#devUsers div.anm-block').length < 8) {
                        $('.loading img').css('display', 'none');
                    } else {
                        $('.loading img').css('display', '');
                    }

                }
                else {

                    $('#devUsers').hide();
                    $('#divNoUsers').show();
                    $('.loading img').css('display', 'none');

                }
            });

        }


        if (initusers == true) {
            initusers = false;
            myApp.attachInfiniteScroll($$('#users-infinite-scroll'));
            $$('#users-infinite-scroll').on('infinite', function () {
                if ($$('#devUsers div.anm-block').length < UsersmaxNumber && IsUsersLoading == true) {
                    IsUsersLoading = false;
                    var pagingIndxobj = { "PageNumber": parseInt(parseInt($$('#devUsers div.anm-block').length / 8) + 1), "PageSize": 8, 'Language': CurrentLanguage };
                    CallService("users", "POST", "/api/Users/GetFilterdUsers", pagingIndxobj, function (PagingUserRes) {
                        if (PagingUserRes != null && PagingUserRes.length > 0) {
                            UsersmaxNumber = PagingUserRes[0].OverAllCount;

                            DrawUser(PagingUserRes, 0, divUsers, 8);
                            IsUsersLoading = true;
                            $('.loading img').css('display', '');
                        }
                        else {

                        }

                    });
                } else {
                    $('.loading img').css('display', 'none');
                }
            });
        }
    }
}
function GoToUserDetailsPage(page) {
    $(window).scrollTop(0);
    loadSideMenuLinks();


    if (typeof page != 'undefined') {
        $('#UserAnimalTab').removeClass('active');
        $('#UserInfTab').removeClass('active');
        $('#UserAnimalTab').addClass('active');
        myApp.showTab('#tab5');
        var reqUser = page.query.User;
        UserMobile = reqUser.PhoneNumber;
        UserMassageid = reqUser.Id;
        reporteduser = reqUser.Id;
        UserShowInforUser = reqUser.ShowInfo;
        var animals = [];
        var loading = false;
        var lastIndex = 8;
        var maxItems = animals.length;
        var itemsPerLoad = 8;
        scrollLoadsBefore = false;
        var divAnimalContainer = document.getElementById('AnimalsinUser');
        document.getElementById('divUserAnimalContainer').innerHTML = "";
        document.getElementById('divneedCareAnimalContainer').innerHTML = "";
        // document.getElementById('divAnimalContainer').innerHTML = "";
        //   document.getElementById('divLostAnimalContainer').innerHTML = "";
        document.getElementById('divRefugeContainer').innerHTML = "";
        var param = {
            'ApplicationUserId': reqUser.Id, 'PageNumber': 1, 'PageSize': 8, 'Language': CurrentLanguage
        }
        CallService("UserDetails", "POST", "/api/Animal/AnimalsByUser", param, function (animalList) {
            if (animalList != null && animalList.length > 0) {
                document.getElementById('AnimalsinUser').innerHTML = "";
                $('#AnimalsinUser').show();
                $('#divNoAnimalsinUserFound').hide();
                $('.loading img').css('display', '');

                animals = animalList;
                maxItems = animalList[0].OverAllCount;
                DrawUseranimal(animals, 0, divAnimalContainer, "", page.name);
                if (animals.length < itemsPerLoad) {
                    $('.loading img').css('display', 'none');
                }
                lastIndex = $$('#AnimalsinUser div.anm-block').length;
            } else {
                $('#AnimalsinUser').hide();
                $('#divNoAnimalsinUserFound').show();
                $('.loading img').css('display', 'none');
            }
        });
        $('#AnimalsinUser').hide();
        $('#divNoAnimalsinUserFound').show();
        $('.loading img').css('display', 'none');

        if (reqUser.PhotoUrl != null && reqUser.PhotoUrl != "") {
            document.getElementById("UserImg").src = reqUser.PhotoUrl;
        } else {
            document.getElementById("UserImg").src = "img/NoImg.jpg";
        }
        if (reqUser.Name != null && reqUser.Name != "") {
            document.getElementById("UserName").innerHTML = '<i class="ionicons ion-ios-person"></i>  ' + reqUser.Name;
        } else {
            document.getElementById("UserName").innerHTML = '<i class="ionicons ion-ios-person"></i> ' + notFound;

        }
        if (reqUser.Email != null && reqUser.Email != "") {
            document.getElementById("UserEmail")
                .innerHTML = '<i class="ionicons ion-android-mail"></i>  ' + reqUser.Email;
        } else {
            document.getElementById("UserEmail")
                .innerHTML = '<i class="ionicons ion-android-mail"></i>' + notFound;
        }
        if (reqUser.PhoneNumber != null && reqUser.PhoneNumber != "") {
            document.getElementById("userPhone")
                .innerHTML = '<i class="ionicons ion-android-phone-portrait"></i>  ' + reqUser.PhoneNumber;
        } else {
            document.getElementById("userPhone")
              .innerHTML = '<i class="ionicons ion-android-phone-portrait"></i>' + notFound;

        }
        if (reqUser.Age != null && reqUser.Age != "") {
            document.getElementById("userAge")
                .innerHTML = '<i class="ionicons ion-arrow-graph-up-right"></i> ' + reqUser.Age;
        } else {
            document.getElementById("userAge")
              .innerHTML = '<i class="ionicons ion-arrow-graph-up-right"></i>  ' + notFound;

        }

        if (!reqUser.ShowInfo) {
            $("#userPhone").css('display', 'none');
            $("#UserEmail").css('display', 'none');
            $("#userAge").css('display', 'none');
        } else {
            $("#userPhone").css('display', 'block');
            $("#UserEmail").css('display', 'block');
            $("#userAge").css('display', 'block');
        }
        myApp.attachInfiniteScroll($$('#tab5'));
        $$('#tab5').on('infinite', function () {

            lastIndex = $$('#AnimalsinUser div.anm-block').length;
            if (lastIndex >= maxItems || scrollLoadsBefore == true) {
                myApp.detachInfiniteScroll($$('#tab5'));
                $$('.infinite-scroll-preloader').remove();
                return;
            }
            if (loading) return;

            loading = true;

            setTimeout(function () {

                var param = {
                    'ApplicationUserId': reqUser.Id, 'PageNumber': parseInt(parseInt(lastIndex / 8) + 1), 'PageSize': 8, 'Language': CurrentLanguage
                }
                CallService("UserDetails", "POST", "/api/Animal/AnimalsByUser", param, function (animalList) {
                    if (animalList != null && animalList.length > 0) {
                        lastIndex = $$('#AnimalsinUser div.anm-block').length;
                        if (lastIndex >= maxItems || scrollLoadsBefore == true) {
                            myApp.detachInfiniteScroll($$('#tab5'));
                            $$('.infinite-scroll-preloader').remove();
                            return;
                        }

                        loading = false;
                        $('#AnimalsinUser').show();
                        $('#divNoAnimalsinUserFound').hide();
                        $('.loading img').css('display', '');
                        var animal = animalList;
                        maxItems = animalList[0].OverAllCount;

                        DrawUseranimal(animal, 0, divAnimalContainer, "", page.name);
                        if (animal.length < itemsPerLoad) {
                            $('.loading img').css('display', 'none');
                        }
                        lastIndex = $$('#AnimalsinUser div.anm-block').length;

                    } else {
                        $('#AnimalsinUser').hide();
                        $('#divNoAnimalsinUserFound').show();
                        $('.loading img').css('display', 'none');
                    }
                });
                $('.loading img').css('display', '');


            }, 1000);
        });
        if (inituserDetails == true) {
            inituserDetails = false;


            $("#UserAnimalTab").click(function () {
                $('#UserAnimalTab').removeClass('active');
                $('#UserAnimalTab').addClass('active');
                $('#UserInfTab').removeClass('active');
                myApp.showTab('#tab5');

            });
            $("#UserInfTab").click(function () {
                $('#UserInfTab').removeClass('active');
                $('#UserInfTab').addClass('active');
                $('#UserAnimalTab').removeClass('active');
                myApp.showTab('#tab6');
            });


            $("#btnUserChat").on("click",
             function () {
                 mainView.router.loadPage({ pageName: 'messages', query: { UserId: UserMassageid } });

             });

            $("#BtnCallUser").on("click",
                function () {


                    if (UserMobile != null) {
                        if (!UserShowInforUser) {
                            myApp.alert(NoNotAvilable, warning, function () { });
                        } else {
                        window.plugins.CallNumber.callNumber(onSuccess, onError, UserMobile, true);
                            
                        }
                    } else {
                        myApp.alert(NoNotAvilable, warning, function () { });
                    }

                    // window.plugins.CallNumber.callNumber(onSuccess, onError, reqUser.PhoneNumber, true);
                });
            $("#btnUserRepot").on("click",
                function () {

                    myApp.popup('#reportPopupPage');

                });
        }
    }
}
function GoToreportPopupPage(page) {
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        $("#ReportDescription").val("");
        $("#lblReportDescriptionRequired").css('display', 'none');

        if (initreportPopupPage == true) {
            initreportPopupPage = false;
            $("#btnSubmitreport").on('click',
                function () {
                    var value = $("#ReportDescription").val();
                    FValidation.ValidateAll('ReportPopup', function (res) {

                        if (res == true) {
                            if (localStorage.getItem('UserId') != null) {
                                if (reporteduser != localStorage.getItem('UserId')) {
                                    var param = { 'Description': value, 'ReportedUserId': reporteduser }
                                    CallService("reportPopup", "POST", "/api/BadUseRequest/save", param, function (res) {
                                        if (res != null && typeof res.status == 'undefined') {
                                            myApp.alert(reportAddedSucessFully, '', function () {
                                                myApp.closeModal('#reportPopupPage');
                                            });
                                        } else {
                                            myApp.alert(NotAuthenticated, Wrong, function () {
                                                myApp.closeModal('#reportPopupPage');
                                            });
                                        }

                                    });
                                } else {
                                    myApp.alert(NotAuthenticatedreport, Wrong, function () {
                                        myApp.closeModal('#reportPopupPage');
                                    });
                                }
                            } else {
                                myApp.alert(NotAuthenticated, Wrong, function () {
                                    myApp.closeModal('#reportPopupPage');
                                });
                            }
                        }

                    });
                });

            $("#btnClosereport").on('click',
                function () {
                    $("#ReportDescription").val('');
                    myApp.closeModal('#reportPopupPage');
                });
        }
    }
}
function GoToAddAnimalPage(page) {
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        document.getElementById('divAnimalContainer').innerHTML = '';
        $("#lblAnimalTypeRequired").css('display', 'none');
        $("#lblAnimalCityRequired").css('display', 'none');
        $("#lblAnimalGenderRequired").css('display', 'none');
        $("#lblAgeAllowedLength").css('display', 'none');
        $("#lblAnimalAddressRequired").css('display', 'none');
        $("#lblAgeNumberOnly").css('display', 'none');
        ClearAddAnimalData();
        CallService("", "GET", "/api/Category/GetCategories/" + CurrentLanguage, "", function (resCiti) {
            document.getElementById('ddlAnimalType').innerHTML = "";
            document.getElementById('selectedAnimalType').innerHTML = AnimalCategory;
            $("#ddlAnimalType").append('<option value="" selected disabled>' + AnimalCategory + '</option>');
            $.each(resCiti, function (i, option) {
                $("#ddlAnimalType").append($('<option/>').attr("value", option.Id).text(option.Name));
            });
        });
        CallService("", "GET", "/api/City/GetAllCities/" + CurrentLanguage, "", function (resCiti) {
            document.getElementById('ddlAnimalCity').innerHTML = "";
            document.getElementById('selectedAnimalCity').innerHTML = City;
            $("#ddlAnimalCity").append('<option value="" selected disabled>' + City + '</option>');
            $.each(resCiti, function (i, option) {
                $("#ddlAnimalCity").append($('<option/>').attr("value", option.Id).text(option.Name));
            });
        });
        var currentPage = page.fromPage.name;
        if (currentPage == "needCareAnimals") {
            document.getElementById('AddAnimalPageTitle').innerHTML = AddAnimalTitleEmergancy;
            document.getElementById('BtnAddAnimalToAdaption').innerHTML = AddAnimalReportBtn;
            animalType = 3;
            PageNameAddanmial = "needCareAnimals";
        }
        else if (page.fromPage.name == "lostAnimals") {
            document.getElementById('AddAnimalPageTitle').innerHTML = AddAnimalTitleLost;
            document.getElementById('BtnAddAnimalToAdaption').innerHTML = AddAnimalsubmitBtn;
            animalType = 4;
            PageNameAddanmial = "lostAnimals";
        }
        else if (page.fromPage.name == "animalDetails") {
            var animalId = page.query.AnimalId;
            AnimalEdit(animalId);
        }
        else {
            document.getElementById('AddAnimalPageTitle').innerHTML = AddAnimalTitleAboption;
            document.getElementById('BtnAddAnimalToAdaption').innerHTML = AddAnimalsubmitBtn;
            animalType = 1;
            PageNameAddanmial = "index";
        }


        var ddlddlAnimalGender = $("#ddlAnimalGender");
        document.getElementById('ddlAnimalGender').innerHTML = '';
        var ddlddlAnimalGendertxt = $("#selectedAnimalGender");
        document.getElementById('selectedAnimalGender').innerHTML = '';
        document.getElementById('selectedAnimalGender').innerHTML = DefultGender;
        fillGenderDDL(ddlddlAnimalGender, ddlddlAnimalGendertxt);
        if (localStorage.getItem('FacilityAddress')) {
            $('#divAnimalAddress').html(localStorage.getItem('FacilityAddress'));
        }
        if (initaddAnimal == true) {
            initaddAnimal = false;
            $("#UploadAnimalPhoto").on('click', function () {
                getImage();
            });
            $('input[type=radio][name=my-radio1_UploadAnimalVedio]').change(function () {
                $("#vedioPage").css('display', 'block');
                $("#myProgress").css('display', 'block');
                if (this.value == "0") {
                    captureVideo(0);
                } else {
                    captureVideo(1);
                }

            });
            $("#AddAnimalBack").on('click', function () {
                mainView.router.loadPage({ pageName: PageNameAddanmial });

            });
            $("#BtnAddAnimalToAdaption").on('click', function () {
                var user = localStorage.getItem('UserId');
                var id = $("#txtAnimalId").val();
                var age = $('#txtAnimalAge').val();
                var name = $("#txtAnimalName").val();
                var vaccine = $("#txtAnimalVaccine").val();
                var location = $("#divAnimalAddress").val();
                var city = $("#ddlAnimalCity").val();
                var category = $("#ddlAnimalType").val();
                var notes = $("#ddlAnimalNotes").val();
                var vedioUrl = $("#videoPlayer").attr("src");
                FValidation.ValidateAll('AddAnimal', function (res) {
                    if (res === true) {
                        var gender = true;
                        if ($('#ddlAnimalGender option:selected').val() == 0) {
                            gender = false;
                        }
                        if (typeof age == 'undefined' || age === "" || age === '') {
                            age = 0;
                        }
                        var IdOfAnimal;
                        if (id.trim() != "") {
                            IdOfAnimal = parseInt(id);
                        } else {
                            IdOfAnimal = 0;
                        }

                        var parame = {
                            Id: IdOfAnimal,
                            Name: name, Gendre: gender, Age: parseInt(age), Vaccinations: vaccine, Address: location
                            , TypeId: animalType, CityId: parseInt(city), CategoryId: parseInt(category), Notes: notes, VedioUrl: vedioUrl
                        };
                        if (animalAdopImages != null && animalAdopImages.length > 0) {
                            CallService("AddAnimal", "POST", "/api/Animal/save", parame, function (res) {
                                if (res != null && typeof res.status == 'undefined') {
                                    var animalImageParam = [];

                                    for (var i = 0; i < animalAdopImages.length; i++) {
                                        var item = {};
                                        item["ImageUrl"] = animalAdopImages[i];
                                        item["AnimalId"] = res;
                                        animalImageParam.push(item);
                                    }


                                    CallServiceJson("AddAnimal", "POST", "/api/AnimalImage/SaveListOfImages", JSON.stringify(animalImageParam), function (res) {
                                        ClearAddAnimalData();
                                        if (IdOfAnimal != 0) {
                                            myApp.alert(AnimalEditSucss, '', function () {
                                                mainView.router.loadPage({ pageName: 'index' });
                                            });
                                        } else {
                                            myApp.alert(AnimalAdded, '', function () {
                                                mainView.router.loadPage({ pageName: 'index' });
                                            });
                                        }


                                    });
                                }
                                else if (res.status == 401) {
                                    myApp.alert(NotAuthenticated, Wrong, function () { });
                                } else {
                                    myApp.alert(ErrorOccured, Wrong, function () { });
                                }
                            });
                        } else {
                            myApp.alert(OneImageAtLeast, Wrong, function () { });
                        }
                    }
                });
            });
        }

    }
}
function GoTolostAnimalsPage(page) {
    loadSideMenuLinks();

    if (typeof page != 'undefined') {
        var LostmaxNumber = 0;
        var IsLostLoading = true;
        var IsLostFristLoading = true;
        var divLostAnimalContainer = document.getElementById('divLostAnimalContainer');
        var pagingobj = { "PageNumber": 1, "PageSize": 8, "TypeId": 4, 'Language': CurrentLanguage };
        if (IsLostFristLoading == true) {
            IsLostFristLoading = false;
            CallService("lostAnimals", "POST", "/api/Animal/GetAnimls", pagingobj, function (LostRes) {
                if (LostRes != null && LostRes.length > 0) {
                    LostmaxNumber = LostRes[0].OverAllCount;
                    document.getElementById('divLostAnimalContainer').innerHTML = '';
                    if ($$('#divLostAnimalContainer div.anm-block').length != 0) {
                        document.getElementById('divLostAnimalContainer').innerHTML = '';
                    }

                    DrawanimalInHome(LostRes, 0, divLostAnimalContainer, 8, "", page.name);
                    $('#divLostAnimalContainer').show();
                    $('#divNoLostAnimalFound').hide();
                    if ($$('#divLostAnimalContainer div.anm-block').length < 8) {
                        $('.loading img').css('display', 'none');
                    } else {
                        $('.loading img').css('display', '');
                    }

                }
                else {

                    $('#divLostAnimalContainer').hide();
                    $('#divNoLostAnimalFound').show();
                    $('.loading img').css('display', 'none');

                }
            });

        }


        if (initlostAnimals == true) {
            initlostAnimals = false;
            myApp.attachInfiniteScroll($$('#lost-animals-infinite-scroll'));
            $$('#lost-animals-infinite-scroll').on('infinite', function () {
                if ($$('#divLostAnimalContainer div.anm-block').length < LostmaxNumber && IsLostLoading == true) {
                    IsLostLoading = false;

                    var pagingIndxobj = { "PageNumber": parseInt(parseInt($$('#divLostAnimalContainer div.anm-block').length / 8) + 1), "PageSize": 8, "TypeId": 4, 'Language': CurrentLanguage };
                    CallService("lostAnimals", "POST", "/api/Animal/GetAnimls", pagingIndxobj, function (PLostRes) {
                        if (PLostRes != null && PLostRes.length > 0) {
                            LostmaxNumber = PLostRes[0].OverAllCount;
                            DrawanimalInHome(PLostRes, 0, divLostAnimalContainer, 8, "", page.name);
                            IsLostLoading = true;
                            $('.loading img').css('display', '');
                        }
                        else {

                        }

                    });
                } else {
                    $('.loading img').css('display', 'none');
                }
            });
            $("#addlostAnimal").on('click', function () {
                mainView.router.loadPage({ pageName: 'addAnimal', query: { prevPage: 'lostAnimals' } });

            });
        }
    }
}
function GoToneedCareAnimalsPage(page) {
    loadSideMenuLinks();

    if (typeof page != 'undefined') {
        var NeedCaremaxNumber = 0;
        var IsNeedCareLoading = true;
        var IsNeedCareFristLoading = true;
        var divneedCareAnimalContainer = document.getElementById('divneedCareAnimalContainer');
        var Indexobj = { "PageNumber": 1, "PageSize": 8, "TypeId": 3, 'Language': CurrentLanguage };
        if (IsNeedCareFristLoading == true) {
            IsNeedCareFristLoading = false;
            CallService("needCareAnimals", "POST", "/api/Animal/GetAnimls", Indexobj, function (NeedCareRes) {
                if (NeedCareRes != null && NeedCareRes.length > 0) {
                    NeedCaremaxNumber = NeedCareRes[0].OverAllCount;
                    document.getElementById('divneedCareAnimalContainer').innerHTML = '';
                    if ($$('#divneedCareAnimalContainer div.anm-block').length != 0) {
                        document.getElementById('divneedCareAnimalContainer').innerHTML = '';
                    }
                    DrawanimalInHome(NeedCareRes, 0, divneedCareAnimalContainer, 8, "", page.name);
                    $('#divneedCareAnimalContainer').show();
                    $('#divNoneedCareFound').hide();
                    if ($$('#divneedCareAnimalContainer div.anm-block').length < 8) {
                        $('.loading img').css('display', 'none');
                    } else {
                        $('.loading img').css('display', '');
                    }

                }
                else {

                    $('#divneedCareAnimalContainer').hide();
                    $('#divNoneedCareFound').show();
                    $('.loading img').css('display', 'none');

                }
            });

        }


        if (initneedCareAnimals == true) {
            initusinitneedCareAnimals = false;
            myApp.attachInfiniteScroll($$('#need-care-animals-infinite-scroll'));
            $$('#need-care-animals-infinite-scroll').on('infinite', function () {
                if ($$('#divneedCareAnimalContainer div.anm-block').length < NeedCaremaxNumber && IsNeedCareLoading == true) {
                    IsNeedCareLoading = false;
                    var pagingIndxobj = { "PageNumber": parseInt(parseInt($$('#divneedCareAnimalContainer div.anm-block').length / 8) + 1), "PageSize": 8, "TypeId": 3, 'Language': CurrentLanguage };
                    CallService("needCareAnimals", "POST", "/api/Animal/GetAnimls", pagingIndxobj, function (needCareRes) {
                        if (needCareRes != null && needCareRes.length > 0) {
                            NeedCaremaxNumber = needCareRes[0].OverAllCount;
                            DrawanimalInHome(needCareRes, 0, divneedCareAnimalContainer, 8, "", page.name);
                            IsNeedCareLoading = true;
                            $('.loading img').css('display', '');
                        }
                        else {

                        }

                    });
                } else {
                    $('.loading img').css('display', 'none');
                }
            });
            $("#needCareAnimalLink").on('click', function () {
                mainView.router.loadPage({ pageName: 'addAnimal', query: { prevPage: 'needCareAnimal' } });
            });
        }
    }
}
function GoTosuccessStoryPage(page) {
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        var divSuccessStoryContainer = document.getElementById('divSuccessStoryContainer');
        var storylastIndex = $$('#divSuccessStoryContainer div.StoryBlock').length;
        var storymaxNumber = 0;
        var isLoadingstory = true;
        var isFristLoadingstory = true;



        var pagingobj = {
            "PageNumber": 1, "PageSize": 8
        }
        if (isFristLoadingstory == true) {
            isFristLoadingstory = false;
            CallService("successStory", "POST", "/api/SucessStory/GetSucessStoryFiltered", pagingobj, function (storyList) {
                if (storyList != null && storyList.length > 0) {
                    storymaxNumber = storyList[0].OverAllCount;
                    document.getElementById('divSuccessStoryContainer').innerHTML = "";
                    $('#divNoSuccessStory').hide();
                    if ($$('#divSuccessStoryContainer div.StoryBlock').length != 0) {
                        document.getElementById('divSuccessStoryContainer').innerHTML = '';
                    }
                    DrawSuccessStory(storyList, 0, divSuccessStoryContainer, 8);
                    $('#divSuccessStoryContainer').show();
                    $('#divNoSuccessStory').hide();
                    if ($$('#divSuccessStoryContainer div.StoryBlock').length < 8) {
                        $('.loading img').css('display', 'none');
                    } else {
                        $('.loading img').css('display', '');
                    }
                } else {

                    $('#divSuccessStoryContainer').hide();
                    $('#divNoSuccessStory').show();
                    $('.loading img').css('display', 'none');

                }

            });
        }

        if (initsuccessStory == true) {
            initsuccessStory = false;
            myApp.attachInfiniteScroll($$('#success-story-infinite-scroll'));
            $$('#success-story-infinite-scroll').on('infinite', function () {
                if ($$('#divSuccessStoryContainer  div.StoryBlock').length < storymaxNumber && isLoadingstory == true && $$('#divSuccessStoryContainer  div.StoryBlock').length >= 8) {
                    isLoadingstory = false;
                    var pagingobj = {
                        'PageNumber': parseInt(parseInt($$('#divSuccessStoryContainer div.StoryBlock').length / 8) + 1),
                        'PageSize': 8
                    };
                    CallService("successStory", "POST", "/api/SucessStory/GetSucessStoryFiltered", pagingobj, function (storyList) {
                        if (storyList != null && storyList.length > 0) {
                            storymaxNumber = storyList[0].OverAllCount;
                            DrawSuccessStory(storyList, 0, divSuccessStoryContainer, 8);
                            isLoadingstory = true;
                            $('.loading img').css('display', '');

                        } else {

                        }
                    });
                } else {
                    $('.loading img').css('display', 'none');
                }

            });
        }


    }
}
function GoTostoryDetailsPage(page) {
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        var reqStory = page.query.Story;
        StoryIdComent = reqStory.Id;
        LoadCommentsStory(reqStory.Id);
        reporteduser = reqStory.AuthorId;

        if (reqStory.ImageUrl != null && reqStory.ImageUrl != "") {
            document.getElementById("StoryImage").src = reqStory.ImageUrl;
        } else {
            document.getElementById("StoryImage").src = "img/NoImg.jpg";
        }


        document.getElementById("StoryPerson").innerHTML = '<i class="ionicons ion-person"></i>  ' + reqStory.AuthorName;
        document.getElementById("storyTitle").innerHTML = '<i class="ionicons ion-android-create"></i>  ' + reqStory.Title;
        document.getElementById("StoryDetails").innerHTML = reqStory.details;
        if (reqStory.VedioUrl != null) {
            $("#ShowStoryvedioDiv").css('display', 'block');
            document.getElementById("ShowStoryvedio").src = reqStory.VedioUrl

        } else {
            $("#ShowStoryvedioDiv").css('display', 'none');
            document.getElementById("ShowStoryvedio").src = "";
        }


        if (reporteduser != null && localStorage.getItem("UserId") != null && reporteduser == localStorage.getItem("UserId")) {
            $("#DeleteStory").css('display', 'block');
        } else {

            $("#DeleteStory").css('display', 'none');
        }

        if (initStoryDetailsPage == true) {
            initStoryDetailsPage = false;

            $("#btnShareStory").on("click", function () {
                window.plugins.socialsharing.share('رفق', null, null, 'http://rfq-sa.com/SuccessStory/SuccessStory?id=' + StoryIdComent + '&lang=' + CurrentLanguage);
                //window.plugins.socialsharing.share('رفق', null, null, 'https://play.google.com/store/apps');
            });

            $("#btnStoryComment").on("click", function () {
                myApp.popup('#commentStoryPopupPage');
            });


            $("#btn_reportAuther").on('click',
                function () {
                    myApp.popup('#reportPopupPage');

                });


            $("#StoryImage").error(function (e) {
                document.getElementById("StoryImage").src = "img/NoImg.jpg";
            });
            $("#DeleteStory").on('click', function () {
                navigator.notification.confirm(
                    DeleteStoryConfirmMsg,
                    function (buttonIndex) {
                        if (buttonIndex == 1) {
                            CallService("storyDetails", "Delete", "/api/SucessStory/delete/" + StoryIdComent, "", function (story) {
                                myApp.alert(SucessStoryDelete, Sucess, function () { });
                                mainView.router.loadPage({ pageName: 'successStory' });
                            });
                        }
                    },
                    warning,
                    [Confirm, Cancel]
                );
            });
        }
    }
}
function GoToMapPage(page) {
    $(window).scrollTop(0);
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        var Lat = page.query.addressLat;
        var Long = page.query.addressLong;
        var Address = page.query.fullAddress;
        var fromPage = page.query.fromPage;
        markers = [];
        if (Address != '' && Lat != '' && Long != '') {
            markers = [
             {
                 "title": Address,
                 "lat": Lat,
                 "lng": Long,
                 "description": ''
             }];
        }
        initMap(markers, fromPage, '', function (map) {

            $$('body').on('touchend', '.pac-item', function (e) {
                var selectedAddress = $(this).text();
                InitMapSearchBox(map, markers, selectedAddress);
            });

            $('#btnSelectAddress').on('click', function () {
                ClearBodyAfterGoogleMap();
                if (fromPage == "LeaveRequestPage") {
                    GoToLeaveRequestPage()
                } else {
                    mainView.router.loadPage({ pageName: fromPage });
                }
            });
        });
    }
}
function GoToadoptedRequestPopup(page) {
    $("#adoptedRequestPopupPage").scrollTop(0);
    ClearAdoptionRequestData();
    loadSideMenuLinks();
    if (typeof page != 'undefined') {


        if (initadoptedRequestPopup == true) {
            initadoptedRequestPopup = false;

            $("#btnSaveAdopteRequest").on("click", function () {
                FValidation.ValidateAll('adoptedRequest',
                    function (res) {
                        if (res == true) {
                            var animal = JSON.parse(AdoptionRequestAnimal);
                            //var animalOwnership = $('#adoptedRequestAnimalOwnership').val();
                            //var raisingHistory = $('#adoptedRequestRaisingHistory').val();
                            //var adoptedReason = $('#adoptedRequestReason').val();
                            //var notes = $('#adoptedRequestNotes').val();
                            //var isSteriled = '';
                            //if ($('#IsSteriledText').val().trim() != "")
                            //    isSteriled = $('input[name=my-radio_IsSteriled]:checked').val() + " : " + $('#IsSteriledText').val();
                            //else
                            //    isSteriled = $('input[name=my-radio_IsSteriled]:checked').val();
                            //var livingPlace = '';
                            //if ($('#LivingPlaceText').val().trim() != "")
                            //    livingPlace = $('input[name=my-radio1_LivingPlace]:checked').val() + " : " + $('#LivingPlaceText').val();
                            //else
                            //    livingPlace = $('input[name=my-radio1_LivingPlace]:checked').val();
                            //var obj = { 'AnimalId': animal.Id, 'Reason': adoptedReason, 'RaisingHistory': raisingHistory, 'AnimalOwnership': animalOwnership, 'IsSteriled': isSteriled, 'LivingPlace': livingPlace, 'Notes': notes }
                            var fullName = $('#adoptedRequestFullName').val();
                            var mobile = $('#adoptedRequestMobile').val();
                            var address = $('#adoptedRequestAddress').val();
                            var age = $('#adoptedRequestAge').val();
                            var email = $('#adoptedRequestEmail').val();
                            var socialStatus = $('#adoptedRequestSocialStatus').val();
                            // var nationalId = $('#adoptedRequestNationalId').val();
                            var question1 = $('#adoptedRequestQuestion1').val();
                            var question2 = $('#adoptedRequestQuestion2').val();
                            var question3 = $('input[name=radio_adoptedRequestQuestion3]:checked').val();
                            var question4 = $('input[name=radio_adoptedRequestQuestion4]:checked').val();
                            var question5 = $('input[name=radio_adoptedRequestQuestion5]:checked').val();
                            var question6 = $('#adoptedRequestQuestion6').val();
                            var question7 = $('input[name=radio_adoptedRequestQuestion7]:checked').val();
                            var question8 = $('#adoptedRequestQuestion8').val();
                            var question9 = $('input[name=radio_adoptedRequestQuestion9]:checked').val();
                            var question10 = $('#adoptedRequestQuestion10').val();
                            var question11 = $('#adoptedRequestQuestion11').val();
                            var question12 = $('#adoptedRequestQuestion12').val();
                            var question13 = $('input[name=radio_adoptedRequestQuestion13]:checked').val();
                            var question21 = $('input[name=radio_adoptedRequestQuestion21]:checked').val();
                            var question22 = $('input[name=radio_adoptedRequestQuestion22]:checked').val();
                            var question23;
                            if ($('#Question23Text').val().trim() !== "")
                                question23 = $('input[name=radio_adoptedRequestQuestion23]:checked').val() + " : " + $('#Question23Text').val();
                            else
                                question23 = $('input[name=radio_adoptedRequestQuestion23]:checked').val();

                            var question24 = $('input[name=radio_adoptedRequestQuestion24]:checked').val();
                            var question25 = $('#adoptedRequestQuestion25').val();
                            var question26 = $('input[name=radio_adoptedRequestQuestion26]:checked').val();
                            var question27 = $('#adoptedRequestQuestion27').val();

                            var obj = {
                                'AnimalId': animal.Id, 'FullName': fullName, 'Mobile': mobile,
                                'Address': address, 'Age': age, 'Email': email, 'SocialStatus': socialStatus, 'Question1': question1,
                                'Question2': question2, 'Question3': question3, 'Question4': question4, 'Question5': question5, 'Question6': question6,
                                'Question7': question7, 'Question8': question8, 'Question9': question9, 'Question10': question10, 'Question11': question11,
                                'Question12': question12, 'Question13': question13, 'Question21': question21,
                                'Question22': question22, 'Question23': question23, 'Question24': question24, 'Question25': question25, 'Question26': question26,
                                'Question27': question27, 'IsApproved': null
                            }

                            CallService("adoptedRequestPopup", "POST", "/api/AdoptionRequest/save", obj, function (res) {
                                if (res != null && typeof res.status == 'undefined') {

                                    myApp.alert(RequestAdded, '', function () {
                                        ClearAdoptionRequestData();
                                        mainView.router.loadPage({ pageName: 'index' });
                                    });
                                } else if (res.status == 401) {
                                    myApp.alert(NotAuthenticated, Wrong, function () { });

                                } else {
                                    myApp.alert(ErrorOccured, Wrong, function () { });
                                }
                            });
                        }
                    });

            });

            $("#btncloseAdopteRequest").on("click",
                function () {
                    ClearAdoptionRequestData();
                    mainView.router.loadPage({ pageName: 'animalDetails' });
                });



            $('input[type=radio][name=radio_adoptedRequestQuestion23]').change(function () {
                if (this.innerText == YES) {
                    $("#Question23TextDiv").css("display", "block");
                } else {
                    $("#Question23TextDiv").css("display", "none");
                }

            });
            //$('input[type=radio][name=my-radio1_LivingPlace]').change(function () {
            //    if (this.innerText == "اسباب اخرى" || this.innerText == "Other") {
            //        $("#LivingPlaceTextDiv").css("display", "block");
            //    } else {
            //        $("#LivingPlaceTextDiv").css("display", "none");
            //    }
            //});
            //$('input[type=radio][name=my-radio_IsSteriled]').change(function () {
            //    if (this.innerText == "غير ذلك" || this.innerText == "Other") {
            //        $("#IsSteriledTextDiv").css("display", "block");
            //    } else {
            //        $("#IsSteriledTextDiv").css("display", "none");
            //    }
            //});
        }
    }
}
function GoToCommentPopupPage(page) {
    $(window).scrollTop(0);
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        if (initcommentPopup == true) {
            initcommentPopup = false;
            $("#Btn_AddCommect").on('click', function () {
                var value = $("#txtComment").val();
                if (value != null && value != '') {
                    var param = {
                        'Value': value, 'AnimalId': AnimalIdComent
                    }
                    CallService("commentPopup",
                        "POST",
                        "/api/Comment/save",
                        param,
                        function (res) {
                            if (res != null && typeof res.status == 'undefined') {
                                myApp.alert(CommentAdded, '', function () { });
                                myApp.closeModal('#commentPopupPage');
                                $("#txtComment").val("");
                                document.getElementById('CommentsDiv').innerHTML = '';
                                LoadComments(AnimalIdComent);

                            } else if (res.status == 401) {
                                myApp.alert(NotAuthenticated, Wrong, function () { });

                            } else {
                                myApp.alert(ErrorOccured, Wrong, function () { });
                            }

                        });

                } else {
                    myApp.alert(FiledsEmpty, Wrong, function () { });
                }

            });

            $("#Btn_CloseCommect").on('click',
                        function () {
                            $("#txtComment").val('');
                            myApp.closeModal('#commentPopupPage');
                        });
        }
    }
}
function GoToCommentStoryPopupPage(page) {
    $(window).scrollTop(0);
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        if (initcommentPopup == true) {
            initcommentPopup = false;
            $("#Btn_AddCommectStory").on('click', function () {
                var value = $("#txtCommentStory").val();
                if (value != null && value != '') {
                    var param = {
                        'Value': value, 'SucessStoryId': StoryIdComent
                    }
                    CallService("commentStoryPopup",
                        "POST",
                        "/api/Comment/save",
                        param,
                        function (res) {
                            if (res != null && typeof res.status == 'undefined') {
                                myApp.alert(CommentAdded, '', function () { });
                                myApp.closeModal('#commentStoryPopupPage');
                                $("#txtCommentStory").val("");
                                document.getElementById('CommentsStoryDiv').innerHTML = '';
                                LoadCommentsStory(StoryIdComent);

                            } else {
                                myApp.alert(NotAuthenticated, Wrong, function () { });
                            }

                        });

                } else {
                    myApp.alert(FiledsEmpty, Wrong, function () { });
                }

            });

            $("#Btn_CloseCommectStory").on('click',
                        function () {
                            $("#txtCommentStory").val('');
                            myApp.closeModal('#commentStoryPopupPage');
                        });
        }
    }
}
function GoTochatPage(page) {
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        var resMessage;
        if (typeof page.query.massage != 'undefined') {
            resMessage = page.query.massage;
            chatId = resMessage.Id;
        }
        else {
            chatId = 0;
        }
        LoadMassage(animalMassageid, chatId);
        document.getElementById('MassagesDiv').innerHTML = '';
        if (initChat == true) {
            initChat = false;
            $("#btn_sendMSG").on('click', function () {
                var value = $("#txtMassage").val();

                if (value != null && value != '') {
                    var param = {
                        'Value': value, 'AnimalId': animalMassageid, 'Id': chatId
                    }
                    CallService("chat", "POST", "/api/Message/SaveChat", param, function (res) {
                        if (res != null && typeof res.status == 'undefined') {
                            //myApp.alert('تم ارسال الرساله بنجاح', '', function () { });
                            $("#txtMassage").val("");
                            document.getElementById('MassagesDiv').innerHTML = '';
                            chatId = res;
                            LoadMassage(animalMassageid, chatId);

                        } else {
                            myApp.alert(NotAuthenticated, Wrong, function () { });
                        }
                    });

                }

            });
        }

    }
}
function GoTomessagesPage(page) {

    loadSideMenuLinks();
    if (typeof page != 'undefined') {
      
        if (typeof page.query.UserChat != 'undefined') {
            if (page.query.UserChat == 1) {
                LoadAllMessages(null,null);
            } else {
                LoadAllMessages(null, page.query.UserChat);
            }
              $("#Btn_NewChat").css('display', 'none');
            
          
        } else {
            $("#Btn_NewChat").css('display', 'block');
            LoadAllMessages(animalMassageid);
 
        }
       
        $("#Btn_NewChat").on('click',
            function () {
                mainView.router.loadPage({ pageName: 'chat', query: { animalId: animalMassageid } });
            });
    }

}
function GoTochangePasswordPage(page) {
    $(window).scrollTop(0);
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        if (initchangePassword == true) {
            initchangePassword = false;
            $("#btnChangePassword").on('click', function () {
                FValidation.ValidateAll('changePassword', function (res) {
                    if (res == true) {
                        var params = {
                            'oldPassword': $('#txtChangeOldPassword').val(),
                            'newPassword': $('#txtChangeNewPassword').val(),
                            'ConfirmPassword': $('#txtChangeConfirmNewPassword').val()
                        }

                        CallService('changePassword', "POST", "/Api/Account/ChangePassword", params, function (res) {
                            if (res != null && typeof res.status == 'undefined') {
                                sginOut();
                                $('#txtChangeOldPassword').val('');
                                $('#txtChangeNewPassword').val('');
                                $('#txtChangeConfirmNewPassword').val('');
                                myApp.alert(PasswordChanged, Sucess, function () { mainView.router.loadPage({ pageName: 'login' }); });
                            }
                            else {
                                $('#txtChangeOldPassword').val('');
                                $('#txtChangeNewPassword').val('');
                                $('#txtChangeConfirmNewPassword').val('');
                                myApp.alert(WrongOldPassWord, Wrong, function () { });
                            }
                        });


                    }
                });
            });
            $("#btnPasswordBackToHome").on('click', function () {
                $('#txtChangeOldPassword').val('');
                $('#txtChangeNewPassword').val('');
                $('#txtChangeConfirmNewPassword').val('');
                mainView.router.loadPage({ pageName: 'index' });
            });

        }
    }

}
function GoTosearchPage(page) {
    $(window).scrollTop(0);
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        $$(".smart-select .item-after").html("");
        $$("#selectedSearchCatName").html(AnimalCategory);
        $("#txt_SearchAnimalName").val("");
        $("#txt_SearchOwnerName").val("");


        CallService("search", "GET", "/api/Category/GetCategories/" + CurrentLanguage, "", function (resCiti) {
            document.getElementById('ddl_SearchCatName').innerHTML = "";
            $("#ddl_SearchCatName").append('<option value="" selected disabled>' + AnimalCategory + '</option>');
            $.each(resCiti,
                function (i, option) {
                    $("#ddl_SearchCatName").append($('<option/>').attr("value", option.Id).text(option.Name));
                });
        });

        if (initSearch == true) {
            initSearch = false;


            $("#btn_search").on('click',
                function () {

                    //var searchCatId = $("#ddl_SearchCatName").val();
                    //var searchAnimalName = $("#txt_SearchAnimalName").val();
                    //var ownerName = $("#txt_SearchOwnerName").val();

                    searchCatId = $("#ddl_SearchCatName").val();
                    animalName = $("#txt_SearchAnimalName").val();
                    ownerName = $("#txt_SearchOwnerName").val();

                    var params = {
                        'CategoryId': searchCatId, 'AnimalName': animalName, 'OwnerName': ownerName, 'title': SearchResultTitle
                    }
                    //  myApp.closeModal('#searchPage');
                    $$(".smart-select .item-after").html("");
                    $$("#selectedSearchCatName").html(AnimalCategory);


                    mainView.router.loadPage({ pageName: 'searchResult' });
                    //GoTosearchResultPage(params);
                });
            //      $("btn_closeSearch").on('click', function () {
            // mainView.router.loadPage({ pageName: 'index' });
            //   myApp.closeModal('#searchPage');
            // });


        }
    }

}
function GoToaddSuccessStoryPage(page) {
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        document.getElementById("SucessImgUploaded").src = '';
        if (initaddSuccessStory == true) {
            initaddSuccessStory = false;
            $("#btn_AddSucessStory").on('click',
                   function () {
                       FValidation.ValidateAll('addSuccessStory',
                           function (res) {
                               if (res == true) {
                                   var imgUploaded = document.getElementById("SucessImgUploaded").src;
                                   var sucessTitle = $("#txt_SucessTitle").val();
                                   var sucessDetails = $("#txt_SucessDetails").val();
                                   var vedioUrl = $("#videoStoryPlayer").attr("src");
                                   var params = {
                                       'Title': sucessTitle, 'Details': sucessDetails, 'ImageUrl': imgUploaded, VedioUrl: vedioUrl
                                   }
                                   CallService("addSuccessStory", "POST", "/api/SucessStory/save", params, function (res) {
                                       if (res != null && typeof res.status == 'undefined') {
                                           $("#txt_SucessTitle").val('');
                                           $("#txt_SucessDetails").val('');
                                           $("#StoryvedioPage").css('display', 'none');
                                           document.getElementById("SucessImgUploaded").src = '';
                                           document.querySelector("#videoStoryPlayer").src = '';
                                           myApp.alert(storyAdded, Sucess, function () {
                                               mainView.router.loadPage({ pageName: 'successStory' });
                                           });

                                       } else if (res.status == 401) {
                                           $("#txt_SucessTitle").val('');
                                           $("#txt_SucessDetails").val('');
                                           $("#StoryvedioPage").css('display', 'none');
                                           document.getElementById("SucessImgUploaded").src = '';
                                           document.querySelector("#videoStoryPlayer").src = '';
                                           myApp.alert(NotAuthenticated, Wrong, function () { });
                                       } else {
                                           myApp.alert(ErrorOccured, Wrong, function () { });
                                       }
                                   });
                               }
                           });

                   });

            $("#Btn_uploadSucessImg").on('click',
                function () {

                    getSucessStoryImage();
                });

            //$("#").on('click', function () {
            //    $("#StoryvedioPage").css('display', 'block');            
            //    captureStoryVideo();
            //});

            $('input[type=radio][name=my-radio1_UploadStoryVedio]').change(function () {
                $("#StoryvedioPage").css('display', 'block');
                $("#vedioPage").css('display', 'block');
                $("#myProgress").css('display', 'block');
                if (this.value == "0") {
                    captureStoryVideo(0);
                } else {
                    captureStoryVideo(1);
                }

            });
        }
    }
}
function GoToNotificationPage(page) {
    $(window).scrollTop(0);
    loadSideMenuLinks();
    if (typeof page != 'undefined') {

        var notifications = [];
        var loading = false;
        var lastIndex = 8;
        var maxItems = notifications.length;
        var itemsPerLoad = 8;
        scrollLoadsBefore = false;

        var divnotificationsContainer = document.getElementById('DivNotification');
        var pagingobj = {
            "PageNumber": 1, "PageSize": 8
        };
        CallService("notification", "POST", "/api/NotificationSetting/GetUserNotifications", pagingobj, function (notifyList) {
            if (notifyList != null && notifyList.length > 0) {
                document.getElementById('DivNotification').innerHTML = "";
                $('#DivNotification').show();
                $('#divNoNotifications').hide();
                $('.loading img').css('display', '');

                notifications = notifyList;

                //  fristload = false;
                maxItems = notifyList[0].OverAllCount;
                //DrawanimalInHome(animals, 0, divAnimalContainer, itemsPerLoad, "AdoptPopupPage");

                drawNotifications(notifications, 0, itemsPerLoad, divnotificationsContainer);
                if (notifications.length < itemsPerLoad) {
                    $('.loading img').css('display', 'none');
                }
                lastIndex = $$('#DivNotification a.notify-block').length;
            } else {
                $('#DivNotification').hide();
                $('#divNoNotifications').show();
                $('.loading img').css('display', 'none');
            }
        });

        $('#DivNotification').hide();
        $('#divNoNotifications').show();
        $('.loading img').css('display', 'none');
        myApp.attachInfiniteScroll($$('#notification-infinite-scroll'));
        $$('#notification-infinite-scroll').on('infinite', function () {

            lastIndex = $$('#DivNotification div.anm-block').length;
            if (lastIndex >= maxItems || scrollLoadsBefore == true) {
                myApp.detachInfiniteScroll($$('#notification-infinite-scroll'));
                $$('.infinite-scroll-preloader').remove();
                return;
            }
            if (loading) return;

            loading = true;

            setTimeout(function () {


                var pagingobj = {
                    "PageNumber": parseInt(parseInt(lastIndex / 8) + 1), "PageSize": 8
                };

                CallService("notification", "POST", "/api/NotificationSetting/GetUserNotifications", pagingobj, function (notifyList) {
                    if (notifyList != null && notifyList.length > 0) {
                        lastIndex = $$('#DivNotification a.notify-block').length;
                        if (lastIndex >= maxItems || scrollLoadsBefore == true) {
                            myApp.detachInfiniteScroll($$('#notification-infinite-scroll'));
                            $$('.infinite-scroll-preloader').remove();
                            return;
                        }

                        loading = false;
                        $('#DivNotification').show();
                        $('#divNoNotifications').hide();
                        $('.loading img').css('display', '');
                        var notify = notifyList;
                        maxItems = notifyList[0].OverAllCount;
                        drawNotifications(notifyList, 0, itemsPerLoad, divnotificationsContainer);

                        if (notifyList.length < itemsPerLoad) {
                            $('.loading img').css('display', 'none');
                        }
                        lastIndex = $$('#DivNotification a.notify-block').length;

                    } else {
                        $('#DivNotification').hide();
                        $('#divNoNotifications').show();
                        $('.loading img').css('display', 'none');
                    }
                });
                $('.loading img').css('display', '');


            }, 1000);
        });

        if (initNotificationPage == true) {

        }
    }
}


function GoToMychatPage(page) {
    loadSideMenuLinks();
    if (typeof page != 'undefined') {
        var resMessage;
        if (typeof page.query.massage != 'undefined') {
            resMessage = page.query.massage
            chatId = resMessage.Id;
        }
        else {
            chatId = 0;
        }
        LoadMassage(animalMassageid, chatId);
        document.getElementById('MassagesDiv').innerHTML = '';
      

    }
}
function GoToMymessagesPage(page) {

    loadSideMenuLinks();
    if (typeof page != 'undefined') {

        LoadAllMessages(animalMassageid);
  
    }

}


function GoToadoptedRequesteDetails(page) {
  
    loadSideMenuLinks();
    if (typeof page != 'undefined') {

        debugger;
        var reqDetails = page.query.request;
                       
        document.getElementById('adoptedDetailsFullName').innerHTML     = reqDetails.UserName;
        document.getElementById('adoptedDetailsMobile').innerHTML       = reqDetails.UserPhoneNumber;
        document.getElementById('adoptedDetailsAddress').innerHTML      =reqDetails.Address;
        document.getElementById('adoptedDetailsAge').innerHTML          = reqDetails.Age;
        document.getElementById('adoptedDetailsEmail').innerHTML        = reqDetails.Email;
        document.getElementById('adoptedDetailsSocialStatus').innerHTML = reqDetails.SocialStatus;
        document.getElementById('adoptedDetailsQuestion1').innerHTML = reqDetails.Question1;
        document.getElementById('adoptedDetailsQuestion2').innerHTML = reqDetails.Question2;
        document.getElementById('adoptedDetailsQuestion3').innerHTML = reqDetails.Question3;
        document.getElementById('adoptedDetailsQuestion4').innerHTML = reqDetails.Question4;
        document.getElementById('adoptedDetailsQuestion5').innerHTML = reqDetails.Question5;
        document.getElementById('adoptedDetailsQuestion6').innerHTML = reqDetails.Question6;
        document.getElementById('adoptedDetailsQuestion7').innerHTML = reqDetails.Question7;
        document.getElementById('adoptedDetailsQuestion8').innerHTML = reqDetails.Question8;
        document.getElementById('adoptedDetailsQuestion9').innerHTML = reqDetails.Question9;
        document.getElementById('adoptedDetailsQuestion10').innerHTML = reqDetails.Question10;
        document.getElementById('adoptedDetailsQuestion11').innerHTML = reqDetails.Question11;
        document.getElementById('adoptedDetailsQuestion12').innerHTML = reqDetails.Question12;
        document.getElementById('adoptedDetailsQuestion13').innerHTML = reqDetails.Question13;
        document.getElementById('adoptedDetailsQuestion21').innerHTML = reqDetails.Question21;
        document.getElementById('adoptedDetailsQuestion22').innerHTML = reqDetails.Question22;
        document.getElementById('adoptedDetailsQuestion23').innerHTML = reqDetails.Question23;
        document.getElementById('adoptedDetailsQuestion24').innerHTML = reqDetails.Question24;
        document.getElementById('adoptedDetailsQuestion25').innerHTML = reqDetails.Question25;
        document.getElementById('adoptedDetailsQuestion26').innerHTML = reqDetails.Question26;
        document.getElementById('adoptedDetailsQuestion27').innerHTML = reqDetails.Question27;
        
    }
}

function AnimalEdit(id) {
    document.getElementById('AddAnimalPageTitle').innerHTML = EditAnimal;
    document.getElementById('BtnAddAnimalToAdaption').innerHTML = EditBtnTxt;
    CallService("AddAnimal", "GET", "/api/Animal/" + id, "", function (animal) {

        $("#txtAnimalId").val(animal.Id);
        $("#txtAnimalAge").val(animal.Age);
        $("#txtAnimalName").val(animal.Name);
        $("#txtAnimalVaccine").val(animal.Vaccinations);
        $("#divAnimalAddress").val(animal.Address);
        $("#ddlAnimalNotes").val(animal.Notes);
        $("#ddlAnimalCity option[value='" + animal.CityId + "']").attr('selected', 'selected');
        document.getElementById('selectedAnimalCity').innerHTML = animal.City;
        $("#ddlAnimalType option[value='" + animal.CategoryId + "']").attr('selected', 'selected');
        document.getElementById('selectedAnimalType').innerHTML = animal.Category;
        if (animal.Gendre == false) {
            document.getElementById('selectedAnimalGender').innerHTML = MaleGender;
            $("#ddlAnimalGender").val("0");
            $("#ddlAnimalGender option[value='0']").attr('selected', 'selected');
        } else {
            document.getElementById('selectedAnimalGender').innerHTML = FemaleGender;
            $("#ddlAnimalGender option[value='1']").attr('selected', 'selected')
        }

        if (animal.VedioUrl != null && animal.VedioUrl != "") {
            document.getElementById("videoPlayer").src = animal.VedioUrl;
            $("#vedioPage").css('display', 'block');
        } else {
            $("#vedioPage").css('display', 'none');
        }
        animalType = animal.TypeId;

        CallService("", "GET", "/api/Animal/GetAnimalImages?id=" + id, "", function (animalImg) {
            for (var i = 0; i < animalImg.length; i++) {
                var obj = animalImg[i];
                DrawAnimalImg(obj.ImageUrl);
            }

        });
    });


    PageNameAddanmial = "animalDetails";
}
function DrawAnimalImg(totalPhotoUrl) {
    var imgContainer = document.getElementById('imgContainerDiv');
    animalAdopImages.push(totalPhotoUrl);
    var ImgUp = document.createElement("img");
    ImgUp.className = 'uploadImg';
    ImgUp.src = totalPhotoUrl;
    var imgdiv = document.createElement("div");
    imgdiv.className = 'imgContent';
    var imgdivoverlay = document.createElement("div");
    imgdivoverlay.className = 'delOverlay';
    var imgIcon = document.createElement("i");
    imgIcon.className = 'ionicons ion-close-round';
    imgdiv.setAttribute('onclick', 'RemoveImage(this);');
    imgdivoverlay.appendChild(imgIcon);
    imgdiv.appendChild(imgdivoverlay);
    imgdiv.appendChild(ImgUp);
    imgContainer.appendChild(imgdiv);

}
function ShowLoader(pageName) {
    var divPage = document.getElementById(pageName + 'Page');
    var divLoader = document.createElement('div');
    var divCube = document.createElement('div');
    var divCubeP1 = document.createElement('div');
    var divCubeP2 = document.createElement('div');
    var divCubeP3 = document.createElement('div');
    var divCubeP4 = document.createElement('div');
    var hdrWait = document.createElement('h3');

    divLoader.className += 'loader divLoader';
    divCube.className += 'cssload-thecube';
    divCubeP1.className += 'cssload-cube cssload-c1';
    divCubeP2.className += 'cssload-cube cssload-c2';
    divCubeP3.className += 'cssload-cube cssload-c3';
    divCubeP4.className += 'cssload-cube cssload-c4';
    hdrWait.innerHTML = loaderMSg;

    divCube.appendChild(divCubeP1);
    divCube.appendChild(divCubeP2);
    divCube.appendChild(divCubeP3);
    divCube.appendChild(divCubeP4);
    divLoader.appendChild(divCube);
    divLoader.appendChild(hdrWait);
    divPage.appendChild(divLoader);
    $(".loader").fadeIn("slow");
}
function HideLoader() {
    $(".loader").fadeOut("slow");
    $('div').each(function () {
        var div = $(this);
        if ($(this).hasClass('loader divLoader')) {
            $(this).remove();
        }
    });
}
function LoadComments(animalId) {
    document.getElementById('CommentsStoryDiv').innerHTML = "";
    var comments = [];
    var loading = false;
    var lastIndex = 8;
    var maxItems = comments.length;
    var itemsPerLoad = 8;
    scrollLoadsBefore = false;
    document.getElementById('CommentsDiv').innerHTML = "";
    var param = {
        'AnimalId': animalId, 'PageNumber': 1, 'PageSize': 8
    }
    CallService("animalDetails", "POST", "/api/Comment/GetAnimalComment", param, function (commentList) {
        if (commentList != null && commentList.length > 0) {
            document.getElementById('CommentsDiv').innerHTML = "";
            $('#CommentsDiv').show();
            $('#divNocomments').hide();
            $('.loading img').css('display', '');
            comments = commentList;
            maxItems = commentList[0].OverAllCount;
            var commentsDiv = document.getElementById('CommentsDiv');
            drawComments(comments, 0, itemsPerLoad, commentsDiv);
            if (comments.length < itemsPerLoad) {
                $('.loading img').css('display', 'none');
                myApp.detachInfiniteScroll($$('#animal-details-infinite-scroll'));
                $$('.infinite-scroll-preloader').remove();
            }
            lastIndex = $$('#CommentsDiv li.Commt-bock').length;
        } else {
            $('#CommentsDiv').hide();
            $('#divNocomments').show();
            $('.loading img').css('display', 'none');
            myApp.detachInfiniteScroll($$('#animal-details-infinite-scroll'));
            $$('.infinite-scroll-preloader').remove();
            return;
        }
    });
    $('#CommentsDiv').hide();
    $('#divNocomments').show();
    $('.loading img').css('display', 'none');
    myApp.attachInfiniteScroll($$('#animal-details-infinite-scroll'));
    $$('#animal-details-infinite-scroll').on('infinite', function () {
        lastIndex = $$('#CommentsDiv li.Commt-bock').length;
        if (lastIndex >= maxItems || scrollLoadsBefore == true) {
            myApp.detachInfiniteScroll($$('#animal-details-infinite-scroll'));
            $$('.infinite-scroll-preloader').remove();
            return;
        }
        if (loading) return;
        loading = true;
        setTimeout(function () {
            var objparam = {
                'AnimalId': animalId, 'PageNumber': parseInt(parseInt(lastIndex / 8) + 1), 'PageSize': 8
            }
            CallService("animalDetails", "POST", "/api/Comment/GetAnimalComment", objparam, function (commentsList) {
                if (commentsList != null && commentsList.length > 0) {
                    lastIndex = $$('#CommentsDiv  li.Commt-bock').length;
                    if (lastIndex >= maxItems || scrollLoadsBefore == true) {
                        myApp.detachInfiniteScroll($$('#animal-details-infinite-scroll'));
                        $$('.infinite-scroll-preloader').remove();
                        return;
                    }
                    loading = false;
                    $('#CommentsDiv').show();
                    $('#divNocomments').hide();
                    $('.loading img').css('display', '');
                    var comment = commentsList;
                    maxItems = commentsList[0].OverAllCount;
                    var commentsDiv = document.getElementById('CommentsDiv');
                    drawComments(comment, 0, itemsPerLoad, commentsDiv);
                    if (comment.length < itemsPerLoad) {
                        $('.loading img').css('display', 'none');
                        myApp.detachInfiniteScroll($$('#animal-details-infinite-scroll'));
                        $$('.infinite-scroll-preloader').remove();
                    }
                    lastIndex = $$('#CommentsDiv li.Commt-bock').length;
                } else {
                    $('#CommentsDiv').hide();
                    $('#divNocomments').show();
                    $('.loading img').css('display', 'none');
                    myApp.detachInfiniteScroll($$('#animal-details-infinite-scroll'));
                    $$('.infinite-scroll-preloader').remove();
                }
            });
            $('.loading img').css('display', '');
        }, 1000);
    });
}
function LoadCommentsStory(storyId) {
    document.getElementById('CommentsDiv').innerHTML = "";
    var comments = [];
    var loading = false;
    var lastIndex = 8;
    var maxItems = comments.length;
    var itemsPerLoad = 8;
    scrollLoadsBefore = false;
    document.getElementById('CommentsStoryDiv').innerHTML = "";

    var param = {
        'StoryId': storyId, 'PageNumber': 1, 'PageSize': 8
    }
    CallService("", "POST", "/api/Comment/GetStoryComment", param, function (commentList) {
        if (commentList != null && commentList.length > 0) {
            document.getElementById('CommentsStoryDiv').innerHTML = "";
            $('#CommentsStoryDiv').show();
            $('#divNoStorycomments').hide();
            $('.loading img').css('display', '');
            comments = commentList;
            maxItems = commentList[0].OverAllCount;
            var commentsDiv = document.getElementById('CommentsStoryDiv');
            drawComments(comments, 0, itemsPerLoad, commentsDiv);
            if (comments.length < itemsPerLoad) {
                $('.loading img').css('display', 'none');
            }
            lastIndex = $$('#CommentsStoryDiv li.Commt-bock').length;
        } else {
            $('#CommentsStoryDiv').hide();
            $('#divNoStorycomments').show();
            $('.loading img').css('display', 'none');
        }
    });

    $('#CommentsStoryDiv').hide();
    $('#divNoStorycomments').show();
    $('.loading img').css('display', 'none');

    myApp.attachInfiniteScroll($$('#story-details-infinite-scroll'));

    $$('#story-details-infinite-scroll').on('infinite', function () {

        lastIndex = $$('#CommentsStoryDiv li.Commt-bock').length;
        if (lastIndex >= maxItems || scrollLoadsBefore == true) {
            myApp.detachInfiniteScroll($$('#story-details-infinite-scroll'));
            $$('.infinite-scroll-preloader').remove();
            return;
        }
        if (loading) return;

        loading = true;

        setTimeout(function () {



            var objparam = {
                'StoryId': storyId, 'PageNumber': parseInt(parseInt(lastIndex / 8) + 1), 'PageSize': 8
            }
            CallService("", "POST", "/api/Comment/GetStoryComment", objparam, function (commentsList) {
                if (commentsList != null && commentsList.length > 0) {
                    lastIndex = $$('#CommentsStoryDiv  li.Commt-bock').length;
                    if (lastIndex >= maxItems || scrollLoadsBefore == true) {
                        myApp.detachInfiniteScroll($$('#story-details-infinite-scroll'));
                        $$('.infinite-scroll-preloader').remove();
                        return;
                    }

                    loading = false;
                    $('#CommentsStoryDiv').show();
                    $('#divNoStorycomments').hide();
                    $('.loading img').css('display', '');
                    var comment = commentsList;
                    maxItems = commentsList[0].OverAllCount;
                    var commentsDiv = document.getElementById('CommentsStoryDiv');
                    drawComments(comment, 0, itemsPerLoad, commentsDiv);
                    if (comment.length < itemsPerLoad) {
                        $('.loading img').css('display', 'none');
                    }
                    lastIndex = $$('#CommentsStoryDiv li.Commt-bock').length;

                } else {
                    $('#CommentsStoryDiv').hide();
                    $('#divNoStorycomments').show();
                    $('.loading img').css('display', 'none');
                    myApp.detachInfiniteScroll($$('#story-details-infinite-scroll'));
                    $$('.infinite-scroll-preloader').remove();
                }
            });
            $('.loading img').css('display', '');


        }, 1000);
    });

}
function LoadMassage(animalId, chatId) {
    var massages = [];
    var loading = false;
    var lastIndex = 8;
    var maxItems = massages.length;
    var itemsPerLoad = 8;
    scrollLoadsBefore = false;
    document.getElementById('MassagesDiv').innerHTML = "";

    var param = {
        'PageNumber': 1, 'PageSize': 8, 'AnimalId': animalId, 'ChatId': chatId
    }
    CallService("chat", "POST", "/api/Message/GetMessageDetails", param, function (massagesList) {
        if (massagesList != null && massagesList.length > 0) {
            document.getElementById('MassagesDiv').innerHTML = "";
            $('#MassagesDiv').show();
            $('#divNoMassages').hide();
            $('.loading img').css('display', '');
         
            massages = massagesList;
            maxItems = massagesList[0].OverAllCount;
            drawMassages(massages, 0, itemsPerLoad);
            if (massages.length < itemsPerLoad) {
                $('.loading img').css('display', 'none');
            }
            lastIndex = $$('#MassagesDiv li.Msg-bock').length;
        } else {
            $('#MassagesDiv').hide();
            $('#divNoMassages').show();
            $('.loading img').css('display', 'none');
        }
    });

    $('#MassagesDiv').hide();
    $('#divNoMassages').show();
    $('.loading img').css('display', 'none');

    myApp.attachInfiniteScroll($$('#chat-infinite-scroll'));

    $$('#chat-infinite-scroll').on('infinite', function () {

        lastIndex = $$('#MassagesDiv li.Msg-bock').length;
        if (lastIndex >= maxItems || scrollLoadsBefore == true) {
            myApp.detachInfiniteScroll($$('#chat-infinite-scroll'));
            $$('.infinite-scroll-preloader').remove();
            return;
        }
        if (loading) return;

        loading = true;

        setTimeout(function () {

            var objparam = {
                'PageNumber': parseInt(parseInt(lastIndex / 8) + 1), 'PageSize': 8, 'AnimalId': animalId, 'ChatId': chatId
            }
            CallService("chat", "POST", "/api/Message/GetMessageDetails", objparam, function (massagesList) {
                if (massagesList != null && massagesList.length > 0) {
                    lastIndex = $$('#MassagesDiv li.Msg-bock').length;
                    if (lastIndex >= maxItems || scrollLoadsBefore == true) {
                        myApp.detachInfiniteScroll($$('#chat-infinite-scroll'));
                        $$('.infinite-scroll-preloader').remove();
                        return;
                    }

                    loading = false;
                    $('#MassagesDiv').show();
                    $('#divNoMassages').hide();
                    $('.loading img').css('display', '');
                    var massage = massagesList;
                    maxItems = massagesList[0].OverAllCount;
                    drawMassages(massage, 0, itemsPerLoad);
                    if (massage.length < itemsPerLoad) {
                        $('.loading img').css('display', 'none');
                    }
                    lastIndex = $$('#MassagesDiv li.Msg-bock').length;

                } else {
                    $('#MassagesDiv').hide();
                    $('#divNoMassages').show();
                    $('.loading img').css('display', 'none');
                }
            });
            $('.loading img').css('display', '');


        }, 1000);
    });

}
function LoadAllMessages(animalId,chatId) {
    var massages = [];
    var loading = false;
    var lastIndex = 8;
    var maxItems = massages.length;
    var itemsPerLoad = 8;
    scrollLoadsBefore = false;
    document.getElementById('divAllMessages').innerHTML = "";
    var param = {};
    var methodname='';
    if (animalId == null) {
         param = {
            'PageNumber': 1,
            'PageSize': 8
         }

        methodname = "/api/Message/GetAllUserMessages";
    } else {
         param = {
            'PageNumber': 1, 'PageSize': 8, 'AnimalId': animalId
         }
        methodname = "/api/Message/GetUserMessages";
    }
   
    CallService("messages", "POST", methodname, param, function (massagesList) {
        if (massagesList != null && massagesList.length > 0) {
            document.getElementById('divAllMessages').innerHTML = "";
            $('#divAllMessages').show();
            $('#divNoAllMassages').hide();
            $('.loading img').css('display', '');
          
            massages = massagesList;
            maxItems = massagesList[0].OverAllCount;
            drawAllMessages(massages, 0, itemsPerLoad,chatId);
            if (massages.length < itemsPerLoad) {
                $('.loading img').css('display', 'none');
            }
            lastIndex = $$('#divAllMessages li.MsgRoom-bock').length;
        } else {
            $('#divAllMessages').hide();
            $('#divNoAllMassages').show();
            $('.loading img').css('display', 'none');

        }
    });

    $('#divAllMessages').hide();
    $('#divNoAllMassages').show();
    $('.loading img').css('display', 'none');

    myApp.attachInfiniteScroll($$('#messages-infinite-scroll'));

    $$('#messages-infinite-scroll').on('infinite', function () {

        lastIndex = $$('#divAllMessages li.MsgRoom-bock').length;
        if (lastIndex >= maxItems || scrollLoadsBefore == true) {
            myApp.detachInfiniteScroll($$('#messages-infinite-scroll'));
            $$('.infinite-scroll-preloader').remove();
            return;
        }
        if (loading) return;

        loading = true;

        setTimeout(function () {


            var objparam = {};
            var namemethod = '';
            if (animalId == null) {
                objparam = {
                    'PageNumber': parseInt(parseInt(lastIndex / 8) + 1), 'PageSize': 8
                }
                namemethod = "/api/Message/GetAllUserMessages"
            } else {
                 objparam = {
                    'PageNumber': parseInt(parseInt(lastIndex / 8) + 1), 'PageSize': 8, 'AnimalId': animalId
                }
                 namemethod = "/api/Message/GetUserMessages"
            }
           
            CallService("messages", "POST", namemethod, objparam, function (massagesList) {
                if (massagesList != null && massagesList.length > 0) {
                    lastIndex = $$('#divAllMessages li.MsgRoom-bock').length;
                    if (lastIndex >= maxItems || scrollLoadsBefore == true) {
                        myApp.detachInfiniteScroll($$('#messages-infinite-scroll'));
                        $$('.infinite-scroll-preloader').remove();
                        return;
                    }

                    loading = false;
                    $('#divAllMessages').show();
                    $('#divNoAllMassages').hide();
                    $('.loading img').css('display', '');
                    var massage = massagesList;
                    maxItems = massagesList[0].OverAllCount;
                    drawAllMessages(massage, 0, itemsPerLoad);
                    if (massage.length < itemsPerLoad) {
                        $('.loading img').css('display', 'none');
                    }
                    lastIndex = $$('#divAllMessages li.MsgRoom-bock').length;

                } else {
                    $('#divAllMessages').hide();
                    $('#divNoAllMassages').show();
                    $('.loading img').css('display', 'none');
                }
            });
            $('.loading img').css('display', '');


        }, 1000);
    });

}
function sginOut() {
    window.plugins.OneSignal
    .startInit(pushAppId)
    .handleNotificationOpened(notificationOpenedCallback)
    .endInit();
    window.plugins.OneSignal.getIds(function (ids) {
        console.log('getIds: ' + JSON.stringify(ids));

        var obj = {
            "DeviceId": ids.userId, "IsOnline": false
        };
        CallService('', "POST", "/api/NotificationSetting/SaveDeviceSetting/", obj, function (res) {
            if (res != null) {

                localStorage.removeItem('appToken');
                localStorage.removeItem('refreshToken');
                localStorage.removeItem('userLoggedIn');
                localStorage.removeItem('Visitor');
                localStorage.removeItem('UserId')
                localStorage.removeItem('UserEntersCode')
                mainView.router.loadPage({ pageName: 'login' });
                $("#imgUserEdit").attr('src', 'img\noimg.jpg');
                $("#txtEditUserName").val("");
                $("#txtEditUserEmail").val("");
                $("#TxtEditUserMobile").val("");


                //initloginPage = true;
                //initsignupPage = true;
                //initactivation = true;
                //initIndexPage = true;
                //initProfilePage = true;
                //initAnimalDetailsPage = true
                //initrefugePage = true;
                //initrefugeDetails = true;
                //initvolunteer = true;
                //initusers = true;
                //inituserDetails = true;
                //initaddAnimal = true;
                //initlostAnimals = true;
                //initneedCareAnimals = true;
                //initsuccessStory = true;
                //initStoryDetailsPage = true;
                //initmapPage = true;
                //initChat = true;
                //initmessages = true;
                //initforgetPass = true;
                //initchangePassword = true;
                //initresetPassword = true;
                //initAdoptPopupPage = true;
                //initEditProfilePopup = true;
                //initreportPopupPage = true;
                //initLeaveReques = true;
                //initadoptedRequestPopup = true;
                //initcommentPopup = true;
                //initSearch = true;
                //initaddSuccessStory = true;
                //initNotificationPage = true;
            }
        });
    });
    window.plugins.googleplus.disconnect(
    function (msg) {
        // alert(msg); // do something useful instead of alerting
    }
);
}
function onConfirmAr(buttonIndex) {
    if (buttonIndex == 1) {
        if (device.platform.indexOf('dr') > 0)
            navigator.app.exitApp();
    }
}
function checkConnection() {
    var networkState = navigator.connection.type;
    return networkState;
}
function onOffline() {
    if (CurrentLanguage == "en")
        navigator.notification.alert('You are not connected to the Internet, please reopen Application after confirming the Internet.', function () {
            navigator.app.exitApp();
        }, 'Warning', 'ok');

    else
        navigator.notification.alert('انت غير متصل بالانترنت , من فضلك أعد تشغيل البرنامج بعد التأكد من الإنترنت .', function () {
            navigator.app.exitApp();
        }, 'خطأ', 'موافق');

    // Handle the offline event
}
function onImgError(e) {
    e.src = "img/NoImg.jpg";
}
function RemoveAnimalVedio(type) {
    navigator.notification.confirm(
    DeleteVedio,
     function (buttonIndex) {
         if (buttonIndex == 1) {
             if (type == 1) {
                 $("#vedioPage").css('display', 'none');
                 document.getElementById("videoPlayer").src = "";
                 $('input[type=radio][name=my-radio1_UploadAnimalVedio]').each(function () {
                     $(this).prop('checked', false);
                     //this.checked = false;
                 });
             } else {
                 $("#vedioGiveUpPage").css('display', 'none');
                 document.getElementById("videoGiveUpPlayer").src = "";
                 $('input[type=radio][name=LeaveRequest_UploadAnimalVedio]').each(function () {
                     $(this).prop('checked', false);
                     //this.checked = false;
                 });
             }

         }
     },
   warning,
    [Confirm, Cancel]
);

}
function RemoveImage(e) {
    navigator.notification.confirm(
       DeleteImg,
        function (buttonIndex) {
            if (buttonIndex == 1) {
                var index = animalAdopImages.indexOf(e.getElementsByTagName('img')[0].src);
                if (index > -1) {
                    animalAdopImages.splice(index, 1);
                }
                e.remove();
            }

        },
      warning,
       [Confirm, Cancel]
   );
}
function RemoveStoryVedio() {
    navigator.notification.confirm(
     DeleteVedio,
      function (buttonIndex) {
          if (buttonIndex == 1) {
              $("#StoryvedioPage").css('display', 'none');
              document.getElementById("videoStoryPlayer").src = "";
              $('input[type=radio][name=my-radio1_UploadStoryVedio]').each(function () {
                  $(this).prop('checked', false);
                  //this.checked = false;
              });
          }
      },
    warning,
     [Confirm, Cancel]
 );

}
function loadSideMenuLinks() {
    //$("a.item-link smart-select").attr("data-picker-close-text", closeTxt);

    $("a.item-link").each(function () {
        $(this).attr("data-picker-close-text", closeTxt);
    });
    Translation();


    CallService('', "GET", "/api/NotificationSetting/NotificationCount", "", function (res) {
        
        if (res != null) {
           
            if (res == 0) {
                $('#notificationCount').css('display', 'none');
                $("#notificationCount").val(res);
            } else {
                $('#notificationCount').css('display', 'block');
                $("#notificationCount").val(res);
                document.getElementById("notificationCount").innerHTML = res;
            }
           

        }
    });
  
    

    localStorage.setItem("IndexPageNumber", 1);
    fristIndex = true;
    if (localStorage.getItem('userLoggedIn')) {
        var user = JSON.parse(localStorage.getItem('userLoggedIn'));
        var FullName = user.Name;
        ShowInformation = user.ShowInfo;
        currentUserId = user.Id;
        $('#hdrSideMenuName').html(FullName);
        $('#linkMenuToSignout').css('display', '');
        $('#linkMenuChangePassword').css('display', '');
        $('#linkMenuToSignIn').css('display', 'none');
        $('#linkMenuToSignUp').css('display', 'none');
        $('#linkMenuProfile').css('display', '');
        $('#linkMenunotification').css('display', '');


        if (localStorage.getItem("isSocial") == 'true')
            $('#linkMenuChangePassword').css('display', 'none');
        else
            $('#linkMenuChangePassword').css('display', '');
        $('#linkMenuSetting').css('display', '');


    }
    else {
        $('#hdrSideMenuName').html(Visitor);
        $('#linkMenuToSignout').css('display', 'none');
        $('#linkMenuChangePassword').css('display', 'none');
        $('#linkMenuToSignIn').css('display', '');
        $('#linkMenuToSignUp').css('display', '');
        $('#linkMenuProfile').css('display', 'none');
        $('#linkMenunotification').css('display', 'none');
        $('#linkMenuSetting').css('display', '');

    }
    if (initSideMenu == true) {
        initSideMenu = false;
        $('#linkMenuIndex').on("click", function () {
            //  mainView.router.reloadPage({ pageName: 'index' , ignoreCache: true});
            //  mainView.loadPage({url:"index",reload:true, ignoreCache: true});
            // mainView.router.reloadPage({ pageName: 'index' });
            //mainView.router.refreshPage();
            fromLogin = true;
            GoToindexPage("page");
        });
        $('#linkMenuProfile').on("click", function () {
            mainView.router.loadPage({ pageName: 'profile' });
        });
        $('#linkMenuRefuge').on("click", function () {
            mainView.router.loadPage({ pageName: 'refuge' });
        });
        $('#linkMenuUsers').on("click", function () {
            mainView.router.loadPage({ pageName: 'users' });
        });
        $('#linkMenuAddAnimal').on("click", function () {
            mainView.router.loadPage({ pageName: 'addAnimal' });
        });
        $('#linkMenuIostAnimals').on("click", function () {
            mainView.router.loadPage({ pageName: 'lostAnimals' });
        });
        $('#linkMenuNeedCareAnimals').on("click", function () {
            mainView.router.loadPage({ pageName: 'needCareAnimals' });
        });
        $('#linkMenuVolunteer').on("click", function () {
            mainView.router.loadPage({ pageName: 'volunteer' });
        });
        $('#linkMenuSuccessStory').on("click", function () {
            mainView.router.loadPage({ pageName: 'successStory' });
        });
        $('#linkMenuToSignout').on("click", function () {
            sginOut();
        });
        $('#linkMenuChangePassword').on("click", function () {
            mainView.router.loadPage({
                pageName: 'changePassword'
            });
        });
        $('#linkMenuToSignIn').on("click", function () {
            mainView.router.loadPage({
                pageName: 'login'
            });
        });
        $('#linkMenuToSignUp').on("click", function () {
            mainView.router.loadPage({
                pageName: 'signup'
            });
        });
        $('#linkMenuLanguage').on("click", function () {
            var lang = localStorage.getItem('Language');
            ChangeAppLanguage(lang);
        });
        $('#linkMenunotification').on("click", function () {
            mainView.router.loadPage({
                pageName: 'notification'
            });
        });
        $('#linkMenuChat').on("click", function () {
            mainView.router.loadPage({ pageName: 'messages', query: { UserChat: 1 } });
        });
        
    }
}
function setAppLanguage(lang) {
    if (lang == null) {
        localStorage.setItem('Language', 'ar');
        //$('link[title="ArabicCss"]').remove();
        //$('link[title="enmaterialCss"]').remove();
        //$('link[title="armaterialCss"]').remove();


        $('link[rel=stylesheet][href~="css/framework7.material.rtl.min.css"]').remove();
        $('link[rel=stylesheet][href~="css/my-app-material-en.css"]').remove();
        $('link[rel=stylesheet][href~="css/my-app-material-ar.css"]').remove();

        //$("head link[rel='stylesheet']").last().after('<link rel="stylesheet"  href="css/framework7.material.rtl.min.css">');
        //$("head link[rel='stylesheet']").last().after('<link rel="stylesheet"  href="css/my-app-material-ar.css">');

        //$('head').append('<link rel="stylesheet" title="ArabicCss" href="css/framework7.material.rtl.min.css">');
        //$('head').append('<link rel="stylesheet" title="armaterialCss" href="css/my-app-material-ar.css">');

        $('head').append('<link rel="stylesheet"  href="css/framework7.material.rtl.min.css">');
        $('head').append('<link rel="stylesheet"  href="css/my-app-material-ar.css">');

        $("#MainPanal").removeClass("panel-right")
        $("#MainPanal").addClass("panel-right");
        $("#MainPanal").removeClass("panel-left");
    }
    else if (lang == 'en') {
        localStorage.setItem('Language', 'en');
        //$('link[title="ArabicCss"]').remove();
        //$('link[title="armaterialCss"]').remove();
        //$('link[title="enmaterialCss"]').remove();

        $('link[rel=stylesheet][href~="css/framework7.material.rtl.min.css"]').remove();
        $('link[rel=stylesheet][href~="css/my-app-material-ar.css"]').remove();
        $('link[rel=stylesheet][href~="css/my-app-material-en.css"]').remove();


        //$("head link[rel='stylesheet']").last().after('<link rel="stylesheet"  href="css/my-app-material-en.css">');
        //$('head').append('<link rel="stylesheet" title="enmaterialCss" href="css/my-app-material-en.css">');
        $('head').append('<link rel="stylesheet"  href="css/my-app-material-en.css">');

        $("#MainPanal").removeClass("panel-left")
        $("#MainPanal").addClass("panel-left");
        $("#MainPanal").removeClass("panel-right");

    }
    else if (lang == 'ar') {
        localStorage.setItem('Language', 'ar');
        //$('link[title="ArabicCss"]').remove();
        //$('link[title="enmaterialCss"]').remove();
        //$('link[title="armaterialCss"]').remove();


        $('link[rel=stylesheet][href~="css/framework7.material.rtl.min.css"]').remove();
        $('link[rel=stylesheet][href~="css/my-app-material-ar.css"]').remove();
        $('link[rel=stylesheet][href~="css/my-app-material-en.css"]').remove();

        $("head link[rel='stylesheet']").last().after('<link rel="stylesheet"  href="css/framework7.material.rtl.min.css">');
        $("head link[rel='stylesheet']").last().after('<link rel="stylesheet"  href="css/my-app-material-ar.css">');
        //$('head').append('<link rel="stylesheet" title="ArabicCss" href="css/framework7.material.rtl.min.css">');
        //$('head').append('<link rel="stylesheet" title="armaterialCss" href="css/my-app-material-ar.css">');
        $("#MainPanal").removeClass("panel-right")
        $("#MainPanal").addClass("panel-right");
        $("#MainPanal").removeClass("panel-left");
    }
    CurrentLanguage = localStorage.getItem('Language');
    Translation()
    HideLoader();
}
function ChangeAppLanguage(lang) {
    if (lang == null) {
        localStorage.setItem('Language', 'ar');


        $('link[rel=stylesheet][href~="css/framework7.material.rtl.min.css"]').remove();
        $('link[rel=stylesheet][href~="css/my-app-material-en.css"]').remove();
        $('link[rel=stylesheet][href~="css/my-app-material-ar.css"]').remove();
        $('head').append('<link rel="stylesheet"  href="css/framework7.material.rtl.min.css">');
        $('head').append('<link rel="stylesheet"  href="css/my-app-material-ar.css">');


        $("#MainPanal").removeClass("panel-right")
        $("#MainPanal").addClass("panel-right");
        $("#MainPanal").removeClass("panel-left");

    }
    else if (lang == 'ar') {
        localStorage.setItem('Language', 'en');
        $('link[rel=stylesheet][href~="css/framework7.material.rtl.min.css"]').remove();
        $('link[rel=stylesheet][href~="css/my-app-material-ar.css"]').remove();
        $('link[rel=stylesheet][href~="css/my-app-material-en.css"]').remove();
        $('head').append('<link rel="stylesheet"  href="css/my-app-material-en.css">');

        $("#MainPanal").removeClass("panel-left")
        $("#MainPanal").addClass("panel-left");
        $("#MainPanal").removeClass("panel-right");

    }
    else if (lang == 'en') {
        localStorage.setItem('Language', 'ar');
        $('link[rel=stylesheet][href~="css/framework7.material.rtl.min.css"]').remove();
        $('link[rel=stylesheet][href~="css/my-app-material-ar.css"]').remove();
        $('link[rel=stylesheet][href~="css/my-app-material-en.css"]').remove();
        $("head link[rel='stylesheet']").last().after('<link rel="stylesheet"  href="css/framework7.material.rtl.min.css">');
        $("head link[rel='stylesheet']").last().after('<link rel="stylesheet"  href="css/my-app-material-ar.css">');
        $("#MainPanal").removeClass("panel-right")
        $("#MainPanal").addClass("panel-right");
        $("#MainPanal").removeClass("panel-left");
    }
    CurrentLanguage = localStorage.getItem('Language');

    // Translation()
    ShowLoader("app");
    location.reload(false);
    if (localStorage.getItem('UserId') != null) {

        mainView.router.loadPage({ pageName: 'index' });
    }
    else {
        mainView.router.loadPage({ pageName: 'login' });
    }



}
function Translation() {
    if (CurrentLanguage == 'ar' || CurrentLanguage == null) {

        MenuLogo.src = "img/smallLogo.png";
        landingPageIcon.src = "img/logo.png";
        loginLogo.src = "img/logo.png";
        signUpLogo.src = "img/logo.png";
        activationLogo.src = "img/logo.png";
        ForgetLogo.src = "img/logo.png";
        resetLogo.src = "img/logo.png";
        showInfoDivMsg = "البيانات الشخصية تظهر لكل المستخدمين";
        HideInfoDivMsg = "البيانات الشخصية تظهر لك فقط";
        showMsg = 'اظهر بياناتى';
        hideMsg = 'اخفاء بياناتى';
        //general
        DeleteImg = "هل تريد حذف الصورة؟";
        DeleteVedio = "هل تريد حذف الفيديو؟";
        Confirm = "تم";
        Cancel = "الغاء";
        VedioDidntUpload = "لم يتم تحميل الفيديو";
        AnimalCategory = "تصنيف الحيوان";
        reportValueRequired = "من فضلك ادخل سبب البلاغ";
        reportAddedSucessFully = "تم اضاغة البلاغ بنجاح";
        Visitor = "زائر";
        MaleGender = "ذكر";
        FemaleGender = "انثى";
        DefultGender = "الجنس";
        City = "المدينة";
        ShelterTxt = "الملاجئ";
        loaderMSg = "يرجى الانتظار";
        VolunterAddedPefor = "لقد قمت بالتطوع لهذا الملجا من قبل";
        subscribeforShelter = 'تفعيل الاشعارات';
        UnsubscribeforShelter = 'ايقاف الاشعارات';
        Wrong = 'خطأ';
        cantFindData = 'لا يمكن التحقق من البيانات ';
        NeedConfirm = 'هذا الحساب غير مفعل';
        warning = 'تنبيه';
        ErrorOccured = 'حدث خطأ'
        Sucess = 'نجاح'
        SucessAnimalDelete = "تم حذف الحيوان بنجاح";
        DeleteStoryConfirmMsg = "هل تريد حذف القصة؟";
        SucessStoryDelete = "تم حذف القصة بنجاح";
        LoginError = 'من فضلك أدخل إسم الدخول وكلمة المرور'
        LoginDataError = 'خطأ في اسم المستخدم أو كلمة المرور .';
        WrongActivationNo = 'من فضلك ادخل كود صحيح';
        CodeSendSucess = 'تم ارسال الكود لجوالك بنجاح';
        CodeSend = 'تم ارسال كلمة السر لبريدك الالكترونى';
        WrongEmail = 'من فضلك ادخل بريد الكترونى صحيح';
        PasswordChanged = 'تم تعديل البيانات بنجاح .';
        NoAge = 'لا يوجد عمر للحيوان';
        NoNotAvilable = 'رقم التليفون غير متوفر';
        notFound = 'لا يوجد';
        NotAuthenticated = 'يجب ان تقوم بتسجيل الدخول اولا';
        RequestAdded = 'تم اضافة الطلب  بنجاح';
        storyAdded = 'تم اضافة القصة  بنجاح'
        FiledsEmpty = 'يجب ان تملاء الحقول اولا ';
        OneImageAtLeast = 'يجب اضافة صوره واحده على الاقل';
        AnimalAdded = 'تم اضافة الحيوان بنجاح';
        AnimalEditSucss = 'تم تعديل الحيوان بنجاح';
        CommentAdded = 'تم اضافة التعليق بنجاح';
        WrongOldPassWord = 'كلمة السر القديمة خطأ.';
        closeTxt = 'اغلاق';
        ChoseImage = 'لم يتم اختيار الصورة';
        LargNoOfImage = 'لا يمكنك اضافة اكثر من ثلاث صور';
        NotAuthenticatedreport = 'لا يمكنك الابلاغ عن نفسك';
        noAnimalName = 'لا يوجد اسم للحيوان';
        PasswordError = "كلمة السر لا يمكن ان تقل عن 6 حروف ";
        EmailExistError = "البريد الاكترونى موجود من قبل . ";
        PhoneExistError = "رقم الجوال موجود من قبل .";
        UserNameExistError = "اسم المستخدم موجود من قبل .";
        ArabicUserNameError = 'اسم المستخدم لا يمكن ان يكون بحروف عربية';
        PasswordEmptyError = 'كلمة السر لايمكن ان تكون مسافات فقط';
        YouOWnAnimal = 'انت صاحب هذا الحيوان';
        MonthesLbl = 'شهر';
        AgeError = 'ادخل العمر بطريقة صحيحة';
        YES = 'نعم';
        nO = 'لا';
        //menu
        document.getElementById('linkMenuIndex').innerHTML = '<i class="ionicons ion-ios-home"></i>الصفحة الرئيسية';
        document.getElementById('linkMenuLanguage').innerHTML = '<i class="ionicons ion-earth"></i>الانجليزية';
        document.getElementById('linkMenuProfile').innerHTML = '<i class="ionicons ion-person"></i>الصفحة الشخصية';
        document.getElementById('linkMenunotification').innerHTML = '<i class="ionicons ion-android-notifications"></i>الاشعارات <span class="notificationNumber" id="notificationCount"></span>';
        
        document.getElementById('linkMenuChat').innerHTML = '<i class="ionicons ion-chatboxes"></i>الرسائل';

        document.getElementById('linkMenuRefuge').innerHTML = '<i class="ionicons ion-android-apps"></i>الملاجئ والجمعيات التطوعية';
        document.getElementById('linkMenuUsers').innerHTML = '<i class="ionicons ion-person-stalker"></i>المستخدمين';
        document.getElementById('linkMenuAddAnimal').innerHTML = '<i class="ionicons ion-android-add-circle"></i>إضافة حيوان للتبنى';
        document.getElementById('linkMenuIostAnimals').innerHTML = '<i class="ionicons ion-close-circled"></i>حيوانات مفقودة';
        document.getElementById('linkMenuNeedCareAnimals').innerHTML = '<i class="ionicons ion-medkit"></i>حالات طارئة';
        document.getElementById('linkMenuVolunteer').innerHTML = '<i class="ionicons ion-android-hand"></i>تطوع معنا ';
        document.getElementById('linkMenuSuccessStory').innerHTML = '<i class="ionicons ion-ios-paper"></i>قصص النجاح ';
        document.getElementById('linkMenuChangePassword').innerHTML = '<i class="ionicons ion-ios-paper"></i>تغير كلمة السر';
        document.getElementById('linkMenuToSignout').innerHTML = '<i class="icon ion-log-out"></i>تسجيل الخروج';
        document.getElementById('linkMenuToSignIn').innerHTML = '<i class="icon ion-log-in"></i>تسجيل الدخول';
        document.getElementById('linkMenuToSignUp').innerHTML = '<i class="icon ion-person-add"></i>تسجيل حساب جديد';
        document.getElementById('linkMenuSetting').innerHTML = 'الاعدادات';
        // login page
        $("#LoginEmail").attr('placeholder', 'اسم المستخدم');
        $("#LoginPassword").attr('placeholder', 'كلمة المرور');
        document.getElementById('forgetPassLink').innerHTML = 'نسيت كلمة المرور؟';
        document.getElementById('btnLogin').innerHTML = '<i class="ionicons ion-log-in"></i>تسجيل الدخول';
       // document.getElementById('notRegTex').innerHTML = 'لست مسجلاً لدينا ؟';
        document.getElementById('btnSignup').innerHTML = '<i class="ionicons ion-person-add"></i>حساب جديد';
        document.getElementById('btnSkip').innerHTML = '<i class="ionicons ion-android-arrow-back"></i>تخطى';


        //forgetpassword
        $("#ForgetPassEmail").attr('placeholder', 'البريد الالكترونى');
        document.getElementById('btn_SendPassword').innerHTML = '<i class="ionicons ion-android-lock"></i>إرسال كلمة المرور';

        //sginup
        $("#txtName").attr('placeholder', 'الاسم');
        document.getElementById("lblNameRequired").innerHTML = 'يجب إدخال الاسم';
        $("#txtUserName").attr('placeholder', 'اسم المستخدم');
        document.getElementById("lblUserNameRequired").innerHTML = 'يجب إدخال اسم المستخدم';
        $("#txtMobile").attr('placeholder', 'الجوال');
        document.getElementById("lblMobileRequired").innerHTML = 'يجب إدخال رقم الجوال';
        document.getElementById("lblMobileNumberOnly").innerHTML = 'أدخل رقم الجوال بطريقة صحيحة';
        document.getElementById("lblMobileAllowedLength").innerHTML = 'رقم الجوال عشرة خانات فقط';
        $("#txtEmail").attr('placeholder', 'البريد الالكترونى');
        document.getElementById("lblEmailRequired").innerHTML = 'يجب إدخال البريد الإلكتروني';
        document.getElementById("lblEmailFormat").innerHTML = ' ادخل بريد الكترونى مشابه(مثال :someone@example.com)';
        document.getElementById("SelectedCity").innerHTML = 'المدينة';
        document.getElementById("lblCityRequired").innerHTML = 'يجب إدخال المدينة';
        $("#txtPassword").attr('placeholder', 'كلمة المرور');
        document.getElementById("lblPasswordRequired").innerHTML = 'يجب إدخال كلمة المرور';
        document.getElementById("lblPasswordAllowedLength").innerHTML = 'كلمة المرور لا تقل عن ستة احرف';
        $("#txtConfirmPassword").attr('placeholder', 'تأكيد كلمة المرور');
        document.getElementById("lblConfirmPasswordRequired").innerHTML = 'يجب تأكيد كلمة المرور';
        document.getElementById("lblConfirmPasswordCompare").innerHTML = 'كلمتا المرور غير متطابقتين';
        document.getElementById("btnSbmitRegistration").innerHTML = '<i class="ionicons ion-person-add"></i>تسجيل حساب جديد';
        document.getElementById("btnBackToLogin").innerHTML = '<i class="ionicons ion-log-in"></i>عودة لصفحة الدخول';
        $("#txtUserAge").attr('placeholder', 'العمر');
        document.getElementById("lblUserAgeRequired").innerHTML = 'يجب إدخال العمر';
        document.getElementById("lblUserAgeNumberOnly").innerHTML = 'أدخل العمر بطريقة صحيحة';
        document.getElementById("lblUserAgeAllowedLength").innerHTML = 'العمر ثلاث خانات فقط';
        document.getElementById("LblIsOwnerForShelter").innerHTML = 'هل انت صاحب ملجأ او جمعية؟';
        
        //activation
        $("#txtActivationCode").attr('placeholder', 'كود التفعيل');
        document.getElementById("btn_ReSendPassword").innerHTML = 'اعادة ارسال الكود';
        document.getElementById("btnActivation").innerHTML = '<i class="ionicons ion-log-in"></i>تفعيل';
        document.getElementById("btnActivtionBack").innerHTML = '<i class="ionicons ion-log-in"></i>عودة';
        document.getElementById("txtActivationhnt").innerHTML = 'ستصلك رسالة بكود التفعيل على جوالك';
      
        // home page
        document.getElementById("NoAnimal").innerHTML = 'لا يوجد حيوانات';
        HomeTitle = "الرئيسية";
        SearchResultTitle = "نتائج البحث";

        document.getElementById("NoAnimalSearch").innerHTML = 'لا يوجد للبحث';
        //search 
        document.getElementById("searchPageTitle").innerHTML = '<i class="ionicons ion-ios-search-strong"></i> البحث';
        $("#txt_SearchOwnerName").attr('placeholder', 'اسم الملجأ او اسم المستخدم');
        $("#txt_SearchAnimalName").attr('placeholder', 'اسم الحيوان');
        document.getElementById("btn_search").innerHTML = '<i class="ionicons ion-ios-search-strong"></i>بحث';
        //   myApp.closeModal('#searchPage');
        //document.getElementById("btn_closeSearch").innerHTML = '<i class="ionicons ion-log-in"></i>إغلاق';


        //Shelter
        document.getElementById("ShelterPageTitle").innerHTML = 'الملاجئ';
        document.getElementById("NoShelter").innerHTML = 'لا يوجد ملاجئ.';

        //ShelterDetaies
        document.getElementById("ShelterDetails").innerHTML = 'الملاجئ';
        document.getElementById("AnimaltabTitle").innerHTML = 'الحيوانات';
        document.getElementById("AnimalInfotabTitle").innerHTML = 'المعلومات';
        document.getElementById("NoAnimalInShelter").innerHTML = 'لا يوجد حيوانات فى الملجأ';
        document.getElementById("btnvolunteer").innerHTML = 'طلب تطوع';
        document.getElementById("btnleaveRequest").innerHTML = 'طلب تخلى';
        document.getElementById("btnNotification").innerHTML = 'تفعيل الاشعارات';


        //users
        document.getElementById("UserTitle").innerHTML = 'المستخدمين';
        document.getElementById("NoUserFound").innerHTML = 'لا يوجد مستخدمين .';
        //userDetaies
        document.getElementById("UserDetailesTitle").innerHTML = 'تفاصيل المستخدم';
        document.getElementById("UserAnimalTab").innerHTML = 'الحيوانات';
        document.getElementById("UserInfTab").innerHTML = 'المعلومات';
        document.getElementById("noUserAnimal").innerHTML = 'لا يوجد حيوانات.';
        //profile
        document.getElementById("ProfilePageTitle").innerHTML = 'الصفحة الشخصية';
        document.getElementById("btnEditProfil").innerHTML = '<i class="fa fa-edit"></i> تعديل البيانات';
        document.getElementById("UserRequeste").innerHTML = 'حيواناتى';
        document.getElementById("btnAdopt").innerHTML = 'تبنى';
        document.getElementById("btnLeave").innerHTML = 'تخلى';
        document.getElementById("btnVolunteer").innerHTML = 'تطوع';
        document.getElementById("btnuserReport").innerHTML = 'بلاغات';
        document.getElementById("btnAdoptRequest").innerHTML = 'طلبات التنبى';

        //EditProfile
        document.getElementById("EditProfilePageTitle").innerHTML = '<i class="ionicons ion-edit"></i> تعديل البيانات الشخصية';
        document.getElementById("btnUploadImage").innerHTML = '<i class="ionicons ion-image"></i>الصورة الشخصية';
        $("#txtEditUserName").attr('placeholder', 'الاسم');
        $("#txtEditUserEmail").attr('placeholder', 'البريد الالكترونى');
        $("#TxtEditUserMobile").attr('placeholder', 'الجوال');
        document.getElementById("lblTxtEditUserMobileRequired").innerHTML = 'يجب إدخال الجوال';
        document.getElementById("lblTxtEditUserMobileNumberOnly").innerHTML = 'أدخل رقم الجوال بطريقة صحيحة';
        document.getElementById("lblTxtEditUserMobileAllowedLength").innerHTML = 'رقم الجوال عشرة خانات فقط';

        document.getElementById("btnUserProfileEdit").innerHTML = '<i class="ionicons ion-edit"></i>تعديل ';
        document.getElementById("btnUserProfileClose").innerHTML = '<i class="ionicons ion-log-in"></i>إغلاق';
        document.getElementById("lblEditUserGenderRequired").innerHTML = 'يجب إختيار الجنس ';
        document.getElementById("lblEditUserCitiesDllRequired").innerHTML = 'يجب إختيار المدينة ';
        document.getElementById("lbltxtEditUserNameRequired").innerHTML = 'الاسم لا يمكن ان يكون فارغ';
        $("#txtEditUserAge").attr('placeholder', 'العمر');
        document.getElementById("lblEditUserAgeRequired").innerHTML = 'يجب إدخال العمر';
        document.getElementById("lblEditUserAgeNumberOnly").innerHTML = 'أدخل العمر بطريقة صحيحة';
        document.getElementById("lblEditUserAgeAllowedLength").innerHTML = 'العمر ثلاث خانات فقط';
        document.getElementById("btnShowInfo").innerHTML = 'اظهر معلوماتى';
        //ProfilePopUps
        AdaptionRequestsTitle = 'طلبات تبنى';
        AbandonRequestsTitle = 'طلبات تخلى';
        ReportsTitle = 'بلاغات الحيوانات المفقودة';
        document.getElementById("VolunteerPopupPageTitle").innerHTML = '<i class="ionicons ion-ios-paw"></i> طلبات التطوع';
        document.getElementById("NoVolunteerRequest").innerHTML = 'لا يوجد طلبات .';
        document.getElementById("btnCloseVolunteer").innerHTML = '<i class="ionicons ion-log-in"></i>إغلاق';
        document.getElementById("NoAnimalpopUp").innerHTML = 'لا يوجد حيوانات';
        document.getElementById("btncloseAdoptPopup").innerHTML = '<i class="ionicons ion-log-in"></i>اغلاق';

        //volunteerPage
        document.getElementById("volunteerWithUs").innerHTML = 'تطوع معنا';
        ddlvolunteerForTxt = 'تطوع لدى';
        ddlvolunteerTypeTxt = 'نوع التطوع';
        document.getElementById("btnSaveVolunteer").innerHTML = '<i class="ionicons ion-checkmark-round"></i>تطوع';

        // giveup 
        document.getElementById("LeaveRequestPageTitle").innerHTML = '<i class="ionicons ion-checkmark-round"></i> بيانات تخلى';
        document.getElementById("lblLeaveAnimalCityRequired").innerHTML = 'يجب إختيار المدينة';
        document.getElementById("lblLeaveAnimalGenderRequired").innerHTML = 'يجب إختيار الجنس';
        document.getElementById("lblLeaveAnimalTypeRequired").innerHTML = 'ييجب إختيار تصنيف الحيوان';

        $("#txtLeaveAnimalName").attr('placeholder', 'اسم الحيوان');
        document.getElementById("lblLeaveAnimalNameRequired").innerHTML = 'يجب إدخال اسم الحيوان';
        $("#txtLeaveAnimalAge").attr('placeholder', 'عمر الحيوان');
        document.getElementById("lblLeaveAnimalAgeRequired").innerHTML = 'يجب إدخال السن';
        document.getElementById("lblLeaveAnimalAgeNumberOnly").innerHTML = 'يمكنك ادخال ارقم فقط';
        document.getElementById("lblLeaveAnimalAgeAllowedLength").innerHTML = 'العدد لا يمكن ان يزد عن 3 خانات';
        $("#txtAnimalLeaveVaccine").attr('placeholder', 'التطعيمات التى تلقاها الحيوان');
        $("#divLeaveAnimalAddress").attr('placeholder', 'العنوان');
        document.getElementById("lblLeaveAnimalLocationRequired").innerHTML = 'يجب إدخال العنوان';
        GiveUpForTxt = "تخلى من اجل";
        document.getElementById("lblleaveForRequired").innerHTML = 'يجب اختيار الملجأ';
        //$("#TxtGiveUpLeave").attr('placeholder', 'ما سبب تخليك عن الحيوان ؟');
        //$("#txtLeaveAnimalDescription").attr('placeholder', 'نبذة عن الحيوان');

        document.getElementById("TxtGiveUpLeave").placeholder = 'ما سبب تخليك عن الحيوان ؟';
        document.getElementById("txtLeaveAnimalDescription").placeholder = 'نبذة عن الحيوان';
        document.getElementById("LeaveAnimalNotes").placeholder = 'ملاحظات';


        document.getElementById("UploadLeaveAnimalPhoto").innerHTML = '<i class="ionicons ion-image"></i>صورة الحيوان';
        document.getElementById("btnsaveLeaveRequest").innerHTML = '<i class="ionicons ion-checkmark-round"></i>تخلى';
        document.getElementById("btnCloseLeaveRequest").innerHTML = '<i class="ionicons ion-log-in"></i>إغلاق';

        //reportPopupPage
        document.getElementById("ReportPopupTitle").innerHTML = '<i class="ionicons ion-android-alert"></i> إبلاغ عن مستخدم';
        $("#ReportDescription").attr('placeholder', 'أكتب بلاغك ...');
        document.getElementById("btnSubmitreport").innerHTML = '<i class="ionicons ion-chatboxes"></i>إبلاغ';
        document.getElementById("btnClosereport").innerHTML = '<i class="ionicons ion-chatboxes"></i>إغلاق';
        document.getElementById('lblReportDescriptionRequired').innerHTML = 'يجب ادخال سبب البلاغ';

        //Comments
        document.getElementById("CommentsPageTitle").innerHTML = '<i class="ionicons ion-edit"></i> تعليق';
        $("#txtComment").attr('placeholder', 'أكتب تعليقك ...');
        document.getElementById("Btn_AddCommect").innerHTML = '<i class="ionicons ion-chatboxes"></i>تعليق';
        document.getElementById("Btn_CloseCommect").innerHTML = '<i class="ionicons ion-chatboxes"></i>اغلاق';

        //messages
        document.getElementById("messagesPageTitle").innerHTML = 'الرسائل';
        $("#txtMassage").attr('placeholder', 'اكتب رسالتك...');
        document.getElementById("NoMessages").innerHTML = 'لا يوجد رسائل';
        document.getElementById("NoChat").innerHTML = 'لا يوجد رسائل';
        document.getElementById("Btn_NewChat").innerHTML = 'ابدأ بالارسال ';
    //    document.getElementById("ChatPagetitle").innerHTML = 'الرسائل';
        document.getElementById("btn_sendMSG").innerHTML = 'إرسل';
        //AnimalDetailes
        document.getElementById("animalDetailsPagetitle").innerHTML = 'تفاصيل الحيوان';
        document.getElementById("btnAdoptedRequest").innerHTML = 'طلب تبنى';
        document.getElementById("NoCommentsForAnimal").innerHTML = 'لا يوجد تعليقات .';



        //AdaptationData
        document.getElementById("adoptedRequestPagetitle").innerHTML = '<i class="ionicons ion-checkmark-round"></i> بيانات تبنى';
        document.getElementById("btnSaveAdopteRequest").innerHTML = ' <i class="ionicons ion-checkmark-round"></i>تبنى';
        document.getElementById("btncloseAdopteRequest").innerHTML = '<i class="ionicons ion-log-in"></i>إغلاق';

        $("#adoptedRequestFullName").attr('placeholder', 'الاسم الثلاثي للمتبني');
        document.getElementById("lbladoptedRequestFullNameRequired").innerHTML = 'من فضلك ادخل الاسم الثلاثي للمتبني';

        $("#adoptedRequestMobile").attr('placeholder', 'رقم جوال المتبني');
        document.getElementById("lbladoptedRequestMobileRequired").innerHTML = 'من فضلك ادخل رقم جوال المتبني';
        document.getElementById("lbladoptedRequestMobileAllowedLength").innerHTML = 'رقم جوال المتبني عشرة خانات فقط ';


        $("#adoptedRequestAge").attr('placeholder', 'العمر');
        document.getElementById("lbladoptedRequestAgeRequired").innerHTML = 'من فضلك ادخل  العمر';
        document.getElementById("lbladoptedRequestAgeAllowedLength").innerHTML = 'العمر لا يمكن ان يزيد عن ثلاثة خانات';

        $("#adoptedRequestEmail").attr('placeholder', 'البريد الالكترونى');
        document.getElementById("lbladoptedRequestEmailRequired").innerHTML = 'من فضلك ادخل  البريد الالكترونى';
        document.getElementById("lbladoptedRequestEmailFormat").innerHTML = 'من فضلك ادخل البريد الالكترونى بطريقة صحيحة';


        $("#adoptedRequestAddress").attr('placeholder', 'المدينة / العنوان');
        document.getElementById("lbladoptedRequestAddressRequired").innerHTML = 'من فضلك ادخل المدينة / العنوان';

        $("#adoptedRequestSocialStatus").attr('placeholder', 'الحالة الاجتماعية');
        document.getElementById("lbladoptedRequestSocialStatusRequired").innerHTML = 'من فضلك ادخل الحالة الاجتماعية ';

        $("#adoptedRequestQuestion1").attr('placeholder', 'لماذا ترغب في التبني ولأجل من؟');
        document.getElementById("lbladoptedRequestQuestion1Required").innerHTML = 'من فضلك ادخل الاجابة ';

        $("#adoptedRequestQuestion2").attr('placeholder', 'هل قمت بتربية اى حيوان اليف مسبقا؟ مازلت تحتفظ به ام ماذا حدث؟');
        document.getElementById("lbladoptedRequestQuestion2Required").innerHTML = 'من فضلك ادخل الاجابة ';


        document.getElementById("adoptedRequestQuestion3").innerHTML = 'اذا كان لديك اي قطط/كلاب/حيوانات اخرى حاليا هل جميعها معقمه اناث بدون رحم او ذكور مخصيه لا تتكاثر؟';
        document.getElementById("lbladoptedRequestQuestion3Required").innerHTML = 'من فضلك اختر الاجابة ';
        $("#Question3YesLbl").val(YES);
        $("#Question3Nolbl").val(nO);
        document.getElementById("Question3Nolbl").innerHTML = nO;
        document.getElementById("Question3Nodiv").innerHTML = nO;
        document.getElementById("Question3YesLbl").innerHTML = YES;
        document.getElementById("Question3YesDiv").innerHTML = YES;


        document.getElementById("adoptedRequestQuestion4").innerHTML = 'هل تقوم برعاية حيوانات اليفه اخرى؟';
        document.getElementById("lbladoptedRequestQuestion4Required").innerHTML = 'من فضلك اختر الاجابة ';
        $("#Question4YesLbl").val(YES);
        $("#Question4Nolbl").val(nO);
        document.getElementById("Question4Nolbl").innerHTML = nO;
        document.getElementById("Question4Nodiv").innerHTML = nO;
        document.getElementById("Question4YesLbl").innerHTML = YES;
        document.getElementById("Question4YesDiv").innerHTML = YES;


        document.getElementById("adoptedRequestQuestion5").innerHTML = 'هل لديك القدرة على أخذ حيواناتك الاليفة للعيادة البيطريه الموثوقة لمتابعة التطعيمات السنوية والكشف الدوري والحالات الطارئة؟';
        document.getElementById("lbladoptedRequestQuestion5Required").innerHTML = 'من فضلك اختر الاجابة ';
        $("#Question5YesLbl").val(YES);
        $("#Question5Nolbl").val(nO);
        document.getElementById("Question5Nolbl").innerHTML = nO;
        document.getElementById("Question5Nodiv").innerHTML = nO;
        document.getElementById("Question5YesLbl").innerHTML = YES;
        document.getElementById("Question5YesDiv").innerHTML = YES;

        $("#adoptedRequestQuestion6").attr('placeholder', 'مع أي عيادة بيطريه تفضل التعامل؟ يرجى تحديد الاسم.');
        document.getElementById("lbladoptedRequestQuestion6Required").innerHTML = 'من فضلك ادخل الاجابة ';

        document.getElementById("adoptedRequestQuestion7").innerHTML = 'هل لديك خلفيه عن طريقة تقديم القط/الكلب/حيوان اخر الى افراد العائله والمنزل الجديد؟ ';
        document.getElementById("lbladoptedRequestQuestion7Required").innerHTML = 'من فضلك اختر الاجابة ';
        $("#Question7YesLbl").val(YES);
        $("#Question7Nolbl").val(nO);
        document.getElementById("Question7Nolbl").innerHTML = nO;
        document.getElementById("Question7Nodiv").innerHTML = nO;
        document.getElementById("Question7YesLbl").innerHTML = YES;
        document.getElementById("Question7YesDiv").innerHTML = YES;

        $("#adoptedRequestQuestion8").attr('placeholder', 'اين سيقضي الحيوان معظم وقته؟');
        document.getElementById("lbladoptedRequestQuestion8Required").innerHTML = 'من فضلك ادخل الاجابة ';

        document.getElementById("adoptedRequestQuestion9").innerHTML = 'هل لديك القدرة الماديه لتحمل مسؤولية الحيوان الاليف والعناية به في الصحة والمرض وتوفير مستلزماته ؟';
        document.getElementById("lbladoptedRequestQuestion9Required").innerHTML = 'من فضلك ادخل الاجابة ';
        $("#Question9YesLbl").val(YES);
        $("#Question9Nolbl").val(nO);
        document.getElementById("Question9Nolbl").innerHTML = nO;
        document.getElementById("Question9Nodiv").innerHTML = nO;
        document.getElementById("Question9YesLbl").innerHTML = YES;
        document.getElementById("Question9YesDiv").innerHTML = YES;

        $("#adoptedRequestQuestion10").attr('placeholder', 'ماذا ستقدم للحيوان الاليف من طعام؟');
        document.getElementById("lbladoptedRequestQuestion10Required").innerHTML = 'من فضلك ادخل الاجابة ';

        $("#adoptedRequestQuestion11").attr('placeholder', 'ما هي المستلزمات الأساسيه التي تعتقد انه يتوجب عليك توفيرها للحيوان الاليف ؟');
        document.getElementById("lbladoptedRequestQuestion11Required").innerHTML = 'من فضلك ادخل الاجابة ';

        $("#adoptedRequestQuestion12").attr('placeholder', 'ماذا تعرف عن عملية التعقيم والاخصاء وما هو رأيك بذلك؟');
        document.getElementById("lbladoptedRequestQuestion12Required").innerHTML = 'من فضلك ادخل الاجابة ';

        document.getElementById("adoptedRequestQuestion13").innerHTML = 'قد تطالب برسوم تبني قيمتها  300 ريال للقط 300-800 للكلاب و قد تتفاوت بين الجميعات و الافراد بناء على متطلباتهم وتمثل (تكلفة عملية التعقيم أوالإخصاء و التطعيمات الكاملة التي حصل عليها). هل انت على استعداد لدفع الرسوم عند التبني؟';
        document.getElementById("lbladoptedRequestQuestion13Required").innerHTML = 'من فضلك ادخل الاجابة ';
        $("#Question13YesLbl").val(YES);
        $("#Question13Nolbl").val(nO);
        document.getElementById("Question13Nolbl").innerHTML = nO;
        document.getElementById("Question13Nodiv").innerHTML = nO;
        document.getElementById("Question13YesLbl").innerHTML = YES;
        document.getElementById("Question13YesDiv").innerHTML = YES;

        document.getElementById("adoptedRequestQuestion21").innerHTML = 'نوع السكن';
        document.getElementById("lbladoptedRequestQuestion13Required").innerHTML = 'من فضلك ادخل الاجابة ';
        $("#Question21VillaLbl").val('فيلا');
        $("#Question21Storeylbl").val('دور');
        $("#Question21ApartmentLbl").val('شقة');
        $("#Question21Otherlbl").val('غير ذلك');
        document.getElementById("Question21VillaLbl").innerHTML = 'فيلا';
        document.getElementById("Question21VillaDiv").innerHTML = 'فيلا';
        document.getElementById("Question21Storeylbl").innerHTML = 'دور';
        document.getElementById("Question21Storeydiv").innerHTML = 'دور';
        document.getElementById("Question21ApartmentLbl").innerHTML = 'شقة';
        document.getElementById("Question21ApartmentDiv").innerHTML = 'شقة';
        document.getElementById("Question21Otherlbl").innerHTML = 'غير ذلك';
        document.getElementById("Question21Otherdiv").innerHTML = 'غير ذلك';

        document.getElementById("adoptedRequestQuestion22").innerHTML = 'هل جميع افراد العائله او من يشاركك في السكن راضِ عن تبني الحيوان الاليف؟';
        document.getElementById("lbladoptedRequestQuestion22Required").innerHTML = 'من فضلك ادخل الاجابة ';
        $("#Question22YesLbl").val(YES);
        $("#Question22Nolbl").val(nO);
        document.getElementById("Question22Nolbl").innerHTML = nO;
        document.getElementById("Question22Nodiv").innerHTML = nO;
        document.getElementById("Question22YesLbl").innerHTML = YES;
        document.getElementById("Question22YesDiv").innerHTML = YES;


        document.getElementById("adoptedRequestQuestion23").innerHTML = 'هل يوجد أطفال في العائلة اذا كان كذلك يرجى ذكر الأعمار؟';
        document.getElementById("lbladoptedRequestQuestion23Required").innerHTML = 'من فضلك ادخل الاجابة ';
        $("#Question23YesLbl").val(YES);
        $("#Question23Nolbl").val(nO);
        document.getElementById("Question23Nolbl").innerHTML = nO;
        document.getElementById("Question23Nodiv").innerHTML = nO;
        document.getElementById("Question23YesLbl").innerHTML = YES;
        document.getElementById("Question23YesDiv").innerHTML = YES;
        $("#Question23Text").attr('placeholder', 'الاعمار');

        document.getElementById("adoptedRequestQuestion24").innerHTML = 'هل انت متأكد ان جميع من يشاركك السكن لا يعاني من حساسية تجاه الحيوانات الأليفة؟';
        document.getElementById("lbladoptedRequestQuestion24Required").innerHTML = 'من فضلك ادخل الاجابة ';
        $("#Question24YesLbl").val(YES);
        $("#Question24Nolbl").val(nO);
        document.getElementById("Question24Nolbl").innerHTML = nO;
        document.getElementById("Question24Nodiv").innerHTML = nO;
        document.getElementById("Question24YesLbl").innerHTML = YES;
        document.getElementById("Question24YesDiv").innerHTML = YES;

        $("#adoptedRequestQuestion25").attr('placeholder', 'كم من الوقت بإمكانك تخصيصه لرعاية الحيوان الأليف يوميا؟');
        document.getElementById("lbladoptedRequestQuestion25Required").innerHTML = 'من فضلك ادخل الاجابة ';


        document.getElementById("adoptedRequestQuestion26").innerHTML = 'هل انت كثير السفر والتنقل؟';
        document.getElementById("lbladoptedRequestQuestion26Required").innerHTML = 'من فضلك ادخل الاجابة ';
        $("#Question26YesLbl").val(YES);
        $("#Question26Nolbl").val(nO);
        document.getElementById("Question26Nolbl").innerHTML = nO;
        document.getElementById("Question26Nodiv").innerHTML = nO;
        document.getElementById("Question26YesLbl").innerHTML = YES;
        document.getElementById("Question26YesDiv").innerHTML = YES;

        $("#adoptedRequestQuestion27").attr('placeholder', 'اين ستضع الحيوان الأليف في حال السفر؟');
        document.getElementById("lbladoptedRequestQuestion27Required").innerHTML = 'من فضلك ادخل الاجابة ';
        document.getElementById("adoptedRequesteDetailstitle").innerHTML = 'تفاصيل طلب التبنى';


        //adoptedRequesteDetails
        
        document.getElementById("adoptedRequesteDetailsFullName").innerHTML = ' <i class="ionicons ion-document-text"></i>الاسم الثلاثي للمتبني';
        document.getElementById("adoptedRequesteDetailsMobile").innerHTML = ' <i class="ionicons ion-document-text"></i>رقم جوال المتبني ';
        document.getElementById("adoptedRequesteDetailsAge").innerHTML = ' <i class="ionicons ion-document-text"></i>العمر';
        document.getElementById("adoptedRequesteDetailsEmail").innerHTML = ' <i class="ionicons ion-document-text"></i>البريد الالكترونى';
        document.getElementById("adoptedRequesteDetailsAddress").innerHTML = ' <i class="ionicons ion-document-text"></i>المدينة / العنوان';
        document.getElementById("adoptedRequesteDetailsSocialStatus").innerHTML = ' <i class="ionicons ion-document-text"></i>الحالة الاجتماعية';
        document.getElementById("adoptedRequesteDetailsQuestion1").innerHTML = ' <i class="ionicons ion-document-text"></i>لماذا ترغب في التبني ولأجل من؟';
        document.getElementById("adoptedRequesteDetailsQuestion2").innerHTML = ' <i class="ionicons ion-document-text"></i>هل قمت بتربية اى حيوان اليف مسبقا؟ مازلت تحتفظ به ام ماذا حدث؟';
        document.getElementById("adoptedRequesteDetailsQuestion3").innerHTML = ' <i class="ionicons ion-document-text"></i>اذا كان لديك اي قطط/كلاب/حيوانات اخرى حاليا هل جميعها معقمه اناث بدون رحم او ذكور مخصيه لا تتكاثر؟';
        document.getElementById("adoptedRequesteDetailsQuestion4").innerHTML = ' <i class="ionicons ion-document-text"></i>هل تقوم برعاية حيوانات اليفه اخرى؟';
        document.getElementById("adoptedRequesteDetailsQuestion5").innerHTML = ' <i class="ionicons ion-document-text"></i>هل لديك القدرة على أخذ حيواناتك الاليفة للعيادة البيطريه الموثوقة لمتابعة التطعيمات السنوية والكشف الدوري والحالات الطارئة؟';
        document.getElementById("adoptedRequesteDetailsQuestion6").innerHTML =  ' <i class="ionicons ion-document-text"></i>مع أي عيادة بيطريه تفضل التعامل؟ يرجى تحديد الاسم.';
        document.getElementById("adoptedRequesteDetailsQuestion7").innerHTML = ' <i class="ionicons ion-document-text"></i>هل لديك خلفيه عن طريقة تقديم القط/الكلب/حيوان اخر الى افراد العائله والمنزل الجديد؟ ';
        document.getElementById("adoptedRequesteDetailsQuestion8").innerHTML =  ' <i class="ionicons ion-document-text"></i>اين سيقضي الحيوان معظم وقته؟';
        document.getElementById("adoptedRequesteDetailsQuestion9").innerHTML = ' <i class="ionicons ion-document-text"></i>هل لديك القدرة الماديه لتحمل مسؤولية الحيوان الاليف والعناية به في الصحة والمرض وتوفير مستلزماته ؟';
        document.getElementById("adoptedRequesteDetailsQuestion10").innerHTML = ' <i class="ionicons ion-document-text"></i>ماذا ستقدم للحيوان الاليف من طعام؟';
        document.getElementById("adoptedRequesteDetailsQuestion11").innerHTML = ' <i class="ionicons ion-document-text"></i>ما هي المستلزمات الأساسيه التي تعتقد انه يتوجب عليك توفيرها للحيوان الاليف ؟';
        document.getElementById("adoptedRequesteDetailsQuestion12").innerHTML = ' <i class="ionicons ion-document-text"></i>ماذا تعرف عن عملية التعقيم والاخصاء وما هو رأيك بذلك؟';
        document.getElementById("adoptedRequesteDetailsQuestion13").innerHTML = ' <i class="ionicons ion-document-text"></i>قد تطالب برسوم تبني قيمتها  300 ريال للقط 300-800 للكلاب و قد تتفاوت بين الجميعات و الافراد بناء على متطلباتهم وتمثل (تكلفة عملية التعقيم أوالإخصاء و التطعيمات الكاملة التي حصل عليها). هل انت على استعداد لدفع الرسوم عند التبني؟';
        document.getElementById("adoptedRequesteDetailsQuestion21").innerHTML = ' <i class="ionicons ion-document-text"></i>نوع السكن';
        document.getElementById("adoptedRequesteDetailsQuestion22").innerHTML = ' <i class="ionicons ion-document-text"></i>هل جميع افراد العائله او من يشاركك في السكن راضِ عن تبني الحيوان الاليف؟';
        document.getElementById("adoptedRequesteDetailsQuestion23").innerHTML = ' <i class="ionicons ion-document-text"></i>هل يوجد أطفال في العائلة اذا كان كذلك يرجى ذكر الأعمار؟';
        document.getElementById("adoptedRequesteDetailsQuestion24").innerHTML = ' <i class="ionicons ion-document-text"></i>هل انت متأكد ان جميع من يشاركك السكن لا يعاني من حساسية تجاه الحيوانات الأليفة؟';
        document.getElementById("adoptedRequesteDetailsQuestion25").innerHTML = ' <i class="ionicons ion-document-text"></i>كم من الوقت بإمكانك تخصيصه لرعاية الحيوان الأليف يوميا؟';
        document.getElementById("adoptedRequesteDetailsQuestion26").innerHTML = ' <i class="ionicons ion-document-text"></i>هل انت كثير السفر والتنقل؟';
        document.getElementById("adoptedRequesteDetailsQuestion27").innerHTML = ' <i class="ionicons ion-document-text"></i>اين ستضع الحيوان الأليف في حال السفر؟';
       
        //Add animal
        $("#txtAnimalName").attr('placeholder', 'اسم الحيوان');
        $('#txtAnimalAge').attr('placeholder', 'عمر الحيوان');
        document.getElementById("lblAgeNumberOnly").innerHTML = 'أدخل العمر بطريقة صحيحة';
        document.getElementById("lblAgeAllowedLength").innerHTML = 'العدد لا يمكن ان يزد عن 3 خانات';
        $("#txtAnimalVaccine").attr('placeholder', 'التطعيمات التى تلقاها الحيوان');
        $("#divAnimalAddress").attr('placeholder', 'العنوان');
        document.getElementById("lblAnimalAddressRequired").innerHTML = 'يجب إدخال العنوان';
        document.getElementById("UploadAnimalPhoto").innerHTML = '<i class="ionicons ion-image"></i>صورة الحيوان';
        document.getElementById("UploadAnimalVedio").innerHTML = '<i class="ionicons ion-image"></i>تحميل فيديو';
        document.getElementById("RecordAnimalVedio").innerHTML = '<i class="ionicons ion-image"></i>تسجيل فيديو';
        document.getElementById("UploadGiveUpAnimalVedio").innerHTML = '<i class="ionicons ion-image"></i>تحميل فيديو';
        document.getElementById("RecordGiveUpAnimalVedio").innerHTML = '<i class="ionicons ion-image"></i>تسجيل فيديو';
        DeleteAnimalConfirmMsg = "هل تريد حذف الحيوان؟";
        AddAnimalTitleEmergancy = "الإبلاغ عن حالة طارئة";
        AddAnimalTitleAboption = "إضافة حيوان للتبنى";
        AddAnimalTitleLost = "إضافة حيوان مفقود";
        EditAnimal = "تعديل الحيوان";
        EditBtnTxt = "تعديل";
        $("#ddlAnimalNotes").attr('placeholder', 'ملاحظات');
        document.getElementById("lblAnimalTypeRequired").innerHTML = 'يجب إختيار تصنيف الحيوان';
        document.getElementById("lblAnimalCityRequired").innerHTML = 'يجب إختيار المدينة';
        document.getElementById("lblAnimalGenderRequired").innerHTML = 'يجب إختيار الجنس';


        AddAnimalEmergancy = "حالة طارئة";
        AddAnimalAboption = "حيوان للتبنى";
        AddAnimalLost = "حيوان مفقود";
        AddAnimalGiveup = "حيوان للتخلى";
        AddAnimalReportBtn = '<i class="ionicons ion-ios-download-outline"></i>الأبلاغ'
        AddAnimalsubmitBtn = '<i class="ionicons ion-ios-download-outline"></i>إضافة حيوان'
        //lost animal 
        document.getElementById("lostAnimalsPageTitle").innerHTML = 'حيوانات مفقودة';
        document.getElementById("NoLostAnimalFound").innerHTML = 'لا يوجد حيوانات مفقودة .'
        //emergencies
        document.getElementById("needCareAnimalsPageTitle").innerHTML = 'حالات طارئة';
        document.getElementById("NoneedCareFound").innerHTML = 'لا يوجد حالات طائة .'
        //success stories        
        document.getElementById("successStoryPageTitle").innerHTML = 'قصص نجاح';
        document.getElementById("NoSuccessStory").innerHTML = 'لا يوجد قصص نجاح .'
        //Add success stories        
        document.getElementById("AddsuccessStoryPageTitle").innerHTML = 'إضافة قصة نجاح';
        document.getElementById("Btn_uploadSucessImg").innerHTML = '<i class="ionicons ion-image"></i>صور القصة ';
        $("#txt_SucessTitle").attr('placeholder', 'عنوان القصة');
        document.getElementById("lbladdSuccessStoryTitleRequired").innerHTML = 'يجب إدخال عنوان القصة';
        $("#txt_SucessDetails").attr('placeholder', 'تفاصيل القصة');
        document.getElementById("lbladdSuccessStoryDetailsRequired").innerHTML = 'يجب إدخال تفاصيل القصة';
        document.getElementById("btn_AddSucessStory").innerHTML = '<i class="ionicons ion-ios-download-outline"></i>إضافة';
        document.getElementById("UploadStoryVedio").innerHTML = '<i class="ionicons ion-image"></i>تحميل فيديو';
        document.getElementById("RecordStoryVedio").innerHTML = '<i class="ionicons ion-image"></i>تسجيل فيديو';
        //change password
        $("#txtChangeOldPassword").attr('placeholder', 'أدخل كلمة السر القديمة');
        document.getElementById("lblChangeOldPasswordRequired").innerHTML = 'يجب إدخال كلمة السر القديمة';
        $("#txtChangeNewPassword").attr('placeholder', 'أدخل كلمة السر الجديدة');
        document.getElementById("lblChangeNewPasswordRequired").innerHTML = 'يجب إدخال كلمة السر الجديدة';
        document.getElementById("lblChangeNewPasswordAllowedLength").innerHTML = 'كلمة المرور الجديدة يجب ألا تقل عن ستة خانات';
        $("#txtChangeConfirmNewPassword").attr('placeholder', 'أعد إدخال كلمة السر الجديدة');
        document.getElementById("lblChangeConfirmNewPasswordRequired").innerHTML = 'يجب تأكيد كلمة السر الجديدة';
        document.getElementById("lblChangeConfirmNewPasswordCompare").innerHTML = 'كلمتا المرور غير متطابقتين';
        document.getElementById("btnChangePassword").innerHTML = '<i class="ionicons ion-person-add"></i>تغيير';
        document.getElementById("btnPasswordBackToHome").innerHTML = '<i class="ionicons ion-android-arrow-forward"></i>عودة';

        //success stories detailes        
        document.getElementById("storyDetailsPageTitle").innerHTML = 'تفاصيل القصة';
        document.getElementById("NoCommentsForStory").innerHTML = 'لا يوجد تعليقات .';

        //add Comment For Story
        document.getElementById("CommentsStoryPageTitle").innerHTML = '<i class="ionicons ion-edit"></i> تعليق';
        $("#txtCommentStory").attr('placeholder', 'أكتب تعليقك ...');
        document.getElementById("Btn_AddCommectStory").innerHTML = '<i class="ionicons ion-chatboxes"></i>تعليق';
        document.getElementById("Btn_CloseCommectStory").innerHTML = '<i class="ionicons ion-chatboxes"></i>اغلاق';

        //notificationPage
        document.getElementById("notificationPageTitle").innerHTML = 'الاشعارات';
        document.getElementById("NoNotifications").innerHTML = 'ليس لديك اشعارات';

        //resetPasswordPage

        $("#txtResetCode").attr('placeholder', 'أدخل الكود');
        document.getElementById("lblresetCodeRequired").innerHTML = 'يجب إدخال الكود';
        $("#txtResetPassword").attr('placeholder', 'أدخل كلمة السر الجديدة');
        $("#txtResetCode").attr('placeholder', 'أدخل الكود');
        $("#txtResetConfirmPassword").attr('placeholder', 'أعد إدخال كلمة السر الجديدة');
        document.getElementById("lblresetPasswordRequired").innerHTML = 'يجب إدخال كلمة المرور';
        document.getElementById("lblresetPasswordAllowedLength").innerHTML = 'كلمة المرور يجب ألا تقل عن ستة خانات';

        document.getElementById("lblresetConfirmPasswordRequired").innerHTML = 'يجب تأكيد كلمة المرور الجديدة';
        document.getElementById("lblresetConfirmPasswordCompare").innerHTML = 'كلمتا المرور غير متطابقتين';
        document.getElementById("btnResetPassword").innerHTML = 'تغيير كلمة السر';
        document.getElementById("btnResetPasswordBackToHome").innerHTML = 'عودة';

        //AdoptRequestPopup
        document.getElementById("AdoptRequestPopupTitle").innerHTML = 'طلبات التبنى';
        document.getElementById("NoAdoptRequestpopUp").innerHTML = 'لا يوجد طلبات';
        document.getElementById("btncloseAdoptRequestPopup").innerHTML = 'اغلاق';

    }
    else if (CurrentLanguage == 'en') {
        //general
        MenuLogo.src = "img/smallLogoEn.png";
        landingPageIcon.src = "img/logoEn.png";
        loginLogo.src = "img/logoEn.png";
        signUpLogo.src = "img/logoEn.png";
        activationLogo.src = "img/logoEn.png";
        ForgetLogo.src = "img/logoEn.png";
        resetLogo.src = "img/logoEn.png";
        AgeError = 'Please enter valid Age';
        DeleteImg = "Do you want to delete the image?";
        DeleteVedio = "Do you want to delete the video?";
        showMsg = 'show my information';
        hideMsg = 'hide my information';
        showInfoDivMsg = "Personal information Appear to All users";
        HideInfoDivMsg = "Personal information Appear only to you";
        Confirm = "ok";
        Cancel = "Cancel";
        VedioDidntUpload = "Video not uploaded";
        reportAddedSucessFully = "Report Has Been Added Successfully";
        SucessAnimalDelete = "Pet Has Been Deleted Successfully";
        DeleteStoryConfirmMsg = "Do You Want To Delete The Story?";
        SucessStoryDelete = "Story Has Been Deleted Successfully";
        YouOWnAnimal = 'This Pet Belongs To You';
        reportValueRequired = "Please Enter the Cause Of Report";
        Visitor = "Visitor";
        AnimalCategory = "Pet";
        City = "City";
        MaleGender = "Male";
        FemaleGender = "Female";
        DefultGender = "Gender";
        ShelterTxt = "Shelters";
        loaderMSg = "Please Wait";
        VolunterAddedPefor = "You have an existing volunteering request";
        subscribeforShelter = 'Get Notification';
        UnsubscribeforShelter = 'Stop Notification';
        Wrong = 'Error';
        MonthesLbl = 'Month';
        ErrorOccured = 'Error Occured';
        warning = 'Warning';
        Sucess = 'Sucess';
        cantFindData = 'Data Can`t Be Verified';
        LoginError = 'Please Enter User Name and Password';
        LoginDataError = 'Wrong User Name Or Password';
        WrongActivationNo = 'Please Insert Correct Code';
        CodeSendSucess = 'Code Send To Your Phone Successfully';
        NeedConfirm = 'This Account Need To Activate';
        CodeSend = 'Code Send To Your Email Successfully';
        WrongEmail = 'Please Insert A Correct Email';
        PasswordChanged = 'Data Changed Successfully';
        NoNotAvilable = 'Phone Number Is Not Available';
        NoAge = 'NO Age For This Pet';
        notFound = 'Not Found';
        NotAuthenticated = 'You Need To Login first';
        RequestAdded = 'Request Has Been Added Successfully';
        storyAdded = 'Story  Has Been Added Successfully';
        FiledsEmpty = 'Fileds Cant Be Empty';
        OneImageAtLeast = 'You Must Add At Least One Photo';
        AnimalAdded = 'Pet Added Successfully';
        AnimalEditSucss = 'Pet Edited Successfully';
        CommentAdded = 'Comment Added Successfully';
        WrongOldPassWord = 'Old Password Is Wrong'
        closeTxt = 'Close';
        ChoseImage = 'Image Is Not Chosen';
        LargNoOfImage = 'You Can Upload 3 Images Only';
        NotAuthenticatedreport = 'You Can`t Report Yourself';
        noAnimalName = 'No Name For Pet';
        PasswordError = "The password Can Not Be Less Than 6 Characters";
        EmailExistError = "Email Already Exists. "
        PhoneExistError = "Phone Already Exists.";
        UserNameExistError = "UserName Already Exists.";
        ArabicUserNameError = 'UserName Cant Be Arabic';
        AddAnimalEmergancy = "Urgent Cases Pet";
        AddAnimalAboption = "Adoption Pet";
        AddAnimalLost = "Lost Pets";
        AddAnimalGiveup = "Pet for Give up";
        PasswordEmptyError = 'Password Cant Be Spaces Only';
        YES = 'YES';
        nO = 'No';
        //menu
        document.getElementById('linkMenuIndex').innerHTML = '<i class="ionicons ion-ios-home"></i>Home Page';
        document.getElementById('linkMenuLanguage').innerHTML = '<i class="ionicons ion-earth"></i>Arabic';
        document.getElementById('linkMenuProfile').innerHTML = '<i class="ionicons ion-person"></i>Profile';
        document.getElementById('linkMenunotification').innerHTML = '<i class="ionicons ion-android-notifications"></i>Notifications <span class="notificationNumber" id="notificationCount"></span>';
         document.getElementById('linkMenuChat').innerHTML = '<i class="ionicons ion-chatboxes"></i>Messages';
        document.getElementById('linkMenuRefuge').innerHTML = '<i class="ionicons ion-android-apps"></i>Shelters And Voluntary Associations';
        document.getElementById('linkMenuUsers').innerHTML = '<i class="ionicons ion-person-stalker"></i>Users';
        document.getElementById('linkMenuAddAnimal').innerHTML = '<i class="ionicons ion-android-add-circle"></i>Add A Pet For Adoption';
        document.getElementById('linkMenuIostAnimals').innerHTML = '<i class="ionicons ion-close-circled"></i>Lost Pets';
        document.getElementById('linkMenuNeedCareAnimals').innerHTML = '<i class="ionicons ion-medkit"></i>Urgent Cases';
        document.getElementById('linkMenuVolunteer').innerHTML = '<i class="ionicons ion-android-hand"></i>Volunteer With Us';
        document.getElementById('linkMenuSuccessStory').innerHTML = '<i class="ionicons ion-ios-paper"></i>Success Stories';
        document.getElementById('linkMenuChangePassword').innerHTML = '<i class="ionicons ion-ios-paper"></i>Change Password';
        document.getElementById('linkMenuToSignout').innerHTML = '<i class="icon ion-log-out"></i>Sign Out';
        document.getElementById('linkMenuToSignIn').innerHTML = '<i class="icon ion-log-in"></i>Sign In';
        document.getElementById('linkMenuToSignUp').innerHTML = '<i class="icon ion-person-add"></i>Sign Up';
        document.getElementById('linkMenuSetting').innerHTML = 'Settings';



        // login page
        $("#LoginEmail").attr('placeholder', 'User Name');
        $("#LoginPassword").attr('placeholder', 'Password');
        document.getElementById('forgetPassLink').innerHTML = 'Forgot Password ?';
        document.getElementById('btnLogin').innerHTML = '<i class="ionicons ion-log-in"></i>Login';
       // document.getElementById('notRegTex').innerHTML = 'Not Registered ?';
        document.getElementById('btnSignup').innerHTML = '<i class="ionicons ion-person-add"></i>New Account';
        document.getElementById('btnSkip').innerHTML = '<i class="ionicons ion-android-arrow-back"></i>Skip';

        //forgetpassword
        $("#ForgetPassEmail").attr('placeholder', 'Email');
        document.getElementById('btn_SendPassword').innerHTML = '<i class="ionicons ion-android-lock"></i>Send Password';

        //sginup
        $("#txtName").attr('placeholder', 'Name');
        document.getElementById("lblNameRequired").innerHTML = 'User Name Is Required';
        $("#txtUserName").attr('placeholder', 'User Name');
        document.getElementById("lblUserNameRequired").innerHTML = 'User Name Is Required';
        $("#txtMobile").attr('placeholder', 'Mobile');
        document.getElementById("lblMobileRequired").innerHTML = 'Mobile Is Required';
        document.getElementById("lblMobileNumberOnly").innerHTML = 'Insert Correct Mobile Number';
        document.getElementById("lblMobileAllowedLength").innerHTML = 'Mobile Number 10 Digits Only ';
        $("#txtEmail").attr('placeholder', 'Email');
        document.getElementById("lblEmailRequired").innerHTML = 'Email Is Required';
        document.getElementById("lblEmailFormat").innerHTML = '  Email Shoud Be Like (ex:someone@example.com)';
        document.getElementById("SelectedCity").innerHTML = 'City';
        document.getElementById("lblCityRequired").innerHTML = 'City Is Required'
        $("#txtPassword").attr('placeholder', 'Password');
        document.getElementById("lblPasswordRequired").innerHTML = 'Password Is Required';
        document.getElementById("lblPasswordAllowedLength").innerHTML = 'Password Must Be More Than 6 Character';
        $("#txtConfirmPassword").attr('placeholder', 'Confirm Password');
        document.getElementById("lblConfirmPasswordRequired").innerHTML = 'Confirm Password Is Required';
        document.getElementById("lblConfirmPasswordCompare").innerHTML = 'Passwords Don`t Match';
        document.getElementById("btnSbmitRegistration").innerHTML = '<i class="ionicons ion-person-add"></i>Sign Up';
        document.getElementById("btnBackToLogin").innerHTML = '<i class="ionicons ion-log-in"></i>Back To Home';
        $("#txtUserAge").attr('placeholder', 'Age');
        document.getElementById("lblUserAgeRequired").innerHTML = 'Age Is Required';
        document.getElementById("lblUserAgeNumberOnly").innerHTML = 'Insert Correct Age';
        document.getElementById("lblUserAgeAllowedLength").innerHTML = 'Age Cant be more than 3 Digits';

        document.getElementById("LblIsOwnerForShelter").innerHTML = 'Are you the owner of a shelter or voluntary association?';
        //activation
        $("#txtActivationCode").attr('placeholder', 'Activation Code');
        document.getElementById("btn_ReSendPassword").innerHTML = 'Resend Activation Code';
        document.getElementById("btnActivation").innerHTML = '<i class="ionicons ion-log-in"></i>Activate';
        document.getElementById("btnActivtionBack").innerHTML = '<i class="ionicons ion-log-in"></i>Back';
        document.getElementById("txtActivationhnt").innerHTML = 'You will receive a activation code on your mobile phone';
        //home 
        document.getElementById("NoAnimal").innerHTML = 'No Pet Found';
        HomeTitle = "Home";
        SearchResultTitle = "Search Results";
        document.getElementById("NoAnimalSearch").innerHTML = 'No Result Found';
        //search 
        document.getElementById("searchPageTitle").innerHTML = '<i class="ionicons ion-ios-search-strong"></i> Search';
        $("#txt_SearchOwnerName").attr('placeholder', 'Shelter Name or User Name');
        $("#txt_SearchAnimalName").attr('placeholder', 'Pet Name');
        document.getElementById("btn_search").innerHTML = '<i class="ionicons ion-ios-search-strong"></i>Search';
        //  document.getElementById("btn_closeSearch").innerHTML = '<i class="ionicons ion-log-in"></i>Close';
        //Shelter
        document.getElementById("ShelterPageTitle").innerHTML = 'Shelters';
        document.getElementById("NoShelter").innerHTML = 'No Shelers Found';
        //ShelterDetaies
        document.getElementById("ShelterDetails").innerHTML = 'Shelters';
        document.getElementById("AnimaltabTitle").innerHTML = 'Pets';
        document.getElementById("AnimalInfotabTitle").innerHTML = 'Information';
        document.getElementById("NoAnimalInShelter").innerHTML = 'No Pet Found In This Shelter';
        document.getElementById("btnvolunteer").innerHTML = 'Volunteer';
        document.getElementById("btnleaveRequest").innerHTML = 'Give up';
        document.getElementById("btnNotification").innerHTML = 'Get Notifications';

        //users
        document.getElementById("UserTitle").innerHTML = 'Users';
        document.getElementById("NoUserFound").innerHTML = 'No Users Found';
        //userDetaies
        document.getElementById("UserDetailesTitle").innerHTML = 'User Details';
        document.getElementById("UserAnimalTab").innerHTML = 'Pets';
        document.getElementById("UserInfTab").innerHTML = 'Information';
        document.getElementById("noUserAnimal").innerHTML = 'No Pet Found';


        //profile
        document.getElementById("ProfilePageTitle").innerHTML = 'Profile';
        document.getElementById("btnEditProfil").innerHTML = '<i class="fa fa-edit"></i> Edit Profile';
        document.getElementById("UserRequeste").innerHTML = 'My Pets';
        document.getElementById("btnAdopt").innerHTML = 'Adoption';
        document.getElementById("btnLeave").innerHTML = 'Give Up';
        document.getElementById("btnVolunteer").innerHTML = 'Volunteer';
        document.getElementById("btnuserReport").innerHTML = 'Reports';
        document.getElementById("btnAdoptRequest").innerHTML = 'Adoption Request';

        //EditProfile
        document.getElementById("EditProfilePageTitle").innerHTML = '<i class="ionicons ion-edit"></i> Edit Profile';
        document.getElementById("btnUploadImage").innerHTML = '<i class="ionicons ion-image"></i>Profile Picture';
        $("#txtEditUserName").attr('placeholder', 'Name');
        $("#txtEditUserEmail").attr('placeholder', 'Email');
        $("#TxtEditUserMobile").attr('placeholder', 'Mobile');
        document.getElementById("lblTxtEditUserMobileRequired").innerHTML = 'Mobile Is Required';
        document.getElementById("lblTxtEditUserMobileNumberOnly").innerHTML = 'Insert Correct Mobile Number';
        document.getElementById("lblTxtEditUserMobileAllowedLength").innerHTML = 'Mobile Number 10 Digits Only';
        document.getElementById("btnUserProfileEdit").innerHTML = '<i class="ionicons ion-edit"></i>Edit ';
        document.getElementById("btnUserProfileClose").innerHTML = '<i class="ionicons ion-log-in"></i>Close';
        document.getElementById("lblEditUserGenderRequired").innerHTML = 'Gender Is Required';
        document.getElementById("lblEditUserCitiesDllRequired").innerHTML = 'City Is Required ';
        document.getElementById("lbltxtEditUserNameRequired").innerHTML = 'Name Is Required';
        $("#txtEditUserAge").attr('placeholder', 'Age');
        document.getElementById("lblEditUserAgeRequired").innerHTML = 'Age Is Required';
        document.getElementById("lblEditUserAgeNumberOnly").innerHTML = 'Insert Correct Age';
        document.getElementById("lblEditUserAgeAllowedLength").innerHTML = 'Age Cant be more than 3 Digits';
        document.getElementById("btnShowInfo").innerHTML = 'Show Information';
        //ProfilePopUps
        AdaptionRequestsTitle = 'Adoption Requests';
        AbandonRequestsTitle = 'Abandon Requests';
        ReportsTitle = 'Reports';
        document.getElementById("btncloseAdoptPopup").innerHTML = '<i class="ionicons ion-log-in"></i>Close';
        document.getElementById("VolunteerPopupPageTitle").innerHTML = '<i class="ionicons ion-ios-paw"></i>Volunteer Requests';
        document.getElementById("NoVolunteerRequest").innerHTML = 'No Requests Found';
        document.getElementById("btnCloseVolunteer").innerHTML = '<i class="ionicons ion-log-in"></i>Close';
        document.getElementById("NoAnimalpopUp").innerHTML = 'No Requests Found';

        //volunteerPage
        document.getElementById("volunteerWithUs").innerHTML = 'Volunteer With Us';
        ddlvolunteerForTxt = 'Volunteer For';
        ddlvolunteerTypeTxt = 'Volunteer Type';
        document.getElementById("btnSaveVolunteer").innerHTML = '<i class="ionicons ion-checkmark-round"></i> Volunteer';

        // giveup 
        document.getElementById("LeaveRequestPageTitle").innerHTML = '<i class="ionicons ion-checkmark-round"></i> Give Up Information';
        document.getElementById("lblLeaveAnimalCityRequired").innerHTML = 'City Is Required';
        document.getElementById("lblLeaveAnimalGenderRequired").innerHTML = 'Gender Is Required';
        document.getElementById("lblLeaveAnimalTypeRequired").innerHTML = 'Pet Is Required';
        $("#txtLeaveAnimalName").attr('placeholder', 'Pet Name');
        document.getElementById("lblLeaveAnimalNameRequired").innerHTML = 'Pet Name Is Required';
        $("#txtLeaveAnimalAge").attr('placeholder', 'Age');
        document.getElementById("lblLeaveAnimalAgeRequired").innerHTML = 'Age Is Required';
        document.getElementById("lblLeaveAnimalAgeNumberOnly").innerHTML = 'Age Must Be Digits Only'
        document.getElementById("lblLeaveAnimalAgeAllowedLength").innerHTML = 'Age Must be Less Than 3 Digits '
        $("#txtAnimalLeaveVaccine").attr('placeholder', 'Vaccinations Given To The Pet');
        $("#divLeaveAnimalAddress").attr('placeholder', 'Address');
        document.getElementById("lblLeaveAnimalLocationRequired").innerHTML = 'Address Is Required';
        GiveUpForTxt = "Leave For";
        document.getElementById("lblleaveForRequired").innerHTML = 'Shelter Is Required';
        //$("TxtGiveUpLeave").attr('placeholder', 'What is the reason for your endorsement pet?');
        //$("txtLeaveAnimalDescription").attr('placeholder', 'Animal Description');

        document.getElementById("TxtGiveUpLeave").placeholder = 'What is the reason behind your adoption request?';
        document.getElementById("txtLeaveAnimalDescription").placeholder = 'Pet Description';
        document.getElementById("LeaveAnimalNotes").placeholder = 'Notes';

        document.getElementById("UploadLeaveAnimalPhoto").innerHTML = '<i class="ionicons ion-image"></i>Pet Image';
        document.getElementById("btnsaveLeaveRequest").innerHTML = '<i class="ionicons ion-checkmark-round"></i>Give Up';
        document.getElementById("btnCloseLeaveRequest").innerHTML = '<i class="ionicons ion-log-in"></i>Close';

        //reportPopupPage
        document.getElementById("ReportPopupTitle").innerHTML = '<i class="ionicons ion-android-alert"></i> Report User';
        $("#ReportDescription").attr('placeholder', 'Write Your Report ...');
        document.getElementById("btnSubmitreport").innerHTML = '<i class="ionicons ion-chatboxes"></i>Report';
        document.getElementById("btnClosereport").innerHTML = '<i class="ionicons ion-chatboxes"></i>Close';
        //Comments
        document.getElementById("CommentsPageTitle").innerHTML = '<i class="ionicons ion-edit"></i> Comment';
        $("#txtComment").attr('placeholder', 'Write You Comment ...');
        document.getElementById("Btn_AddCommect").innerHTML = '<i class="ionicons ion-chatboxes"></i>Comment';
        document.getElementById("Btn_CloseCommect").innerHTML = '<i class="ionicons ion-chatboxes"></i>Close';
        document.getElementById('lblReportDescriptionRequired').innerHTML = 'Cause of report is required';


        //messages
        document.getElementById("messagesPageTitle").innerHTML = 'Messages';
        $("#txtMassage").attr('placeholder', 'Write Your Message...');
        document.getElementById("NoMessages").innerHTML = 'No Messages Found';
        document.getElementById("NoChat").innerHTML = 'No Messages Found';
        document.getElementById("Btn_NewChat").innerHTML = 'Start Sending';
       // document.getElementById("ChatPagetitle").innerHTML = 'Messages';
        document.getElementById("btn_sendMSG").innerHTML = 'Send';


        //AnimalDetailes
        document.getElementById("animalDetailsPagetitle").innerHTML = 'Pet Details';
        document.getElementById("btnAdoptedRequest").innerHTML = 'Adoption Request';
        document.getElementById("NoCommentsForAnimal").innerHTML = 'No Comments';

        //AdaptationData
        document.getElementById("adoptedRequestPagetitle").innerHTML = '<i class="ionicons ion-checkmark-round"></i>Adoption Form';
        document.getElementById("btnSaveAdopteRequest").innerHTML = '<i class="ionicons ion-checkmark-round"></i>Adopt';
        document.getElementById("btncloseAdopteRequest").innerHTML = '<i class="ionicons ion-log-in"></i>Close';
        document.getElementById("adoptedRequesteDetailstitle").innerHTML = 'Adopted requeste details';
        $("#adoptedRequestFullName").attr('placeholder', 'Name');
        document.getElementById("lbladoptedRequestFullNameRequired").innerHTML = 'Name is Required';

        //$("#adoptedRequestNationalId").attr('placeholder', 'National / Iqama Number');

        $("#adoptedRequestMobile").attr('placeholder', 'Mobile No.');
        document.getElementById("lbladoptedRequestMobileRequired").innerHTML = 'Mobile No. is Required';
        document.getElementById("lbladoptedRequestMobileAllowedLength").innerHTML = 'Mobile Number 10 Digits Only';


        $("#adoptedRequestAge").attr('placeholder', 'Age');
        document.getElementById("lbladoptedRequestAgeRequired").innerHTML = 'Age is Required';
        document.getElementById("lbladoptedRequestAgeAllowedLength").innerHTML = 'Age 3 Digits Only';

        $("#adoptedRequestEmail").attr('placeholder', 'Email');
        document.getElementById("lbladoptedRequestEmailRequired").innerHTML = 'Email is Required';
        document.getElementById("lbladoptedRequestEmailFormat").innerHTML = 'Insert Correct Email';


        $("#adoptedRequestAddress").attr('placeholder', 'City/Address');
        document.getElementById("lbladoptedRequestAddressRequired").innerHTML = 'City/Address is Required';

        $("#adoptedRequestSocialStatus").attr('placeholder', 'Social Status');
        document.getElementById("lbladoptedRequestSocialStatusRequired").innerHTML = 'Social Status is Required';

        $("#adoptedRequestQuestion1").attr('placeholder', 'Why do you want to adopt a pet?');
        document.getElementById("lbladoptedRequestQuestion1Required").innerHTML = 'Please Enter The Answer';

        $("#adoptedRequestQuestion2").attr('placeholder', 'Have you ever had pets before? If so, what kind of pets were they? How long have you had them and where are they now?');
        document.getElementById("lbladoptedRequestQuestion2Required").innerHTML = 'Please Enter The Answer';


        document.getElementById("adoptedRequestQuestion3").innerHTML = 'If you have any cats/dogs/other animals, Are all of them are spayed/neutered?';
        document.getElementById("lbladoptedRequestQuestion3Required").innerHTML = 'Please Enter The Answer';
        $("#Question3YesLbl").val(YES);
        $("#Question3Nolbl").val(nO);
        document.getElementById("Question3Nolbl").innerHTML = nO;
        document.getElementById("Question3Nodiv").innerHTML = nO;
        document.getElementById("Question3YesLbl").innerHTML = YES;
        document.getElementById("Question3YesDiv").innerHTML = YES;


        document.getElementById("adoptedRequestQuestion4").innerHTML = 'Do you have any other pets?';
        document.getElementById("lbladoptedRequestQuestion4Required").innerHTML = 'Please Enter The Answer';
        $("#Question4YesLbl").val(YES);
        $("#Question4Nolbl").val(nO);
        document.getElementById("Question4Nolbl").innerHTML = nO;
        document.getElementById("Question4Nodiv").innerHTML = nO;
        document.getElementById("Question4YesLbl").innerHTML = YES;
        document.getElementById("Question4YesDiv").innerHTML = YES;


        document.getElementById("adoptedRequestQuestion5").innerHTML = 'Do you have the ability to take your pets to a veterinary clinic to follow up on annual vaccinations, and emergency cases?';
        document.getElementById("lbladoptedRequestQuestion5Required").innerHTML = 'Please Enter The Answer';
        $("#Question5YesLbl").val(YES);
        $("#Question5Nolbl").val(nO);
        document.getElementById("Question5Nolbl").innerHTML = nO;
        document.getElementById("Question5Nodiv").innerHTML = nO;
        document.getElementById("Question5YesLbl").innerHTML = YES;
        document.getElementById("Question5YesDiv").innerHTML = YES;

        $("#adoptedRequestQuestion6").attr('placeholder', 'Which veterinary clinic do you usually/intend to go to? (Please provide the name of the clinic.)');
        document.getElementById("lbladoptedRequestQuestion6Required").innerHTML = 'Please Enter The Answer';

        document.getElementById("adoptedRequestQuestion7").innerHTML = 'Do you have any experience in introducing a cat/dog to a new home, other pets, and family members?';
        document.getElementById("lbladoptedRequestQuestion7Required").innerHTML = 'Please Enter The Answer';
        $("#Question7YesLbl").val(YES);
        $("#Question7Nolbl").val(nO);
        document.getElementById("Question7Nolbl").innerHTML = nO;
        document.getElementById("Question7Nodiv").innerHTML = nO;
        document.getElementById("Question7YesLbl").innerHTML = YES;
        document.getElementById("Question7YesDiv").innerHTML = YES;

        $("#adoptedRequestQuestion8").attr('placeholder', 'Where will your adopted pet spend most of its time?');
        document.getElementById("lbladoptedRequestQuestion8Required").innerHTML = 'Please Enter The Answer';

        document.getElementById("adoptedRequestQuestion9").innerHTML = 'Are you financially capable of taking your pet to a reputable veterinary clinic for annual vaccinations, regular checkups, and emergencies?';
        document.getElementById("lbladoptedRequestQuestion9Required").innerHTML = 'Please Enter The Answer';
        $("#Question9YesLbl").val(YES);
        $("#Question9Nolbl").val(nO);
        document.getElementById("Question9Nolbl").innerHTML = nO;
        document.getElementById("Question9Nodiv").innerHTML = nO;
        document.getElementById("Question9YesLbl").innerHTML = YES;
        document.getElementById("Question9YesDiv").innerHTML = YES;

        $("#adoptedRequestQuestion10").attr('placeholder', 'How often should it be fed and given water?');
        document.getElementById("lbladoptedRequestQuestion10Required").innerHTML = 'Please Enter The Answer';

        $("#adoptedRequestQuestion11").attr('placeholder', 'What supplies do you think your adopted pet should be provided?');
        document.getElementById("lbladoptedRequestQuestion11Required").innerHTML = 'Please Enter The Answer';

        $("#adoptedRequestQuestion12").attr('placeholder', 'What do you know about spaying/neutering? What do you think of the practice?');
        document.getElementById("lbladoptedRequestQuestion12Required").innerHTML = 'Please Enter The Answer';

        document.getElementById("adoptedRequestQuestion13").innerHTML = 'Adoption fees Vary depending on the type of animal and where medical Services were provided. The fees cover some of the costs of spaying/neutering and/or vaccinations. Are you willing to pay adoption fees?';
          //  'are 300 SR for cats and 300-800 SR for dogs. The fees will cover some of the cost of spaying/neutering, and/or complete vaccinations that were given to the pet. Are you willing to pay adoption fees?';
        document.getElementById("lbladoptedRequestQuestion13Required").innerHTML = 'Please Enter The Answer';
        $("#Question13YesLbl").val(YES);
        $("#Question13Nolbl").val(nO);
        document.getElementById("Question13Nolbl").innerHTML = nO;
        document.getElementById("Question13Nodiv").innerHTML = nO;
        document.getElementById("Question13YesLbl").innerHTML = YES;
        document.getElementById("Question13YesDiv").innerHTML = YES;

        document.getElementById("adoptedRequestQuestion21").innerHTML = 'Type of accommodation';
        document.getElementById("lbladoptedRequestQuestion21Required").innerHTML = 'Please Enter The Answer';
        $("#Question21VillaLbl").val('Villa');
        $("#Question21Storeylbl").val('Story');
        $("#Question21ApartmentLbl").val('Apartment');
        $("#Question21Otherlbl").val('Other');
        document.getElementById("Question21VillaLbl").innerHTML = 'Villa';
        document.getElementById("Question21VillaDiv").innerHTML = 'Villa';
        document.getElementById("Question21Storeylbl").innerHTML = 'Story';
        document.getElementById("Question21Storeydiv").innerHTML = 'Story';
        document.getElementById("Question21ApartmentLbl").innerHTML = 'Apartment';
        document.getElementById("Question21ApartmentDiv").innerHTML = 'Apartment';
        document.getElementById("Question21Otherlbl").innerHTML = 'Other';
        document.getElementById("Question21Otherdiv").innerHTML = 'Other';

        document.getElementById("adoptedRequestQuestion22").innerHTML = 'Are all your family members/roommates okay with you adopting a pet?';
        document.getElementById("lbladoptedRequestQuestion22Required").innerHTML = 'Please Enter The Answer';
        $("#Question22YesLbl").val(YES);
        $("#Question22Nolbl").val(nO);
        document.getElementById("Question22Nolbl").innerHTML = nO;
        document.getElementById("Question22Nodiv").innerHTML = nO;
        document.getElementById("Question22YesLbl").innerHTML = YES;
        document.getElementById("Question22YesDiv").innerHTML = YES;


        document.getElementById("adoptedRequestQuestion23").innerHTML = 'Are there any children in your family? If the answer is “yes” please specify their ages.';
        document.getElementById("lbladoptedRequestQuestion23Required").innerHTML = 'Please Enter The Answer';
        $("#Question23YesLbl").val(YES);
        $("#Question23Nolbl").val(nO);
        document.getElementById("Question23Nolbl").innerHTML = nO;
        document.getElementById("Question23Nodiv").innerHTML = nO;
        document.getElementById("Question23YesLbl").innerHTML = YES;
        document.getElementById("Question23YesDiv").innerHTML = YES;
        $("#Question23Text").attr('placeholder', 'Ages');

        document.getElementById("adoptedRequestQuestion24").innerHTML = 'Are you certain nobody in your household is allergic to cats, dogs, or the pet you are planning to adopt?';
        document.getElementById("lbladoptedRequestQuestion24Required").innerHTML = 'Please Enter The Answer';
        $("#Question24YesLbl").val(YES);
        $("#Question24Nolbl").val(nO);
        document.getElementById("Question24Nolbl").innerHTML = nO;
        document.getElementById("Question24Nodiv").innerHTML = nO;
        document.getElementById("Question24YesLbl").innerHTML = YES;
        document.getElementById("Question24YesDiv").innerHTML = YES;

        $("#adoptedRequestQuestion25").attr('placeholder', 'On average, how many hours a day will you spend with your pet? How many hours a day will the pet be left unattended?');
        document.getElementById("lbladoptedRequestQuestion25Required").innerHTML = 'Please Enter The Answer';


        document.getElementById("adoptedRequestQuestion26").innerHTML = 'Do you travel/move much?';
        document.getElementById("lbladoptedRequestQuestion26Required").innerHTML = 'Please Enter The Answer';
        $("#Question26YesLbl").val(YES);
        $("#Question26Nolbl").val(nO);
        document.getElementById("Question26Nolbl").innerHTML = nO;
        document.getElementById("Question26Nodiv").innerHTML = nO;
        document.getElementById("Question26YesLbl").innerHTML = YES;
        document.getElementById("Question26YesDiv").innerHTML = YES;

        $("#adoptedRequestQuestion27").attr('placeholder', 'In the event of travelling, where do you intend to keep the pet?');
        document.getElementById("lbladoptedRequestQuestion27Required").innerHTML = 'Please Enter The Answer';


        //adoptedRequesteDetails

        document.getElementById("adoptedRequesteDetailsFullName").innerHTML = ' <i class="ionicons ion-document-text"></i> Name';
        document.getElementById("adoptedRequesteDetailsMobile").innerHTML = ' <i class="ionicons ion-document-text"></i> Mobile ';
        document.getElementById("adoptedRequesteDetailsAge").innerHTML = ' <i class="ionicons ion-document-text"></i> Age';
        document.getElementById("adoptedRequesteDetailsEmail").innerHTML = ' <i class="ionicons ion-document-text"></i> Email ';
        document.getElementById("adoptedRequesteDetailsAddress").innerHTML = ' <i class="ionicons ion-document-text"></i> City/Address';
        document.getElementById("adoptedRequesteDetailsSocialStatus").innerHTML = ' <i class="ionicons ion-document-text"></i> Social Status';
        document.getElementById("adoptedRequesteDetailsQuestion1").innerHTML = ' <i class="ionicons ion-document-text"></i> Why do you want to adopt a pet?';
        document.getElementById("adoptedRequesteDetailsQuestion2").innerHTML = ' <i class="ionicons ion-document-text"></i> Have you ever had pets before? If so, what kind of pets were they? How long have you had them and where are they now? ';
        document.getElementById("adoptedRequesteDetailsQuestion3").innerHTML = ' <i class="ionicons ion-document-text"></i> If you have any cats/dogs/other animals, Are all of them are spayed/neutered?';
        document.getElementById("adoptedRequesteDetailsQuestion4").innerHTML = ' <i class="ionicons ion-document-text"></i> Do you have any other pets?';
        document.getElementById("adoptedRequesteDetailsQuestion5").innerHTML = ' <i class="ionicons ion-document-text"></i> Do you have the ability to take your pets to a veterinary clinic to follow up on annual vaccinations, and emergency cases?';
        document.getElementById("adoptedRequesteDetailsQuestion6").innerHTML = ' <i class="ionicons ion-document-text"></i> Which veterinary clinic do you usually/intend to go to? (Please provide the name of the clinic.';
        document.getElementById("adoptedRequesteDetailsQuestion7").innerHTML = ' <i class="ionicons ion-document-text"></i> Do you have any experience in introducing a cat/dog to a new home, other pets, and family members? ';
        document.getElementById("adoptedRequesteDetailsQuestion8").innerHTML = ' <i class="ionicons ion-document-text"></i> Where will your adopted pet spend most of its time?';
        document.getElementById("adoptedRequesteDetailsQuestion9").innerHTML = ' <i class="ionicons ion-document-text"></i> Are you financially capable of taking your pet to a reputable veterinary clinic for annual vaccinations, regular checkups, and emergencies?';
        document.getElementById("adoptedRequesteDetailsQuestion10").innerHTML = ' <i class="ionicons ion-document-text"></i> How often should it be fed and given water?';
        document.getElementById("adoptedRequesteDetailsQuestion11").innerHTML = ' <i class="ionicons ion-document-text"></i> What supplies do you think your adopted pet should be provided?';
        document.getElementById("adoptedRequesteDetailsQuestion12").innerHTML = ' <i class="ionicons ion-document-text"></i> What do you know about spaying/neutering? What do you think of the practice?';
        document.getElementById("adoptedRequesteDetailsQuestion13").innerHTML = ' <i class="ionicons ion-document-text"></i> Adoption fees Vary depending on the type of animal and where medical Services were provided. The fees cover some of the costs of spaying/neutering and/or vaccinations. Are you willing to pay adoption fees?';
        document.getElementById("adoptedRequesteDetailsQuestion21").innerHTML = ' <i class="ionicons ion-document-text"></i> Type of accommodation';
        document.getElementById("adoptedRequesteDetailsQuestion22").innerHTML = ' <i class="ionicons ion-document-text"></i> Are all your family members/roommates okay with you adopting a pet?';
        document.getElementById("adoptedRequesteDetailsQuestion23").innerHTML = ' <i class="ionicons ion-document-text"></i> Are there any children in your family? If the answer is “yes” please specify their ages.';
        document.getElementById("adoptedRequesteDetailsQuestion24").innerHTML = ' <i class="ionicons ion-document-text"></i> Are you certain nobody in your household is allergic to cats, dogs, or the pet you are planning to adopt?';
        document.getElementById("adoptedRequesteDetailsQuestion25").innerHTML = ' <i class="ionicons ion-document-text"></i> On average, how many hours a day will you spend with your pet? How many hours a day will the pet be left unattended?';
        document.getElementById("adoptedRequesteDetailsQuestion26").innerHTML = ' <i class="ionicons ion-document-text"></i> Do you travel/move much?';
        document.getElementById("adoptedRequesteDetailsQuestion27").innerHTML = ' <i class="ionicons ion-document-text"></i> In the event of travelling, where do you intend to keep the pet?';


        //Add animal
        DeleteAnimalConfirmMsg = "Do You Want To Delete The Pet?";
        $("#txtAnimalName").attr('placeholder', 'Pet Name');
        $('#txtAnimalAge').attr('placeholder', 'Pet Age');
        document.getElementById("lblAgeNumberOnly").innerHTML = 'Age Must Be Digits Only';
        document.getElementById("lblAgeAllowedLength").innerHTML = 'Age Cant Not  Be More Than 3 Digits ';
        $("#txtAnimalVaccine").attr('placeholder', 'Vaccinations');
        $("#divAnimalAddress").attr('placeholder', 'Address');
        $("#ddlAnimalNotes").attr('placeholder', 'Notes');
        document.getElementById("lblAnimalAddressRequired").innerHTML = 'Address Is Required';
        document.getElementById("UploadAnimalPhoto").innerHTML = '<i class="ionicons ion-image"></i>Pet Picture';
        document.getElementById("UploadAnimalVedio").innerHTML = '<i class="ionicons ion-image"></i>Upload Video';
        document.getElementById("RecordAnimalVedio").innerHTML = '<i class="ionicons ion-image"></i>Record Video';
        document.getElementById("UploadGiveUpAnimalVedio").innerHTML = '<i class="ionicons ion-image"></i>Upload Video';
        document.getElementById("RecordGiveUpAnimalVedio").innerHTML = '<i class="ionicons ion-image"></i>Record Video';
        document.getElementById("BtnAddAnimalToAdaption").innerHTML = '<i class="ionicons ion-ios-download-outline"></i>Add A Pet';
        AddAnimalTitleEmergancy = "Add An Urgent Case";
        AddAnimalTitleAboption = "Add A Pet For Adoption";
        AddAnimalTitleLost = "Add A Lost Pet";
        EditAnimal = "Edit Pet";
        EditBtnTxt = "Edit";
        AddAnimalReportBtn = '<i class="ionicons ion-ios-download-outline"></i>Report';
        AddAnimalsubmitBtn = '<i class="ionicons ion-ios-download-outline"></i>Add A Pet';
        document.getElementById("lblAnimalTypeRequired").innerHTML = 'Pet Is Required';
        document.getElementById("lblAnimalCityRequired").innerHTML = 'City Is Required';
        document.getElementById("lblAnimalGenderRequired").innerHTML = 'Gender Is Required';
        //lost animal 
        document.getElementById("lostAnimalsPageTitle").innerHTML = 'Lost Pets';
        document.getElementById("NoLostAnimalFound").innerHTML = 'No Lost Pet Found';
        //emergencies
        document.getElementById("needCareAnimalsPageTitle").innerHTML = 'Urgent Cases';
        document.getElementById("NoneedCareFound").innerHTML = 'No Urgent Cases';

        //success stories        
        document.getElementById("successStoryPageTitle").innerHTML = 'Success Stories';
        document.getElementById("NoSuccessStory").innerHTML = 'No Success Stories Found';

        //Add success stories        
        document.getElementById("AddsuccessStoryPageTitle").innerHTML = 'Add Success Story';
        document.getElementById("Btn_uploadSucessImg").innerHTML = '<i class="ionicons ion-image"></i> Story Picture ';
        $("#txt_SucessTitle").attr('placeholder', 'Story Title');
        document.getElementById("lbladdSuccessStoryTitleRequired").innerHTML = 'Story Title Is Required';
        $("#txt_SucessDetails").attr('placeholder', 'Story Details');
        document.getElementById("lbladdSuccessStoryDetailsRequired").innerHTML = 'Story Details Is Required';
        document.getElementById("btn_AddSucessStory").innerHTML = '<i class="ionicons ion-ios-download-outline"></i>Add';
        document.getElementById("UploadStoryVedio").innerHTML = '<i class="ionicons ion-image"></i>Upload Video';
        document.getElementById("RecordStoryVedio").innerHTML = '<i class="ionicons ion-image"></i>Record Video';

        //change password
        $("#txtChangeOldPassword").attr('placeholder', 'Enter Old Password');
        document.getElementById("lblChangeOldPasswordRequired").innerHTML = 'Old Password Is Required';
        $("#txtChangeNewPassword").attr('placeholder', 'Enter New Password');
        document.getElementById("lblChangeNewPasswordRequired").innerHTML = 'New Password Is Required';
        document.getElementById("lblChangeNewPasswordAllowedLength").innerHTML = 'New Password Must Not Be Less Than 6 Characters ';
        $("#txtChangeConfirmNewPassword").attr('placeholder', 'ReEnter New Password');
        document.getElementById("lblChangeConfirmNewPasswordRequired").innerHTML = 'New Password  Must Be Confirmed';
        document.getElementById("lblChangeConfirmNewPasswordCompare").innerHTML = 'Passwords Do Not Match';
        document.getElementById("btnChangePassword").innerHTML = '<i class="ionicons ion-person-add"></i>Change';
        document.getElementById("btnPasswordBackToHome").innerHTML = '<i class="ionicons ion-android-arrow-forward"></i>Back';

        //success stories detailes        
        document.getElementById("storyDetailsPageTitle").innerHTML = 'Story Details';
        document.getElementById("NoCommentsForStory").innerHTML = 'No Comments';

        //add Comment For Story
        document.getElementById("CommentsStoryPageTitle").innerHTML = '<i class="ionicons ion-edit"></i> Comment';
        $("#txtCommentStory").attr('placeholder', 'Write Your Comment ...');
        document.getElementById("Btn_AddCommectStory").innerHTML = '<i class="ionicons ion-chatboxes"></i>Comment';
        document.getElementById("Btn_CloseCommectStory").innerHTML = '<i class="ionicons ion-chatboxes"></i>Close';
        //notificationPage
        document.getElementById("notificationPageTitle").innerHTML = 'Notifications';
        document.getElementById("NoNotifications").innerHTML = 'No Notifications Found';

        //resetPasswordPage
        $("#txtResetCode").attr('placeholder', 'Enter Code');
        $("#txtResetPassword").attr('placeholder', 'Enter New Password');
        $("#txtResetConfirmPassword").attr('placeholder', 'Confirm New Password');
        document.getElementById("lblresetCodeRequired").innerHTML = 'Code Is Required';
        document.getElementById("lblresetPasswordRequired").innerHTML = 'Password Is Required';
        document.getElementById("lblresetPasswordAllowedLength").innerHTML = 'Password Must Be More Than 6 Character';
        document.getElementById("lblresetConfirmPasswordRequired").innerHTML = 'Confirm Password Is Required';
        document.getElementById("lblresetConfirmPasswordCompare").innerHTML = 'Passwords Don`t Match';
        document.getElementById("btnResetPassword").innerHTML = 'Reset Password';
        document.getElementById("btnResetPasswordBackToHome").innerHTML = 'Back';
        //AdoptRequestPopup
        document.getElementById("AdoptRequestPopupTitle").innerHTML = 'Adoption Requests';
        document.getElementById("NoAdoptRequestpopUp").innerHTML = 'No Rquests Found';
        document.getElementById("btncloseAdoptRequestPopup").innerHTML = 'Close';

    }
}
function fillGenderDDL(select, placeHolderTxt) {
    select.innerHTML = '';
    //placeHolderTxt.innerHTML = '';
    //placeHolderTxt.innerHTML = DefultGender;


    var data = [
    {
        "id": "0",
        "name": MaleGender
    },
    {
        "id": "1",
        "name": FemaleGender
    }
    ]

    select.append('<option value="" selected disabled>' + DefultGender + '</option>');
    $.each(data, function (i, option) {
        select.append($('<option/>').attr("value", option.id).text(option.name));
    });

}
function ClearAddAnimalData() {
    document.getElementById('imgContainerDiv').innerHTML = "";
    localStorage.setItem('UplodedPhoto', "");
    document.querySelector("#videoPlayer").src = "";

    $("#vedioPage").css('display', 'none');
    animalAdopImages = [];
    $("#txtAnimalId").val('')
    $('#txtAnimalAge').val('');
    $("#txtAnimalName").val('');
    $("#txtAnimalVaccine").val('');
    $("#divAnimalAddress").val('');
    $("#ddlAnimalNotes").val('');
    $$(".smart-select .item-after").html("");
    $$("#selectedLeaveAnimalType").html(AnimalCategory);
    $$("#selectedLeaveAnimalCity").html(City);
    $$("#seletedLeaveAnimalGender").html(DefultGender);
    $$("#seletedLeaveAnimalLleaveFor").html(GiveUpForTxt);
    $$("#selectedAnimalType").html(AnimalCategory);
    $$("#selectedAnimalCity").html(City);
    $$("#selectedAnimalGender").html(DefultGender);
    $('input[type=radio][name=my-radio1_UploadAnimalVedio]').each(function () {
        $(this).prop('checked', false);
        //this.checked = false;
    });
}
function ClearAdoptionRequestData() {
    $('#adoptedRequestFullName').val('');
    $('#adoptedRequestMobile').val('');
    $('#adoptedRequestAge').val('');
    $('#adoptedRequestEmail').val('');
    $('#adoptedRequestAddress').val('');
    $('#adoptedRequestSocialStatus').val('');
    $('#adoptedRequestQuestion1').val('');
    $('#adoptedRequestQuestion2').val('');
    $('#adoptedRequestQuestion6').val('');
    $('#adoptedRequestQuestion8').val('');
    $('#adoptedRequestQuestion10').val('');
    $('#adoptedRequestQuestion11').val('');
    $('#adoptedRequestQuestion12').val('');
    $('#Question23Text').val('');
    $('#adoptedRequestQuestion25').val('');
    $('#adoptedRequestQuestion27').val('');
    $("#lbladoptedRequestFullNameRequired").css('display', 'none');
    $("#lbladoptedRequestAddressRequired").css('display', 'none');
    $("#lbladoptedRequestSocialStatusRequired").css('display', 'none');
    $("#lbladoptedRequestQuestion1Required").css('display', 'none');
    $("#lbladoptedRequestQuestion2Required").css('display', 'none');
    $("#lbladoptedRequestQuestion6Required").css('display', 'none');
    $("#lbladoptedRequestQuestion8Required").css('display', 'none');
    $("#lbladoptedRequestQuestion10Required").css('display', 'none');
    $("#lbladoptedRequestQuestion11Required").css('display', 'none');
    $("#lbladoptedRequestQuestion12Required").css('display', 'none');
    $("#lbladoptedRequestQuestion25Required").css('display', 'none');
    $("#lbladoptedRequestQuestion27Required").css('display', 'none');

    $("#lbladoptedRequestEmailRequired").css('display', 'none');
    $("#lbladoptedRequestEmailAllowedLength").css('display', 'none');
    $("#lbladoptedRequestAgeRequired").css('display', 'none');
    $("#lbladoptedRequestAgeAllowedLength").css('display', 'none');
    $("#lbladoptedRequestMobileRequired").css('display', 'none');
    $("#lbladoptedRequestMobileAllowedLength").css('display', 'none');

}
function ClearLeaveRequest() {
    animalAdopImages = [];
    $('#txtLeaveAnimalName').val('');
    $('#txtLeaveAnimalAge').val('');
    $('#txtAnimalLeaveVaccine').val('');
    $('#txtAnimalLeaveReason').val('');
    $('#divLeaveAnimalAddress').val('');
    $('#TxtGiveUpLeave').val('');
    $('#txtLeaveAnimalDescription').val('');

    $$(".smart-select .item-after").html("");
    $$("#selectedLeaveAnimalType").html(AnimalCategory);
    $$("#selectedLeaveAnimalCity").html(City);
    $$("#seletedLeaveAnimalGender").html(DefultGender);
    $$("#seletedLeaveAnimalLleaveFor").html(GiveUpForTxt);
    $$("#selectedAnimalType").html(AnimalCategory);
    $$("#selectedAnimalCity").html(City);
    $$("#selectedAnimalGender").html(DefultGender);
    document.getElementById('imgLeaveAnimalContainerDiv').innerHTML = "";
    $("#LeaveAnimalNotes").val('');
    document.querySelector("#videoGiveUpPlayer").src = "";
    $("#vedioGiveUpPage").css('display', 'none');

    $("#lblleaveForRequired").css('display', 'none');
    $("#lblLeaveAnimalLocationRequired").css('display', 'none');
    $("#lblLeaveAnimalAgeRequired").css('display', 'none');
    $("#lblLeaveAnimalAgeNumberOnly").css('display', 'none');
    $("#lblLeaveAnimalNameRequired").css('display', 'none');
    $("#lblLeaveAnimalGenderRequired").css('display', 'none');
    $("#lblLeaveAnimalCityRequired").css('display', 'none');
    $("#lblLeaveAnimalTypeRequired").css('display', 'none');

}
function ClearRegister() {
    $("#txtName").val('');
    $("#txtUserName").val('');
    $("#txtMobile").val('');
    $("#txtUserAge").val('');
    $("#txtEmail").val('');
    $("#txtPassword").val('');
    $("#txtConfirmPassword").val('');
    $("#CitiesDll").val();
    $$(".smart-select .item-after").html("");
    $$("#SelectedCity").html(City);
}

function getImage() {
    navigator.camera.getPicture(uploadPhoto, function (message) {
        myApp.alert(ChoseImage, warning, function () { });

    }, {
        quality: 50,
        destinationType: navigator.camera.DestinationType.DATA_URL,
        sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: false,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 300,
        targetHeight: 300,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        correctOrientation: true
    });
}
function uploadPhoto(imageURI) {
    var imageName = imageURI.substr(imageURI.lastIndexOf('/') + 1).split('?')[0];

    //var extnention = (imageName.substring(imageName.lastIndexOf(".") + 1));
    //var filename = imageName.split('.')[0] + Date.now() + '.' + extnention;

    var options = new FileUploadOptions();
    options.fileKey = "file";
    options.fileName = imageName;
    options.mimeType = "image/jpeg";
    options.chunkedMode = false;

    var params = new Object();
    params.value1 = "test";
    params.value2 = "param";

    options.params = params;

    // var ft = new FileTransfer();

    var params = {
        'ImageFilename': 'img.jpg',
        'ImageBase64': imageURI
    };

    CallService('', "POST", "/api/Users/UploadImage", params, function (imageName) {
        if (imageName != null) {
            var totalPhotoUrl = hostUrl + 'uploads/' + imageName;
            var imgContainer = document.getElementById('imgContainerDiv');
            var ImgCount = $$('#imgContainerDiv img.uploadImg').length;
            if (ImgCount < 3) {
                localStorage.setItem('UplodedPhoto', totalPhotoUrl);
                animalAdopImages.push(totalPhotoUrl);
                var ImgUp = document.createElement("img");
                ImgUp.className = 'uploadImg';
                ImgUp.src = totalPhotoUrl;
                // imgContainer.appendChild(ImgUp);

                var imgdiv = document.createElement("div");
                imgdiv.className = 'imgContent';
                var imgdivoverlay = document.createElement("div");
                imgdivoverlay.className = 'delOverlay';
                var imgIcon = document.createElement("i");
                imgIcon.className = 'ionicons ion-close-round';
                imgdiv.setAttribute('onclick', 'RemoveImage(this);');
                imgdivoverlay.appendChild(imgIcon);
                imgdiv.appendChild(imgdivoverlay);
                imgdiv.appendChild(ImgUp);
                imgContainer.appendChild(imgdiv);

            } else {
                //  myApp.alert( ');
                myApp.alert(LargNoOfImage, warning, function () {
                });
            }

        }
    });
    //ft.upload(imageURI, encodeURI(hostUrl + "UploadPhoto.asmx/UploadFile"), function (r) {
    //    var totalPhotoUrl = hostUrl + 'uploads/' + imageName;
    //    var imgContainer = document.getElementById('imgContainerDiv');
    //    var ImgCount = $$('#imgContainerDiv img.uploadImg').length;
    //    if (ImgCount < 3) {
    //        localStorage.setItem('UplodedPhoto', totalPhotoUrl);
    //        animalAdopImages.push(totalPhotoUrl);
    //        var ImgUp = document.createElement("img");
    //        ImgUp.className = 'uploadImg';
    //        ImgUp.src = totalPhotoUrl;
    //        imgContainer.appendChild(ImgUp);
    //    } else {
    //        //  myApp.alert( ');
    //        myApp.alert(LargNoOfImage, warning, function () { });
    //    }
    //},
    //function (error) {
    //    console.log("An error has occurred: Code = " + error.code);
    //}, options);
}
function getProfileImage() {
    navigator.camera.getPicture(uploadProfilePhoto, function (message) {
        myApp.alert(ChoseImage, warning, function () { });

    }, {
        quality: 50,
        destinationType: navigator.camera.DestinationType.DATA_URL,
        sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: false,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 300,
        targetHeight: 300,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        correctOrientation: true
    });
}
function uploadProfilePhoto(imageUrl) {
    var imageName = imageUrl.substr(imageUrl.lastIndexOf('/') + 1).split('?')[0];
    //var extnention = (imageName.substring(imageName.lastIndexOf(".") + 1));
    //var filename = imageName.split('.')[0] + Date.now() + '.' + extnention;

    var options = new FileUploadOptions();
    options.fileKey = "file";
    options.fileName = imageName;
    options.mimeType = "image/jpeg";
    options.chunkedMode = false;

    var params = new Object();
    params.value1 = "test";
    params.value2 = "param";

    options.params = params;

    var params = {
        'ImageFilename': 'img.jpg',
        'ImageBase64': imageUrl
    };

    CallService('', "POST", "/api/Users/UploadImage", params, function (imageName) {
        if (imageName != null) {
            var totalPhotoUrl = hostUrl + 'uploads/' + imageName;

            var profileImg = document.getElementById("imgUserEdit");
            profileImg.src = totalPhotoUrl;
        }
    });

    //var ft = new FileTransfer();
    //ft.upload(imageUrl, encodeURI(hostUrl + "UploadPhoto.asmx/UploadFile"), function (r) {
    //    var totalPhotoUrl = hostUrl + 'uploads/' + imageName;
    //    var profileImg = document.getElementById("imgUserEdit");
    //    profileImg.src = totalPhotoUrl;
    //},
    //function (error) {
    //    console.log("An error has occurred: Code = " + error.code);
    //}, options);
}
function getSucessStoryImage() {
    navigator.camera.getPicture(uploadSucessStoryPhoto, function (message) {
        myApp.alert(ChoseImage, warning, function () { });
    }, {
        quality: 50,
        destinationType: navigator.camera.DestinationType.DATA_URL,
        sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: false,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 300,
        targetHeight: 300,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        correctOrientation: true
    });
}
function uploadSucessStoryPhoto(imageUrl) {
    var imageName = imageUrl.substr(imageUrl.lastIndexOf('/') + 1).split('?')[0];

    //var extnention = (imageName.substring(imageName.lastIndexOf(".") + 1));
    //var filename = imageName.split('.')[0] + Date.now() + '.' + extnention;

    var options = new FileUploadOptions();
    options.fileKey = "file";
    options.fileName = imageName;//imageName;
    options.mimeType = "image/jpeg";
    options.chunkedMode = false;

    var params = new Object();
    params.value1 = "test";
    params.value2 = "param";

    options.params = params;
    var params = {
        'ImageFilename': 'img.jpg',
        'ImageBase64': imageUrl
    };
    CallService('', "POST", "/api/Users/UploadImage", params, function (imageName) {
        if (imageName != null) {
            var totalPhotoUrl = hostUrl + 'uploads/' + imageName;
            var profileImg = document.getElementById("SucessImgUploaded");
            profileImg.src = totalPhotoUrl;
        }
    });
    //var ft = new FileTransfer();
    //ft.upload(imageUrl, encodeURI(hostUrl + "UploadPhoto.asmx/UploadFile"), function (r) {
    //    var totalPhotoUrl = hostUrl + 'uploads/' + imageName;
    //    var profileImg = document.getElementById("SucessImgUploaded");
    //    profileImg.src = totalPhotoUrl;
    //},
    //function (error) {
    //    console.log("An error has occurred: Code = " + error.code);
    //}, options);
}
function getLeaveAnimalImage() {
    navigator.camera.getPicture(uploadLeaveAnimalPhoto, function (message) {
        myApp.alert(ChoseImage, warning, function () { });
    }, {
        quality: 50,
        destinationType: navigator.camera.DestinationType.DATA_URL,
        sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: false,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 300,
        targetHeight: 300,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        correctOrientation: true
    });
}
function uploadLeaveAnimalPhoto(imageURI) {
    var imageName = imageURI.substr(imageURI.lastIndexOf('/') + 1).split('?')[0];
    //var extnention = (imageName.substring(imageName.lastIndexOf(".") + 1));
    //var filename = imageName.split('.')[0] + Date.now() + '.' + extnention;

    var options = new FileUploadOptions();
    options.fileKey = "file";
    options.fileName = imageName;
    options.mimeType = "image/jpeg";
    options.chunkedMode = false;

    var params = new Object();
    params.value1 = "test";
    params.value2 = "param";

    options.params = params;

    var params = {
        'ImageFilename': 'img.jpg',
        'ImageBase64': imageURI
    };
    CallService('', "POST", "/api/Users/UploadImage", params, function (imageName) {
        if (imageName != null) {
            var totalPhotoUrl = hostUrl + 'uploads/' + imageName;
            var ImgCount = $$('#imgLeaveAnimalContainerDiv img.uploadImg').length;
            if (ImgCount < 3) {
                localStorage.setItem('UplodedPhoto', totalPhotoUrl);
                animalAdopImages.push(totalPhotoUrl);
                var imgLeaveContainer = document.getElementById('imgLeaveAnimalContainerDiv');
                var ImgUp = document.createElement("img");
                ImgUp.src = totalPhotoUrl;
                ImgUp.className = 'uploadImg';
                // imgLeaveContainer.appendChild(ImgUp);


                var imgdiv = document.createElement("div");
                imgdiv.className = 'imgContent';
                var imgdivoverlay = document.createElement("div");
                imgdivoverlay.className = 'delOverlay';
                var imgIcon = document.createElement("i");
                imgIcon.className = 'ionicons ion-close-round';
                imgdiv.setAttribute('onclick', 'RemoveImage(this);');
                imgdivoverlay.appendChild(imgIcon);
                imgdiv.appendChild(imgdivoverlay);
                imgdiv.appendChild(ImgUp);
                imgLeaveContainer.appendChild(imgdiv);
            } else {
                myApp.alert(LargNoOfImage, warning, function () { });

            }
        }
    });
    //var ft = new FileTransfer();
    //ft.upload(imageURI, encodeURI(hostUrl + "UploadPhoto.asmx/UploadFile"), function (r) {
    //    var totalPhotoUrl = hostUrl + 'uploads/' + imageName;
    //    var ImgCount = $$('#imgLeaveAnimalContainerDiv img.uploadImg').length;
    //    if (ImgCount < 3) {
    //        localStorage.setItem('UplodedPhoto', totalPhotoUrl);
    //        animalAdopImages.push(totalPhotoUrl);
    //        var imgLeaveContainer = document.getElementById('imgLeaveAnimalContainerDiv');
    //        var ImgUp = document.createElement("img");
    //        ImgUp.src = totalPhotoUrl;
    //        ImgUp.className = 'uploadImg';
    //        imgLeaveContainer.appendChild(ImgUp);
    //    } else {
    //        myApp.alert(LargNoOfImage, warning, function () { });
    //    }
    //},
    //function (error) {
    //    console.log("An error has occurred: Code = " + error.code);
    //}, options);
}
function loadUserImage(imgElementId, userImage) {
    // User Image
    if (localStorage.getItem('userLoggedIn')) {
        var user = JSON.parse(localStorage.getItem('userLoggedIn'));
        var totalPhotoUrl = user.PhotoUrl;

        if (userImage != '' && userImage != null) {
            totalPhotoUrl = userImage;
        } else {
            totalPhotoUrl = "img/NoImg.jpg";
        }

        var imgUserImg = document.getElementById(imgElementId);
        imgUserImg.src = totalPhotoUrl;
    }
}
function onSuccess(result) {
    console.log("Success:" + result);
}
function onError(result) {
    console.log("Error:" + result);
}
function CallService(pageName, callType, methodName, dataVariables, callback) {

    //if (pageName != null && methodName != "/api/Users/UploadImage") {
    //    HideLoader();
    //}
    if (pageName != null && pageName != "" && methodName != "/api/Animal/GetAnimls" && methodName != "/api/Animal/AnimalsByLoggedInUser"
        && methodName != "/api/AdoptionRequest/AdoptionRequestForUser" && methodName != "/api/Volunteer/VolunteerUserRequest"
        && methodName != "/api/AnimalShelter/GetShelter" && methodName != "/api/Animal/AnimalsByShelter" && methodName != "/api/Users/GetFilterdUsers"
        && methodName != "/api/Animal/AnimalsByUser" && methodName != "/api/NotificationSetting/GetUserNotifications") {
        ShowLoader(pageName);
    }
    var token = localStorage.getItem('appToken');




    $.ajax({
        type: callType,
        url: serviceURL + methodName,
        headers: {
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "authorization": "bearer " + token
        },
        data: dataVariables,
        async: true,
        crossDomain: true
    }).done(function (result) {
        var res = result;
        callback(res);
        HideLoader();

    }).fail(function (error, textStatus) {

        console.log("Error in (" + methodName + ") , Error text is : [" + error.responseText + "] , Error json is : [" + error.responseJSON + "] .");
        HideLoader();
        callback(error);

        //myApp.alert(ror.responseText, 'خطأ', function () { });
        //if (error.status === 401) { 
        //    myApp.alert('مدة صلاحية رمز التحقق الخاص بك قد انتهت من فضلك قم بتسجيل الدخول مرة أخري .', 'خطأ', function () {
        //        localStorage.removeItem('appToken');
        //        localStorage.removeItem('USName');
        //        localStorage.removeItem('refreshToken');
        //        localStorage.removeItem('userLoggedIn');
        //        localStorage.removeItem('Visitor');
        //        initLoginPage = true;
        //        mainView.router.loadPage({ pageName: 'login' });
        //    });
        //}
        //else {
        //    //if (typeof responseText.error_description != 'undefined') {
        //    //    var error_description = responseText.error_description;
        //    //    myApp.alert(error_description, 'خطأ', function () { });
        //    //}
        //    //if (typeof responseText.message != 'undefined') {
        //    //    var message = responseText.message;
        //    //    myApp.alert(message, 'خطأ', function () { });
        //    //}
        //    //else if (typeof responseText.modelState != 'undefined') {
        //    //    var messages = responseText.modelState[""];
        //    //    var message = "";
        //    //    if (messages.length > 0) {
        //    //        if (messages[0] === 'Incorrect password.') {
        //    //            myApp.alert('كلمة السر القديمة غير صحيحة .', 'خطأ', function () { });
        //    //        }
        //    //        else {
        //    //            for (var m = 0; m < messages.length; m++) {
        //    //                message += "- [" + messages[m] + " ] - ";
        //    //            }
        //    //            myApp.alert(message, 'خطأ', function () { });
        //    //        }
        //    //    }
        //    //    else {
        //    //        callback(null);
        //    //    }
        //    //}
        //    //else {
        //    //    callback(null);
        //    //}
        //}

        //  HideLoader();
    });


}
function CallServiceJson(pageName, callType, methodName, dataVariables, callback) {
    if (pageName != null && pageName != "") {
        ShowLoader(pageName);
    }
    var token = localStorage.getItem('appToken');
    $.ajax({
        type: callType,
        url: serviceURL + methodName,
        headers: {
            "content-type": "application/json",
            "cache-control": "no-cache",
            "authorization": "bearer " + token
        },
        data: dataVariables,
        async: true,
        crossDomain: true
    }).done(function (result) {
        var res = result;
        HideLoader();
        callback(res);
    }).fail(function (error, textStatus) {

        console.log("Error in (" + methodName + ") , Error text is : [" + error.responseText + "] , Error json is : [" + error.responseJSON + "] .");
        HideLoader();
        callback(error);

    });
}
function phonenumber(inputtxt) {
    var phoneno = /^\d{14}$/;
    if (inputtxt.match(/\d/g).length === 10 || inputtxt.match(/\d/g).length < 15) {
        return true;
    }
    else {

        return false;
    }
}
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function GetToken(pageName, CallType, MethodName, userName, password, callback) {
    ShowLoader(pageName);
    $.ajax({
        type: CallType,
        url: serviceURL + MethodName,
        headers: {
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "postman-token": "a7924ea4-7d97-e2f6-5b56-33cbffb586a7"
        },
        data: {
            'userName': userName, 'Password': password, 'grant_type': 'password', 'client_id': clientId, 'client_secret': clientSecret
        },
        async: true,
        crossDomain: true,
        success: function (result) {

        },
        error: function (error) {

        }
    }).done(function (result) {
        var res = result;
        HideLoader();
        callback(res);
    })
        .fail(function (error, textStatus) {
            HideLoader();
            console.log("Error in (" + MethodName + ") , Error text is : [" + error.responseText + "] , Error json is : [" + error.responseJSON + "] .");

            var responseText = JSON.parse(error.responseText);

            if (typeof responseText.error_description != 'undefined') {
                var error_description = responseText.error_description;
                if (error_description === 'The user name or password is incorrect.') {
                    myApp.alert(LoginDataError, Wrong, function () { });

                } else if (error_description === 'You Have No Right To Enter')
                    myApp.alert(cantFindData, Wrong, function () { });
                else if (error_description === 'Email Need To Confirm')
                    myApp.alert(NeedConfirm, Wrong, function () {
                        //mainView.router.loadPage({ pageName: 'activation' });
                    });

            }
            else if (typeof responseText.message != 'undefined') {
                var message = responseText.message;
                callback(null);
            }
            else if (typeof responseText.error != 'undefined') {
                var error = responseText.error;
                callback(null);
            }

        });
}
function RefreshToken(pageName, CallType, MethodName, callback) {
    var token = "'" + localStorage.getItem('refreshToken') + "'";
    $.ajax({
        type: CallType,
        url: serviceURL + MethodName,
        headers: {
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "postman-token": "a7924ea4-7d97-e2f6-5b56-33cbffb586a7"
        },
        data: {
            'refresh_token': token, 'grant_type': 'refresh_token', 'client_id': clientId, 'client_secret': clientSecret
        },
        async: true,
        crossDomain: true,
        success: function (result) {

        },
        error: function (error) {

        }
    }).done(function (result) {
        var res = result;
        HideLoader();
        callback(res);
    })
        .fail(function (error, textStatus) {
            HideLoader();
            console.log("Error in (" + MethodName + ") , Error text is : [" + error.responseText + "] , Error json is : [" + error.responseJSON + "] .");

            var responseT = JSOextN.parse(error.responseText);

            if (typeof responseText.error_description != 'undefined') {
                var error_description = responseText.error_description;
                myApp.alert(error_description, 'خطأ', function () { });
            }

            if (typeof responseText.message != 'undefined') {
                var message = responseText.message;
                myApp.alert(message, 'خطأ', function () { });
            }

            if (typeof responseText.error != 'undefined') {
                var error = responseText.error;
                myApp.alert(error, 'خطأ', function () { });
            }

        });
}
function InitMapSearchBox(map, markers, selectedAddress) {
    if (selectedAddress != '') {
        $('#pac-input').val(selectedAddress);
    }

    var geocoder = new google.maps.Geocoder();
    if (geocoder) {
        geocoder.geocode({
            'address': selectedAddress
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                    map.setCenter(results[0].geometry.location);

                    markers = [];

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        title: results[0].formatted_address,
                        position: results[0].geometry.location
                    }));


                    var lat = results[0].geometry.location.lat();
                    var lang = results[0].geometry.location.lng();


                    localStorage.setItem('FacilityAddressLatitude', lat);
                    localStorage.setItem('FacilityAddressLongtitude', lang);
                    localStorage.setItem('FacilityAddress', $('#pac-input').val());


                    var infoWindow = new google.maps.InfoWindow();

                    for (var i = 0; i < markers.length; i++) {
                        var data = markers[i];
                        var myLatlng = new google.maps.LatLng(data.position.lat(), data.position.lng());
                        var marker = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
                            title: data.title
                        });

                        (function (marker, data) {
                            google.maps.event.addListener(marker, "click", function (e) {
                                infoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.title + "</div>");
                                infoWindow.open(map, marker);
                            });
                        })(marker, data);
                    }

                }
                else {
                    alert("No results found");
                }
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
    }
}
function initMap(markers, fromPage, selectedAddress, callback) {
    var mapDiv = document.getElementById('map');
    var flightPlanCoordinates = [];

    var map = new google.maps.Map(mapDiv, {
        center: new google.maps.LatLng('24.7135517', '46.6752957'),
        zoom: 10,
        mapTypeId: 'terrain'
    });

    if (markers.length > 0) {
        map = new google.maps.Map(mapDiv, {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 10,
            mapTypeId: 'terrain'
        });

        $('#btnSelectAddress').html('رجوع');

        if (fromPage === 'Signup') {
            $('#btnSelectAddress').html('تحديد');
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });

            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                markers = [];

                // For each place, get the icon, name and location.

                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        title: place.name,
                        position: place.geometry.location
                    }));


                    var lat = place.geometry.location.lat();
                    var lang = place.geometry.location.lng();

                    localStorage.setItem('FacilityAddressLatitude', lat);
                    localStorage.setItem('FacilityAddressLongtitude', lang);
                    localStorage.setItem('FacilityAddress', $('#pac-input').val());

                    if (place.geometry.viewport) {
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });
        }

        var infoWindow = new google.maps.InfoWindow();

        for (var i = 0; i < markers.length; i++) {
            var data = markers[i];
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            //flightPlanCoordinates.push(new google.maps.LatLng(data.lat, data.lng));
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title
            });

            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.title + "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker, data);
        }
    }
    else {
        var input = document.getElementById('pac-input');

        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });

        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            markers = [];

            // For each place, get the icon, name and location.

            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    title: place.name,
                    position: place.geometry.location
                }));


                var lat = place.geometry.location.lat();
                var lang = place.geometry.location.lng();


                localStorage.setItem('FacilityAddressLatitude', lat);
                localStorage.setItem('FacilityAddressLongtitude', lang);
                localStorage.setItem('FacilityAddress', $('#pac-input').val());


                if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });

    }
    callback(map);

    //var flightPath = new google.maps.Polyline({
    //    path: flightPlanCoordinates,
    //    geodesic: true,
    //    strokeColor: '#FF0000',
    //    strokeOpacity: 1.0,
    //    strokeWeight: 2
    //});

    //flightPath.setMap(map);
}
function ClearBodyAfterGoogleMap() {
    console.log('clear');
    $('span').each(function () {
        var span = $(this);
        if ($(this).text() === 'BESbewy') {
            $(this).remove();
        }
    });

    $('div').each(function () {
        var div = $(this);
        if ($(this).hasClass('pac-container pac-logo hdpi') && $(this).children().length == 0) {
            $(this).remove();
        }
    });
}
function GetCurrentDateTime(date) {
    var currentdate = new Date();
    if (date == '') {
        currentdate = new Date();
    }
    else {
        currentdate = date;
    }

    var month = parseInt((currentdate.getMonth() + 1));
    var day = parseInt(currentdate.getDate());
    var datetime;

    if (month < 10) {
        if (day < 10) {
            datetime = currentdate.getFullYear() + "-0" + (currentdate.getMonth() + 1) + "-0" + currentdate.getDate();
            //+ " T"+ currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
        }
        else {
            datetime = currentdate.getFullYear() + "-0" + (currentdate.getMonth() + 1) + "-" + currentdate.getDate();
            //+ " T" + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
        }
    }
    else {
        if (day < 10) {
            datetime = currentdate.getFullYear() + "-" + (currentdate.getMonth() + 1) + "-0" + currentdate.getDate();
            //+ " T" + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
        }
        else {
            datetime = currentdate.getFullYear() + "-" + (currentdate.getMonth() + 1) + "-" + currentdate.getDate();
            //+ " T" + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
        }
    }

    return datetime;
}
function ShowDatePicker(birthDatecontrol) {
    //    var doctorBirthDate = document.getElementById('doctorBirthDate');
    doctorBirthDate.onclick = function () {
        var options = {
            date: new Date(),
            mode: 'date'
        };

        function onSuccess(date) {
            BirthDatecontrol.value = GetCurrentDateTime(date);
        }

        function onError(error) { // Android only

        }

        datePicker.show(options, onSuccess, onError);
    }
}
function _calculateAge(birthday) { // birthday is a date
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function calculate_age(birth_month, birth_day, birth_year) {
    today_date = new Date();
    today_year = today_date.getFullYear();
    today_month = today_date.getMonth();
    today_day = today_date.getDate();
    age = today_year - birth_year;

    if (today_month < (birth_month - 1)) {
        age--;
    }
    if (((birth_month - 1) == today_month) && (today_day < birth_day)) {
        age--;
    }
    return age;
}
function ErrorMsg(result) {
    if (typeof result.responseText != 'undefined') {
        var responseText = JSON.parse(result.responseText);

        var errortextMsg = "";
        if (typeof responseText.ModelState != 'undefined') {
            if (typeof responseText.ModelState != 'undefined') {
                for (var i = 0; i < responseText.ModelState[""].length; i++) {

                    errortextMsg += responseText.ModelState[""][i];

                }
                myApp.alert(errortextMsg, Wrong, function () { });

            } else {
                myApp.alert(responseText.Message, Wrong, function () { });
            }
        } else {
            myApp.alert(responseText.Message, Wrong, function () { });
        }


    }
}
function GetCurrentDateTime(date) {
    var currentdate = new Date();
    if (date == '') {
        currentdate = new Date();
    }
    else {
        currentdate = date;
    }

    var month = parseInt((currentdate.getMonth() + 1));
    var day = parseInt(currentdate.getDate());
    var datetime;

    if (month < 10) {
        if (day < 10) {
            datetime = currentdate.getFullYear() + "-0" + (currentdate.getMonth() + 1) + "-0" + currentdate.getDate();
            //+ " T"+ currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
        }
        else {
            datetime = currentdate.getFullYear() + "-0" + (currentdate.getMonth() + 1) + "-" + currentdate.getDate();
            //+ " T" + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
        }
    }
    else {
        if (day < 10) {
            datetime = currentdate.getFullYear() + "-" + (currentdate.getMonth() + 1) + "-0" + currentdate.getDate();
            //+ " T" + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
        }
        else {
            datetime = currentdate.getFullYear() + "-" + (currentdate.getMonth() + 1) + "-" + currentdate.getDate();
            //+ " T" + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
        }
    }

    return datetime;
}

function captureVideo(vedioType) {
    if (vedioType == 1) {
        navigator.device.capture.captureVideo(captureVideoSuccess, captureError, { limit: 1, duration: 60 });
    } else {
        navigator.camera.getPicture(captureVideoSuccess, captureError, {
            quality: 50, // this does nothing for me
            destinationType: navigator.camera.DestinationType.FILE_URI,
            sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
            mediaType: 1 // videos only
        });
    }

}
function captureVideoSuccess(s) {

    var file = s[0];
    if (s[0] == '/') {
        file = "file://" + s;
    } else {
        file = s[0].fullPath;
    }

    var videoFileName = 'animal-video'; // I suggest a uuid
    ShowLoader('vedio');
    VideoEditor.transcodeVideo(
        UploadVideo,
        function (err) {
            alert(ErrorOccured);
        },
        {

            fileUri: file,
            outputFileName: videoFileName,
            outputFileType: VideoEditorOptions.OutputFileType.MPEG4,
            optimizeForNetworkUse: VideoEditorOptions.OptimizeForNetworkUse.YES,
            saveToLibrary: false,
            maintainAspectRatio: true,
            width: 640,
            height: 640,
            videoBitrate: 500000, // 1 megabit
            audioChannels: 2,
            audioSampleRate: 44100,
            audioBitrate: 128000, // 128 kilobits
            progress: function (info) {
                console.log('transcodeVideo progress callback, info: ' + info);
            }
        }
    );

}
function UploadVideo(videoPath) {
    var fileReader = new FileReader(), file;
    var videoMediaType;
    fileReader.onloadend = function (readerEvt) {
        var srcSplit = this.result.split(';base64,');
        videoMedia = srcSplit[1];
        videoMediaType = srcSplit[0].replace("data:", "");
        videoMediaType = videoMediaType.split('/')[1];
        var params = {
            'ImageFilename': 'Vedio.' + videoMediaType,
            'ImageBase64': videoMedia
        };

        CallService('vedio', "POST", "/api/Users/UploadImage", params, function (vedioName) {
            if (vedioName != null) {

                var totalPhotoUrl = hostUrl + 'uploads/' + vedioName;
                document.querySelector("#videoPlayer").src = totalPhotoUrl;

            }
        });
    };


    var blob = null;


    var xhr = new XMLHttpRequest();

    xhr.open("GET", videoPath);

    xhr.responseType = "blob";

    xhr.onload = function () {

        blob = xhr.response;

        fileReader.readAsDataURL(blob);

    }

    xhr.send();

    //fileReader.onprogress = function (progressEvent) {
    //    if (progressEvent.lengthComputable) {
    //    }
    //    else {
    //        console.log('something not loading');
    //    }
    //    console.log(progressEvent.loaded / progressEvent.total);
    // var Pres= progressEvent.loaded / progressEvent.total
    // var value= parseFloat(Math.round(Pres * 100) / 100).toFixed(2);
    //    var elem = document.getElementById("myBar");
    //    var width = 1;
    //    function frame() {
    //        if ((progressEvent.loaded / progressEvent.total) >= 1 && Vediolodeded) {
    //            clearInterval(id);
    //            elem.style.width = '100%';
    //        } if ((progressEvent.loaded / progressEvent.total) >= 1) {
    //            clearInterval(id);
    //            elem.style.width = '90%';
    //        } else {
    //            width++;
    //            elem.style.width = width + '%';
    //        }
    //    }
    //    var id = setInterval(frame, 10);
    //};
}

function captureStoryVideo(vedioType) {
    if (vedioType == 1) {
        navigator.device.capture.captureVideo(captureStoryVideoSuccess, captureError, { limit: 1, duration: 300 });
    } else {
        navigator.camera.getPicture(captureStoryVideoSuccess, captureError, {
            quality: 50, // this does nothing for me
            destinationType: navigator.camera.DestinationType.FILE_URI,
            sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
            mediaType: 1 // videos only
        });
    }
}
function captureStoryVideoSuccess(s) {

    var file = s[0];
    if (s[0] == '/') {
        file = "file://" + s;
    } else {
        file = s[0].fullPath;
    }
    var videoFileName = 'Story-video'; // I suggest a uuid
    ShowLoader('Storyvedio');
    VideoEditor.transcodeVideo(
        UploadStoryVideo,
        function (err) {
            alert(ErrorOccured);
        },
        {

            fileUri: file,
            outputFileName: videoFileName,
            outputFileType: VideoEditorOptions.OutputFileType.MPEG4,
            optimizeForNetworkUse: VideoEditorOptions.OptimizeForNetworkUse.YES,
            saveToLibrary: false,
            maintainAspectRatio: true,
            width: 640,
            height: 640,
            videoBitrate: 500000, // 1 megabit
            audioChannels: 2,
            audioSampleRate: 44100,
            audioBitrate: 128000, // 128 kilobits
            progress: function (info) {
                console.log('transcodeVideo progress callback, info: ' + info);
            }
        }
    );

}
function UploadStoryVideo(videoPath) {
    debugger

    var fileReader = new FileReader(), file;
    var videoMediaType;
    fileReader.onloadend = function (readerEvt) {
        var srcSplit = this.result.split(';base64,');
        videoMedia = srcSplit[1];
        videoMediaType = srcSplit[0].replace("data:", "");
        videoMediaType = videoMediaType.split('/')[1];
        var params = {
            'ImageFilename': 'Vedio.' + videoMediaType,
            'ImageBase64': videoMedia
        };

        CallService('Storyvedio', "POST", "/api/Users/UploadImage", params, function (vedioName) {
            if (vedioName != null) {
                debugger;
                var totalPhotoUrl = hostUrl + 'uploads/' + vedioName;
                document.querySelector("#videoStoryPlayer").src = totalPhotoUrl;

            }
        });
    };


    var blob = null;


    var xhr = new XMLHttpRequest();

    xhr.open("GET", videoPath);

    xhr.responseType = "blob";

    xhr.onload = function () {

        blob = xhr.response;

        fileReader.readAsDataURL(blob);

    }

    xhr.send();
    fileReader.onprogress = function (progressEvent) {

        if (progressEvent.lengthComputable) {

            // console.log(progressEvent)
            // loadingStatus.setPercentage(progressEvent.loaded / progressEvent.total);
        } else {
            console.log('something not loading');
            // loadingStatus.increment();
        }
        console.log(progressEvent.loaded / progressEvent.total);
    };

}


function captureGiveUpVideo(vedioType) {
    if (vedioType == 1) {
        navigator.device.capture.captureVideo(captureGiveUpVideoSuccess, captureError, { limit: 1, duration: 60 });
    } else {
        navigator.camera.getPicture(captureGiveUpVideoSuccess, captureError, {
            quality: 50, // this does nothing for me
            destinationType: navigator.camera.DestinationType.FILE_URI,
            sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
            mediaType: 1 // videos only
        });
    }

}
function captureGiveUpVideoSuccess(s) {

    var file = s[0];
    if (s[0] == '/') {
        file = "file://" + s;
    } else {
        file = s[0].fullPath;
    }

    var videoFileName = 'animal-video'; // I suggest a uuid
    ShowLoader('LeaveRequest');
    VideoEditor.transcodeVideo(
        UploadGiveUpVideo,
        function (err) {
            alert(ErrorOccured);
        },
        {

            fileUri: file,
            outputFileName: videoFileName,
            outputFileType: VideoEditorOptions.OutputFileType.MPEG4,
            optimizeForNetworkUse: VideoEditorOptions.OptimizeForNetworkUse.YES,
            saveToLibrary: false,
            maintainAspectRatio: true,
            width: 640,
            height: 640,
            videoBitrate: 500000, // 1 megabit
            audioChannels: 2,
            audioSampleRate: 44100,
            audioBitrate: 128000, // 128 kilobits
            progress: function (info) {
                console.log('transcodeVideo progress callback, info: ' + info);
            }
        }
    );

}
function UploadGiveUpVideo(videoPath) {
    var fileReader = new FileReader(), file;
    var videoMediaType;
    fileReader.onloadend = function (readerEvt) {
        var srcSplit = this.result.split(';base64,');
        videoMedia = srcSplit[1];
        videoMediaType = srcSplit[0].replace("data:", "");
        videoMediaType = videoMediaType.split('/')[1];
        var params = {
            'ImageFilename': 'Vedio.' + videoMediaType,
            'ImageBase64': videoMedia
        };

        CallService('LeaveRequest', "POST", "/api/Users/UploadImage", params, function (vedioName) {
            if (vedioName != null) {

                var totalPhotoUrl = hostUrl + 'uploads/' + vedioName;
                document.querySelector("#videoGiveUpPlayer").src = totalPhotoUrl;

            }
        });
    };


    var blob = null;


    var xhr = new XMLHttpRequest();

    xhr.open("GET", videoPath);

    xhr.responseType = "blob";

    xhr.onload = function () {

        blob = xhr.response;

        fileReader.readAsDataURL(blob);

    }

    xhr.send();
}

function captureError(error) {
    myApp.alert(VedioDidntUpload, Wrong, function () {
        $("#vedioPage").css('display', 'none');
        $("#StoryvedioPage").css('display', 'none');
        $("#vedioGiveUpPage").css('display', 'none');
        $('input[type=radio][name=my-radio1_UploadStoryVedio]').each(function () {
            $(this).prop('checked', false);
            //this.checked = false;
        });
        $('input[type=radio][name=my-radio1_UploadAnimalVedio]').each(function () {
            $(this).prop('checked', false);
            //this.checked = false;
        });
        $('input[type=radio][name=LeaveRequest_UploadAnimalVedio]').each(function () {
            $(this).prop('checked', false);
            //this.checked = false;
        });

    });
}

function UpdateNotificationSeen(id) {
    var obj = {
        "Id": id, "IsSeen": true
    };
    CallService('', "POST", "/api/NotificationSetting/UpdateNotificationSeen/", obj, function (res) {
        if (res != null) {

   
        }
    });
}



function DrawanimalInIndexSearch(animalsList, startIndex, divAnimalContainer, itemsPerLoad, popupId, prevName) {

    var maxNumber = parseInt(startIndex + itemsPerLoad);

    if (maxNumber > animalsList.length) {
        maxNumber = animalsList.length;
    }
    for (var animalIndex = 0; animalIndex < animalsList.length; animalIndex++) {
        var animal = animalsList[animalIndex];
        var animalBlock = document.createElement("div");
        var linkAnimal = document.createElement("a");
        var animalcard = document.createElement("div");
        var animalcardcontent = document.createElement("div");
        var animalCardInner = document.createElement("div");
        var animalImage = document.createElement("img");
        var animalcardfooter = document.createElement("div");
        var animalfooterContent = document.createElement("div");

        var animalfooterRow = document.createElement("div");
        var shelterNsmeDiv = document.createElement("div");
        var animalNsmeDiv = document.createElement("div");
        var animalTypeDiv = document.createElement("div");
        var aimalPlaceDiv = document.createElement("div");

        animalBlock.className = 'col-50 anmIndxSrc-block';
        animalBlock.id = 'divAnimalIndexsrc_' + animal.Id;
        linkAnimal.id = 'animalIndexSrc_' + animal.Id;

        if (animal.TypeId == 1) {
            animalcard.className = 'card card-refuge';
        } else {
            animalcard.className = 'card card-person';
        }


        animalcardcontent.className = 'card-content';
        animalCardInner.className = 'card-content-inner';
        animalcardfooter.className = 'card-footer';
        animalfooterContent.className = 'content-block';
        shelterNsmeDiv.className = 'col-100 short_title';
        animalNsmeDiv.className = 'col-33 short_title';
        animalTypeDiv.className = 'col-33 short_title';
        aimalPlaceDiv.className = 'col-33 short_title';
        animalfooterRow.className = "row";


        animalBlock.appendChild(linkAnimal);
        linkAnimal.appendChild(animalcard);
        animalcard.appendChild(animalcardcontent);
        animalcard.appendChild(animalcardfooter);

        animalcardcontent.appendChild(animalCardInner);
        animalCardInner.appendChild(animalImage);

        if (animal.ImageUrl != null && animal.ImageUrl != "") {
            animalImage.src = animal.ImageUrl;
        } else {
            animalImage.src = "img/NoImg.jpg";
        }
        animalImage.setAttribute('onerror', 'onImgError(this);');

        animalcardfooter.appendChild(animalfooterContent);
        animalfooterContent.appendChild(animalfooterRow);
        animalfooterRow.appendChild(shelterNsmeDiv);
        var owner = "";
        if (animal.Shelter != null) {
            owner = animal.Shelter;
        } else {
            owner = animal.UserFullName;
        }
        shelterNsmeDiv.innerHTML = owner;
        animalfooterRow.appendChild(animalNsmeDiv);
        animalNsmeDiv.innerHTML = animal.Category;
        animalfooterRow.appendChild(animalTypeDiv);
        var gender = "";
        if (animal.Gendre == "0") {
            gender = MaleGender;

        }
        else {
            gender = FemaleGender;
        }
        animalTypeDiv.innerHTML = gender;
        animalfooterRow.appendChild(aimalPlaceDiv);
        aimalPlaceDiv.innerHTML = animal.City;

        divAnimalContainer.appendChild(animalBlock);
        $('#animalIndexSrc_' + animal.Id).on('click', function () {
            var elemId = $(this).attr('id');
            var animalId = elemId.split('_')[1];
            for (var i = 0; i < animalsList.length; ++i) {
                if (animalsList[i].Id == animalId) {
                    reqAnimal = animalsList[i];
                    break;
                }
            }
            myApp.closeModal("#" + popupId);
            mainView.router.loadPage({ pageName: 'animalDetails', query: { animal: reqAnimal, PrevName: prevName } });
        });
    }

}
function DrawanimalInIndex(animalsList, startIndex, divAnimalContainer, itemsPerLoad, popupId, prevName) {

    var maxNumber = parseInt(startIndex + itemsPerLoad);

    if (maxNumber > animalsList.length) {
        maxNumber = animalsList.length;
    }
    for (var animalIndex = 0; animalIndex < animalsList.length; animalIndex++) {
        var animal = animalsList[animalIndex];
        var animalBlock = document.createElement("div");
        var linkAnimal = document.createElement("a");
        var animalcard = document.createElement("div");
        var animalcardcontent = document.createElement("div");
        var animalCardInner = document.createElement("div");
        var animalImage = document.createElement("img");
        var animalcardfooter = document.createElement("div");
        var animalfooterContent = document.createElement("div");

        var animalfooterRow = document.createElement("div");
        var shelterNsmeDiv = document.createElement("div");
        var animalNsmeDiv = document.createElement("div");
        var animalTypeDiv = document.createElement("div");
        var aimalPlaceDiv = document.createElement("div");

        animalBlock.className = 'col-50 anmIndx-block';
        animalBlock.id = 'divAnimalIndex_' + animal.Id;
        linkAnimal.id = 'animalIndex_' + animal.Id;

        if (animal.TypeId == 1) {
            animalcard.className = 'card card-refuge';
        } else {
            animalcard.className = 'card card-person';
        }


        animalcardcontent.className = 'card-content';
        animalCardInner.className = 'card-content-inner';
        animalcardfooter.className = 'card-footer';
        animalfooterContent.className = 'content-block';
        shelterNsmeDiv.className = 'col-100 short_title';
        animalNsmeDiv.className = 'col-33 short_title';
        animalTypeDiv.className = 'col-33 short_title';
        aimalPlaceDiv.className = 'col-33 short_title';
        animalfooterRow.className = "row";


        animalBlock.appendChild(linkAnimal);
        linkAnimal.appendChild(animalcard);
        animalcard.appendChild(animalcardcontent);
        animalcard.appendChild(animalcardfooter);

        animalcardcontent.appendChild(animalCardInner);
        animalCardInner.appendChild(animalImage);

        if (animal.ImageUrl != null && animal.ImageUrl != "") {
            animalImage.src = animal.ImageUrl;
        } else {
            animalImage.src = "img/NoImg.jpg";
        }
        animalImage.setAttribute('onerror', 'onImgError(this);');

        animalcardfooter.appendChild(animalfooterContent);
        animalfooterContent.appendChild(animalfooterRow);
        animalfooterRow.appendChild(shelterNsmeDiv);
        var owner = "";
        if (animal.Shelter != null) {
            owner = animal.Shelter;
        } else {
            owner = animal.UserFullName;
        }
        shelterNsmeDiv.innerHTML = owner;
        animalfooterRow.appendChild(animalNsmeDiv);
        animalNsmeDiv.innerHTML = animal.Category;
        animalfooterRow.appendChild(animalTypeDiv);
        var gender = "";
        if (animal.Gendre == "0") {
            gender = MaleGender;

        }
        else {
            gender = FemaleGender;
        }
        animalTypeDiv.innerHTML = gender;
        animalfooterRow.appendChild(aimalPlaceDiv);
        aimalPlaceDiv.innerHTML = animal.City;

        divAnimalContainer.appendChild(animalBlock);
        $('#animalIndex_' + animal.Id).on('click', function () {
            var elemId = $(this).attr('id');
            var animalId = elemId.split('_')[1];
            for (var i = 0; i < animalsList.length; ++i) {
                if (animalsList[i].Id == animalId) {
                    reqAnimal = animalsList[i];
                    break;
                }
            }
            myApp.closeModal("#" + popupId);
            mainView.router.loadPage({ pageName: 'animalDetails', query: { animal: reqAnimal, PrevName: prevName } });
        });
    }

}
function DrawanimalInHome(animalsList, startIndex, divAnimalContainer, itemsPerLoad, popupId, prevName) {

    var maxNumber = parseInt(startIndex + itemsPerLoad);

    if (maxNumber > animalsList.length) {
        maxNumber = animalsList.length;
    }
    for (var animalIndex = startIndex; animalIndex < maxNumber; animalIndex++) {
        var animal = animalsList[animalIndex];
        var animalBlock = document.createElement("div");
        var linkAnimal = document.createElement("a");
        var animalcard = document.createElement("div");
        var animalcardcontent = document.createElement("div");
        var animalCardInner = document.createElement("div");
        var animalImage = document.createElement("img");
        var animalcardfooter = document.createElement("div");
        var animalfooterContent = document.createElement("div");

        var animalfooterRow = document.createElement("div");
        var shelterNsmeDiv = document.createElement("div");
        var animalNsmeDiv = document.createElement("div");
        var animalTypeDiv = document.createElement("div");
        var aimalPlaceDiv = document.createElement("div");

        animalBlock.className = 'col-50 anm-block';
        animalBlock.id = 'divAnimal_' + animal.Id;
        linkAnimal.id = 'animal_' + animal.Id;

        if (animal.TypeId == 1) {
            animalcard.className = 'card card-refuge';
        } else {
            animalcard.className = 'card card-person';
        }


        animalcardcontent.className = 'card-content';
        animalCardInner.className = 'card-content-inner';
        animalcardfooter.className = 'card-footer';
        animalfooterContent.className = 'content-block';
        shelterNsmeDiv.className = 'col-100 short_title';
        animalNsmeDiv.className = 'col-33 short_title';
        animalTypeDiv.className = 'col-33 short_title';
        aimalPlaceDiv.className = 'col-33 short_title';
        animalfooterRow.className = "row";


        animalBlock.appendChild(linkAnimal);
        linkAnimal.appendChild(animalcard);
        animalcard.appendChild(animalcardcontent);
        animalcard.appendChild(animalcardfooter);

        animalcardcontent.appendChild(animalCardInner);
        animalCardInner.appendChild(animalImage);

        if (animal.ImageUrl != null && animal.ImageUrl != "") {
            animalImage.src = animal.ImageUrl;
        } else {
            animalImage.src = "img/NoImg.jpg";
        }
        animalImage.setAttribute('onerror', 'onImgError(this);');

        animalcardfooter.appendChild(animalfooterContent);
        animalfooterContent.appendChild(animalfooterRow);
        animalfooterRow.appendChild(shelterNsmeDiv);
        var owner = "";
        if (animal.Shelter != null) {
            owner = animal.Shelter;
        } else {
            owner = animal.UserFullName;
        }
        shelterNsmeDiv.innerHTML = owner;
        animalfooterRow.appendChild(animalNsmeDiv);
        animalNsmeDiv.innerHTML = animal.Category;
        animalfooterRow.appendChild(animalTypeDiv);
        var gender = "";
        if (animal.Gendre == "0") {
            gender = MaleGender;

        }
        else {
            gender = FemaleGender;
        }
        animalTypeDiv.innerHTML = gender;
        animalfooterRow.appendChild(aimalPlaceDiv);
        aimalPlaceDiv.innerHTML = animal.City;

        divAnimalContainer.appendChild(animalBlock);
        $('#animal_' + animal.Id).on('click', function () {
            var elemId = $(this).attr('id');
            var animalId = elemId.split('_')[1];
            for (var i = 0; i < animalsList.length; ++i) {
                if (animalsList[i].Id == animalId) {
                    reqAnimal = animalsList[i];
                    break;
                }
            }
            myApp.closeModal("#" + popupId);
            mainView.router.loadPage({ pageName: 'animalDetails', query: { animal: reqAnimal, PrevName: prevName } });
        });
    }

}
function DrawUseranimal(animalsList, startIndex, parentDiv, popupId, prevName) {

    for (var animalIndex = startIndex; animalIndex < animalsList.length; animalIndex++) {
        var animal = animalsList[animalIndex];
        var animalBlock = document.createElement("div");
        var linkAnimal = document.createElement("a");
        var animalcard = document.createElement("div");
        var animalcardcontent = document.createElement("div");
        var animalCardInner = document.createElement("div");
        var animalImage = document.createElement("img");
        var animalcardfooter = document.createElement("div");
        var animalfooterContent = document.createElement("div");

        var animalfooterRow = document.createElement("div");
        var shelterNsmeDiv = document.createElement("div");
        var animalNsmeDiv = document.createElement("div");
        var animalTypeDiv = document.createElement("div");
        var aimalPlaceDiv = document.createElement("div");


        animalBlock.id = 'divUserAnimal_' + animal.Id;
        linkAnimal.id = 'Useranimal_' + animal.Id;
        if (animal.shelterType == 1) {
            animalcard.className = 'card card-refuge';
        } else {
            animalcard.className = 'card card-person';
        }

        animalBlock.className = 'col-50 anm-block';
        animalcardcontent.className = 'card-content';
        animalCardInner.className = 'card-content-inner';
        animalcardfooter.className = 'card-footer';
        animalfooterContent.className = 'content-block';
        shelterNsmeDiv.className = 'col-100 short_title';
        animalNsmeDiv.className = 'col-33 short_title';
        animalTypeDiv.className = 'col-33 short_title';
        aimalPlaceDiv.className = 'col-33 short_title';
        animalfooterRow.className = "row";


        animalBlock.appendChild(linkAnimal);
        linkAnimal.appendChild(animalcard);
        animalcard.appendChild(animalcardcontent);
        animalcard.appendChild(animalcardfooter);

        animalcardcontent.appendChild(animalCardInner);
        animalCardInner.appendChild(animalImage);
        if (animal.ImageUrl != null && animal.ImageUrl != "") {
            animalImage.src = animal.ImageUrl;
        } else {
            animalImage.src = "img/NoImg.jpg";
        }
        animalImage.setAttribute('onerror', 'onImgError(this);');

        animalcardfooter.appendChild(animalfooterContent);
        animalfooterContent.appendChild(animalfooterRow);
        animalfooterRow.appendChild(shelterNsmeDiv);
        shelterNsmeDiv.innerHTML = animal.AnimalName;
        animalfooterRow.appendChild(animalNsmeDiv);
        var gender = "";
        if (animal.Gendre == "0") {
            gender = MaleGender;
        }
        else {
            gender = FemaleGender;
        }
        animalNsmeDiv.innerHTML = gender;
        //animalfooterRow.appendChild(animalTypeDiv);
        //animalTypeDiv.innerHTML = animal.TypeName;
        animalfooterRow.appendChild(aimalPlaceDiv);
        if (animal.City != null) {
            aimalPlaceDiv.innerHTML = animal.City;
        } 
        

        parentDiv.appendChild(animalBlock);
        $('#Useranimal_' + animal.Id).on('click', function () {

            var elemId = $(this).attr('id');
            var animalId = elemId.split('_')[1];
            for (var i = 0; i < animalsList.length; ++i) {
                if (animalsList[i].Id == animalId) {
                    reqAnimal = animalsList[i];
                    break;
                }
            }
            myApp.closeModal("#" + popupId);
            mainView.router.loadPage({ pageName: 'animalDetails', query: { animal: reqAnimal, PrevName: prevName } });
        });
    }

}
function DrawUseruserShelter(userSheltersList, startIndex, parentDiv, itemsPerLoad, popupId) {

    var maxNumber = parseInt(startIndex + itemsPerLoad);

    if (maxNumber > userSheltersList.length) {
        maxNumber = userSheltersList.length;
    }

    for (var shelterIndex = startIndex; shelterIndex < maxNumber; shelterIndex++) {
        var shelter = userSheltersList[shelterIndex];
        var shelterBlock = document.createElement("div");
        var linkShelter = document.createElement("a");
        var sheltercard = document.createElement("div");
        var sheltercardcontent = document.createElement("div");
        var shelterCardInner = document.createElement("div");
        var shelterImage = document.createElement("img");
        var sheltercardfooter = document.createElement("div");
        var shelterfooterContent = document.createElement("div");

        var shelterfooterRow = document.createElement("div");
        var shelterNsmeDiv = document.createElement("div");
        var volunteerDiv = document.createElement("div");

        shelterBlock.className = 'col-50 anm-block';
        shelterBlock.id = 'divShelter' + shelterIndex + '_' + shelter.Id;
        linkShelter.id = 'Shelter' + shelterIndex + '_' + shelter.Id;
        sheltercard.className = 'card card-refuge';
        sheltercardcontent.className = 'card-content';
        shelterCardInner.className = 'card-content-inner';
        sheltercardfooter.className = 'card-footer';
        shelterfooterContent.className = 'content-block';
        shelterNsmeDiv.className = 'col-100 short_title';
        volunteerDiv.className = 'col-100 short_title';
        shelterfooterRow.className = "row";


        shelterBlock.appendChild(linkShelter);
        linkShelter.appendChild(sheltercard);
        sheltercard.appendChild(sheltercardcontent);
        sheltercard.appendChild(sheltercardfooter);

        sheltercardcontent.appendChild(shelterCardInner);
        shelterCardInner.appendChild(shelterImage);
        if (shelter.PhotoUrl != null && shelter.PhotoUrl != "") {
            shelterImage.src = shelter.PhotoUrl;
        } else {
            shelterImage.src = "img/NoImg.jpg";
        }

        shelterImage.setAttribute('onerror', 'onImgError(this);');
        sheltercardfooter.appendChild(shelterfooterContent);
        shelterfooterContent.appendChild(shelterfooterRow);
        shelterfooterRow.appendChild(shelterNsmeDiv);
        shelterNsmeDiv.innerHTML = shelter.Name;
        shelterfooterRow.appendChild(volunteerDiv);
        volunteerDiv.innerHTML = shelter.Volunteers;


        parentDiv.appendChild(shelterBlock);
        $('#Shelter' + shelterIndex + '_' + shelter.Id).on('click', function () {
            var elemId = $(this).attr('id');
            var shelteId = elemId.split('_')[1];
            for (var i = 0; i < userSheltersList.length; ++i) {
                if (userSheltersList[i].Id == shelteId) {
                    reqShelter = userSheltersList[i];
                    break;
                }
            }
            myApp.closeModal("#" + popupId);
            mainView.router.loadPage({ pageName: 'refugeDetails', query: { Shelter: reqShelter } });
        });
    }

}

function DrawShelters(userSheltersList, startIndex, parentDiv, itemsPerLoad, popupId) {

    var maxNumber = parseInt(startIndex + itemsPerLoad);

    if (maxNumber > userSheltersList.length) {
        maxNumber = userSheltersList.length;
    }

    for (var shelterIndex = startIndex; shelterIndex < maxNumber; shelterIndex++) {
        var shelter = userSheltersList[shelterIndex];
        var shelterBlock = document.createElement("div");
        var linkShelter = document.createElement("a");
        var sheltercard = document.createElement("div");
        var sheltercardcontent = document.createElement("div");
        var shelterCardInner = document.createElement("div");
        var shelterImage = document.createElement("img");
        var sheltercardfooter = document.createElement("div");
        var shelterfooterContent = document.createElement("div");

        var shelterfooterRow = document.createElement("div");
        var shelterNsmeDiv = document.createElement("div");
        var volunteerDiv = document.createElement("div");

        shelterBlock.className = 'col-50 anm-block';
        shelterBlock.id = 'divShelterAnimal' + shelterIndex + '_' + shelter.Id;
        linkShelter.id = 'ShelterAnimal' + shelterIndex + '_' + shelter.Id;
        sheltercard.className = 'card card-refuge';
        sheltercardcontent.className = 'card-content';
        shelterCardInner.className = 'card-content-inner';
        sheltercardfooter.className = 'card-footer';
        shelterfooterContent.className = 'content-block';
        shelterNsmeDiv.className = 'col-100 short_title';
        volunteerDiv.className = 'col-100 short_title';
        shelterfooterRow.className = "row";


        shelterBlock.appendChild(linkShelter);
        linkShelter.appendChild(sheltercard);
        sheltercard.appendChild(sheltercardcontent);
        sheltercard.appendChild(sheltercardfooter);

        sheltercardcontent.appendChild(shelterCardInner);
        shelterCardInner.appendChild(shelterImage);
        if (shelter.PhotoUrl != null && shelter.PhotoUrl != "") {
            shelterImage.src = shelter.PhotoUrl;
        } else {
            shelterImage.src = "img/NoImg.jpg";
        }

        shelterImage.setAttribute('onerror', 'onImgError(this);');
        sheltercardfooter.appendChild(shelterfooterContent);
        shelterfooterContent.appendChild(shelterfooterRow);
        shelterfooterRow.appendChild(shelterNsmeDiv);
        shelterNsmeDiv.innerHTML = shelter.Name;
        shelterfooterRow.appendChild(volunteerDiv);
        volunteerDiv.innerHTML = shelter.Volunteers;


        parentDiv.appendChild(shelterBlock);
        $('#ShelterAnimal' + shelterIndex + '_' + shelter.Id).on('click', function () {
            var elemId = $(this).attr('id');
            var shelteId = elemId.split('_')[1];
            for (var i = 0; i < userSheltersList.length; ++i) {
                if (userSheltersList[i].Id == shelteId) {
                    reqShelter = userSheltersList[i];
                    break;
                }
            }
            myApp.closeModal("#" + popupId);
            mainView.router.loadPage({ pageName: 'refugeDetails', query: { Shelter: reqShelter } });
        });
    }

}


function DrawUser(usersList, startIndex, divUsers, itemsPerLoad) {

    var maxNumber = parseInt(startIndex + itemsPerLoad);

    if (maxNumber > usersList.length) {
        maxNumber = usersList.length;
    }

    for (var userIndex = startIndex; userIndex < maxNumber; userIndex++) {
        var user = usersList[userIndex];
        var userBlock = document.createElement("div");
        var userLink = document.createElement("a");
        var useCard = document.createElement("div");
        var useContent = document.createElement("div");
        var usefooter = document.createElement("div");


        var useContentInner = document.createElement("div");
        var useContentBlock = document.createElement("div");


        var useContentImage = document.createElement("img");
        var useContentBlockRow = document.createElement("div");
        var useName = document.createElement("div");
        useName.innerHTML = ' <b> ' + user.Name + '</b>';


        if (user.PhotoUrl != null && user.PhotoUrl != "") {
            useContentImage.src = user.PhotoUrl;
        } else {
            useContentImage.src = "img/NoImg.jpg";
        }
        useContentImage.setAttribute('onerror', 'onImgError(this);');

        userBlock.className = 'col-50 anm-block';
        userBlock.id = 'divUser_' + user.Id;
        userLink.id = 'LinkUser_' + user.Id;
        useCard.className = "card card-person";
        useContent.className = "card-content";
        useContentInner.className = "card-content-inner";
        usefooter.className = "card-footer";
        useContentBlock.className = "content-block";
        useContentBlockRow.className = "row";
        useName.className = "col-100";

        useContentBlockRow.appendChild(useName);
        useContentBlock.appendChild(useContentBlockRow);
        usefooter.appendChild(useContentBlock);
        useContentInner.appendChild(useContentImage);
        useContent.appendChild(useContentInner);
        useCard.appendChild(useContent);
        useCard.appendChild(usefooter);
        userLink.appendChild(useCard);
        userBlock.appendChild(userLink);
        divUsers.appendChild(userBlock);
        $('#LinkUser_' + user.Id).on('click', function () {
            var elemId = $(this).attr('id');
            var userId = elemId.split('_')[1];
            for (var i = 0; i < usersList.length; ++i) {
                if (usersList[i].Id == userId) {
                    reqUser = usersList[i];
                    break;
                }
            }
            mainView.router.loadPage({ pageName: 'userDetails', query: { User: reqUser } });
        });
    }


};
function DrawSuccessStory(successStoryList, startIndex, parentDiv, itemsPerLoad) {

    var maxNumber = parseInt(startIndex + itemsPerLoad);

    if (maxNumber > successStoryList.length) {
        maxNumber = successStoryList.length;
    }
    for (var storyIndex = startIndex; storyIndex < maxNumber; storyIndex++) {
        var successStory = successStoryList[storyIndex];
        var storyBlock = document.createElement("div");
        var storyLink = document.createElement("a");
        var storycard = document.createElement("div");
        var storyrow = document.createElement("div");
        var storyImg = document.createElement("img");
        var storyDetails = document.createElement("div");
        var storyDetailsRow = document.createElement("div");
        var divStorytitle = document.createElement("div");
        var divStoryPesron = document.createElement("div");
        var divStorydetails = document.createElement("div");
        var divStorytitleParagraph = document.createElement("p");
        var divStoryPesronParagraph = document.createElement("p");
        var divStorydetailsParagraph = document.createElement("p");

        storyBlock.className = 'col-100 StoryBlock';
        storyBlock.id = 'story_' + successStory.Id;
        storyLink.id = 'storyLink_' + successStory.Id;
        storycard.className = 'card';
        storyrow.className = 'row';
        storyImg.className = 'col-40';
        storyDetails.className = 'col-60';
        storyDetailsRow.className = 'row';
        divStorytitle.className = 'col-100';
        divStoryPesron.className = 'col-100';
        divStorydetails.className = 'col-100';
        divStorydetailsParagraph.className = 'details';
        divStorytitleParagraph.className = 'title';
        divStorytitleParagraph.innerHTML = '<i class="ionicons ion-android-create"></i> ' + successStory.Title;
        divStoryPesronParagraph.innerHTML = '<i class="ionicons ion-person"></i> ' + successStory.AuthorName;
        divStorydetailsParagraph.innerHTML = '<i class="ionicons ion-document-text"></i> ' + successStory.details;
        if (successStory.ImageUrl != null && successStory.ImageUrl != "") {
            storyImg.src = successStory.ImageUrl;
        } else {
            storyImg.src = "img/NoImg.jpg";
        }

        storyImg.setAttribute('onerror', 'onImgError(this);');

        divStorytitle.appendChild(divStorytitleParagraph);
        divStoryPesron.appendChild(divStoryPesronParagraph);
        divStorydetails.appendChild(divStorydetailsParagraph);
        storyDetailsRow.appendChild(divStorytitle);
        storyDetailsRow.appendChild(divStoryPesron);
        storyDetailsRow.appendChild(divStorydetails);
        storyDetails.appendChild(storyDetailsRow);
        storyrow.appendChild(storyImg);
        storyrow.appendChild(storyDetails);
        storycard.appendChild(storyrow);
        storyLink.appendChild(storycard);
        storyBlock.appendChild(storyLink);
        parentDiv.appendChild(storyBlock);
        $('#storyLink_' + successStory.Id).on('click', function () {
            var elemId = $(this).attr('id');
            var storyId = elemId.split('_')[1];
            for (var i = 0; i < successStoryList.length; ++i) {
                if (successStoryList[i].Id == storyId) {
                    reqStory = successStoryList[i];
                    break;
                }
            }
            mainView.router.loadPage({ pageName: 'storyDetails', query: { Story: reqStory } });
        });
    }
}
function drawComments(comments, startIndex, itemsPerLoad, commentsDiv) {

    var maxNumber = parseInt(startIndex + itemsPerLoad);

    if (maxNumber > comments.length) {
        maxNumber = comments.length;
    }
    // var commentsDiv = document.getElementById('CommentsDiv');

    for (var index = startIndex; index < maxNumber; index++) {
        var commentList = comments[index];
        var liComment = document.createElement("li");
        liComment.className = 'Commt-bock';
        var divImgComment = document.createElement("div");
        divImgComment.className = 'chatavatar';
        var imgComment = document.createElement("img");
     

        if (commentList.UserImage != null && commentList.UserImage != "") {
            imgComment.src = commentList.UserImage;
        } else {
            imgComment.src = "img/NoImg.jpg";
        }
        imgComment.setAttribute('onerror', 'onImgError(this);');
        var divTxtContComment = document.createElement("div");
        divTxtContComment.className = 'w-clearfix column-right chat right';
        var divArrowComment = document.createElement("div");
        divArrowComment.className = 'arrow-globe right';
        var divtxtComment = document.createElement("div");
        divtxtComment.className = 'chat-text right';
        divtxtComment.innerHTML = ' <h4  style="margin-top: 0;">' + commentList.UserName + '<i class="ionicons ion-android-person" style="margin-left: 5px;"></i></h4> ' + commentList.Value;
        var divTimeComment = document.createElement("div");
        divTimeComment.className = 'chat-time right';
        divTimeComment.innerHTML = commentList.DateDifference;
        divImgComment.appendChild(imgComment);
        liComment.appendChild(divImgComment);
     
        divTxtContComment.appendChild(divArrowComment);
        divTxtContComment.appendChild(divtxtComment);
        divTxtContComment.appendChild(divTimeComment);
       
        liComment.appendChild(divTxtContComment);
      
        commentsDiv.appendChild(liComment);

    }

}
function drawMassages(massagesList, startIndex, itemsPerLoad) {

    var maxNumber = parseInt(startIndex + itemsPerLoad);

    if (maxNumber > massagesList.length) {
        maxNumber = massagesList.length;
    }
    var massagesDiv = document.getElementById('MassagesDiv');
    for (var index = startIndex; index < maxNumber; index++) {
        var Massages = massagesList[index];
        var liMassage = document.createElement("li");

        liMassage.className = 'Msg-bock';
        var divImgMassage = document.createElement("div");
        divImgMassage.className = 'chatavatar';
        var imgMassage = document.createElement("img");
        var divTxtContMassage = document.createElement("div");
        var divArrowMassages = document.createElement("div");


        var divtxtMassage = document.createElement("div");
        divtxtMassage.innerHTML = Massages.Value;


        var divTimeMassage = document.createElement("div");
        divTimeMassage.innerHTML = Massages.DateDifference;
        var userId = localStorage.getItem("UserId")
        if (Massages.SenderId == userId) {
            divTxtContMassage.className = 'w-clearfix column-right chat right';
            divArrowMassages.className = 'arrow-globe right';
            divtxtMassage.className = 'chat-text right';
            divTimeMassage.className = 'chat-time right';
        } else {
            if (Massages.ReceiverImage != null && Massages.ReceiverImage != "") {
                imgMassage.src = Massages.ReceiverImage;
            } else {
                imgMassage.src = "img/NoImg.jpg";
            }
            divTxtContMassage.className = 'column-right chat';
            divArrowMassages.className = 'arrow-globe';
            divtxtMassage.className = 'chat-text';
            divTimeMassage.className = 'chat-time';
            divImgMassage.appendChild(imgMassage);
            liMassage.appendChild(divImgMassage);
        }
        imgMassage.setAttribute('onerror', 'onImgError(this);');
        divTxtContMassage.appendChild(divArrowMassages);
        divTxtContMassage.appendChild(divtxtMassage);
        divTxtContMassage.appendChild(divTimeMassage);
        liMassage.appendChild(divTxtContMassage);
        massagesDiv.appendChild(liMassage);
    }
}
function drawAllMessages(massagesList, startIndex, itemsPerLoad,chatId) {

    var maxNumber = parseInt(startIndex + itemsPerLoad);

    if (maxNumber > massagesList.length) {
        maxNumber = massagesList.length;
    }
    var massagesDiv = document.getElementById('divAllMessages');
    for (var index = startIndex; index < maxNumber; index++) {
     
        var massages = massagesList[index];
        
        var liMassage = document.createElement("li");
        liMassage.id = "ChatLi" + massages.Id;
        if (chatId != null) {
            liMassage.className = 'item-content MsgRoom-bock active';
        } else {
            liMassage.className = 'item-content MsgRoom-bock';
        }
        var hrefMassage = document.createElement("a");
        liMassage.id = "ChatLink_" + massages.Id + "_" + massages.AnimalId;
        var divImgMassage = document.createElement("div");
        divImgMassage.className = 'item-media';
       
       
        var imgMassage = document.createElement("img");

        var divTxtContMassage = document.createElement("div");
        divTxtContMassage.className = 'item-inner';

        var divTitleMassages = document.createElement("div");
        divTitleMassages.className = 'item-title';
        var userId = localStorage.getItem("UserId");
        if (massages.SenderId == userId) {
            divTitleMassages.innerHTML = massages.ReceiverName;
            if (massages.ReceiverImage != null && massages.ReceiverImage != "") {
                imgMassage.src = massages.ReceiverImage;
            } else {
                imgMassage.src = "img/NoImg.jpg";
            }
        } else {
            divTitleMassages.innerHTML = massages.SenderName;
            if (massages.UserImage != null && massages.UserImage != "") {
                imgMassage.src = massages.UserImage;

            } else {
                imgMassage.src = "img/NoImg.jpg";
            }
        }
        imgMassage.setAttribute('onerror', 'onImgError(this);');



        var pdescMassages = document.createElement("p");

        pdescMassages.innerHTML = massages.Value;

        divImgMassage.appendChild(imgMassage);
        divTxtContMassage.appendChild(divTitleMassages);
        divTxtContMassage.appendChild(pdescMassages);
        liMassage.appendChild(divImgMassage);
        liMassage.appendChild(divTxtContMassage);
      //  liMassage.appendChild(hrefMassage);
        hrefMassage.appendChild(liMassage);
        massagesDiv.appendChild(hrefMassage);
        $('#ChatLink_' + massages.Id + "_" + massages.AnimalId).on('click', function () {

            var elemId = $(this).attr('id');
              $(this).removeClass("active");
            var messageId = elemId.split('_')[1];
            animalMassageid = elemId.split('_')[2];
            for (var i = 0; i < massagesList.length; ++i) {
                if (massagesList[i].Id == messageId) {
                    reqMassage = massagesList[i];
                    break;
                }
            }
            var CurrentuserId = localStorage.getItem("UserId");
            if (reqMassage.SenderId == CurrentuserId) {
                document.getElementById("ChatPagetitle").innerHTML = reqMassage.ReceiverName;
            } else {
                document.getElementById("ChatPagetitle").innerHTML = reqMassage.SenderName;
            }
          
            mainView.router.loadPage({ pageName: 'chat', query: { massage: reqMassage } });
        });
    }

}
function drawNotifications(notificationList, startIndex, itemsPerLoad, divContainer) {
    var maxNumber = parseInt(startIndex + itemsPerLoad);

    if (maxNumber > notificationList.length) {
        maxNumber = notificationList.length;
    }


    for (var index = startIndex; index < maxNumber; index++) {

        var notifications = notificationList[index];
        var hrefNotify = document.createElement("a");
        if (notifications.AnimalShelterId != null)
        {
            hrefNotify.id = "Notify" + "_" + index +"_" + notifications.Id + "_" + notifications.AnimalShelterId;
        }
        if (notifications.TypeId == 2) {         
            hrefNotify.id = "NotifyRequest" + "_" + index + "_" + notifications.Id + "_" + notifications.RequestId;
        }
        else if (notifications.TypeId == 1) {
            hrefNotify.id = "NotifyRequestAnimal" + "_" + index + "_" + notifications.Id + "_" + notifications.RequestId;
        }
        else if (notifications.TypeId == 4) {
            hrefNotify.id = "NotifyChat" + "_" + index + "_" + notifications.Id + "_" + notifications.RequestId;
        }
         else if (notifications.ApplicationUserId != null) {
            hrefNotify.id = "Notify" + "_" + +index + "_" + notifications.ApplicationUserId;
         }

       // hrefNotify.id = "Notify" + index + "_" + notifications.AnimalShelterId;
        var divNotify = document.createElement("div");
        var hNotify = document.createElement("h3");
        // var iNotify = document.createElement("i");
        var pNotify = document.createElement("p");

        if (notifications.IsSeen == false) {
            divNotify.className = 'card unRead';
        } else {
             divNotify.className = 'card';
        }

        hrefNotify.className = 'notify-block';
       
        //  iNotify.className = 'ionicons ion-android-notifications';

        // pNotify.appendChild(iNotify)
        if (CurrentLanguage == 'ar')
            pNotify.innerHTML += notifications.Message;
        else
            pNotify.innerHTML += notifications.EnglishMessage;


        hNotify.appendChild(pNotify);
        divNotify.appendChild(hNotify);
        hrefNotify.appendChild(divNotify);
        divContainer.appendChild(hrefNotify);
        if (notifications.AnimalShelterId != null) {
            
            $('#Notify' + "_" + index + "_" + notifications.Id + "_" + notifications.AnimalShelterId).on('click', function () {

                var elemId = $(this).attr('id');
                var notificationId = elemId.split('_')[2];
                var shelterId = elemId.split('_')[3];
                UpdateNotificationSeen(notificationId);
                    CallService("", "GET", "/api/AnimalShelter/view/" + shelterId, "", function (reqShelter) {

                        if (reqShelter != null && typeof reqShelter.message == 'undefined') {
                            mainView.router.loadPage({ pageName: 'refugeDetails', query: { Shelter: reqShelter } });
                        }

                    });


                    

            });
        }
        else if (notifications.TypeId == 2) {
            $('#NotifyRequest' + "_" + index + "_" + notifications.Id + "_" + notifications.RequestId).on('click', function () {

                var elemId = $(this).attr('id');
            
                var textId = elemId.split('_')[0];
                var notificationId = elemId.split('_')[2];
                var shelterId = elemId.split('_')[3];
                UpdateNotificationSeen(notificationId);
                mainView.router.loadPage({ pageName: 'AdoptRequestPopup' });

               
            });
        }
        else if (notifications.TypeId == 1) {
            $('#NotifyRequestAnimal' + "_" + index + "_"  + notifications.Id + "_" + notifications.RequestId).on('click', function () {

                var elemId = $(this).attr('id');

                var textId = elemId.split('_')[0];
                var notificationId = elemId.split('_')[2];
                var animalId = elemId.split('_')[3];

                UpdateNotificationSeen(notificationId);
                var obj = {
                    'AnimalId': animalId,
                    'Language': CurrentLanguage
                };


                CallService("", "POST", "/api/Animal/GetAnimlById", obj, function (animal) {
                    if (animal != null && typeof animal.message == 'undefined') {
                        mainView.router.loadPage({ pageName: 'animalDetails', query: { animal: animal, PrevName: 'index' } });
                    }
                   
                });
            });
          
           
        }
        else if (notifications.TypeId == 4) {
            $("#NotifyChat" + "_" + index + "_" + notifications.Id + "_" + notifications.RequestId).on('click', function () {

                var elemId = $(this).attr('id');

                var textId = elemId.split('_')[0];
                var notificationId = elemId.split('_')[2];
                var chatId = elemId.split('_')[3];

                UpdateNotificationSeen(notificationId);
                
                mainView.router.loadPage({ pageName: 'messages', query: { UserChat: 1 } });

               
            });


        }
        else if (notifications.ApplicationUserId != null) {
            $('#Notify' + '_' + index + '_' + notifications.Id + "_" + notifications.ApplicationUserId).on('click', function () {

                var elemId = $(this).attr('id');
                
                var notificationId = elemId.split('_')[2];
                var userId = elemId.split('_')[3];
                UpdateNotificationSeen(notificationId);
                CallService("", "GET", "/api/Users/GetUserById/" + userId, "", function (reqUser) {

                    if (reqUser != null && typeof reqUser.message == 'undefined') {
                        
                        mainView.router.loadPage({ pageName: 'userDetails', query: { User: reqUser } });
                    }

                });

            });

            
        }
    
    }
}

function DrawanigAdoptRequest(adoptionRequestList, startIndex, parentDiv, itemsPerLoad) {

    var maxNumber = parseInt(startIndex + itemsPerLoad);

    if (maxNumber > adoptionRequestList.length) {
        maxNumber = adoptionRequestList.length;
    }
    for (var shelterIndex = startIndex; shelterIndex < maxNumber; shelterIndex++) {
        var request = adoptionRequestList[shelterIndex];
        var shelterBlock = document.createElement("div");
        var linkShelter = document.createElement("a");
        var sheltercard = document.createElement("div");
        var sheltercardcontent = document.createElement("div");
        var shelterCardInner = document.createElement("div");
        var shelterImage = document.createElement("img");
        var sheltercardfooter = document.createElement("div");
        var shelterfooterContent = document.createElement("div");

        var shelterfooterRow = document.createElement("div");
        var shelterNsmeDiv = document.createElement("div");
        var volunteerDiv = document.createElement("div");

        shelterBlock.className = 'col-50 anm-block';
        shelterBlock.id = 'divAdoption' + shelterIndex + '_' + request.Id + '_' + request.ApplicationUserId;
        linkShelter.id = 'LinkAdoption' + shelterIndex + '_' + request.Id + '_' + request.ApplicationUserId;
        sheltercard.className = 'card card-refuge';
        sheltercardcontent.className = 'card-content';
        shelterCardInner.className = 'card-content-inner';
        sheltercardfooter.className = 'card-footer';
        shelterfooterContent.className = 'content-block';
        shelterNsmeDiv.className = 'col-100 short_title';
        volunteerDiv.className = 'col-100 short_title';
        shelterfooterRow.className = "row";


        shelterBlock.appendChild(linkShelter);
        linkShelter.appendChild(sheltercard);
        sheltercard.appendChild(sheltercardcontent);
        sheltercard.appendChild(sheltercardfooter);

        sheltercardcontent.appendChild(shelterCardInner);
        shelterCardInner.appendChild(shelterImage);
        if (request.PhotoUrl != null && request.PhotoUrl != "") {
            shelterImage.src = request.PhotoUrl;
        } else {
            shelterImage.src = "img/NoImg.jpg";
        }

        shelterImage.setAttribute('onerror', 'onImgError(this);');
        sheltercardfooter.appendChild(shelterfooterContent);
        shelterfooterContent.appendChild(shelterfooterRow);
        shelterfooterRow.appendChild(shelterNsmeDiv);
        shelterNsmeDiv.innerHTML = request.UserName;
        shelterfooterRow.appendChild(volunteerDiv);
        volunteerDiv.innerHTML = request.AnimalName;
        parentDiv.appendChild(shelterBlock);

        $('#LinkAdoption' + shelterIndex + '_' + request.Id + '_' + request.ApplicationUserId).on('click', function () {
            var elemId = $(this).attr('id');
            var userId = elemId.split('_')[2];
            var requestId = elemId.split('_')[1];
            CallService("", "GET", "/api/AdoptionRequest/AdoptionRequestById/" + requestId+"/"+CurrentLanguage,"", function (AdoptionRequest) {
                if (AdoptionRequest != null ) {
                    mainView.router.loadPage({ pageName: 'adoptedRequesteDetails', query: { request: AdoptionRequest } });
                }
            });

          
                

            
        });
    }

}


myApp.onPageBeforeInit('landing', function (page) {

}).trigger();
myApp.onPageBeforeAnimation('login', function (page) {
    GoToLoginPage(page);
}).trigger();
myApp.onPageBeforeAnimation('signup', function (page) {
    GoTosignupPage(page);
}).trigger();
myApp.onPageBeforeAnimation('activation', function (page) {
    GoToaCtivationPage(page);
}).trigger();
myApp.onPageBeforeAnimation('index', function (page) {
    GoToindexPage(page);
}).trigger();
myApp.onPageBeforeAnimation('profile', function (page) {
    GoToProfilePage(page);
}).trigger();
myApp.onPageBeforeAnimation('animalDetails', function (page) {
    GoToAnimalDetailsPage(page);
}).trigger();
myApp.onPageBeforeAnimation('refuge', function (page) {

    GoToRefugePage(page);
}).trigger();
myApp.onPageBeforeAnimation('refugeDetails', function (page) {
    GoTorefugeDetailsPage(page);
}).trigger();
myApp.onPageBeforeAnimation('volunteer', function (page) {
    GoTovolunteerPage(page);
}).trigger();
myApp.onPageBeforeAnimation('users', function (page) {
    GoTousersPage(page);
}).trigger();
myApp.onPageBeforeAnimation('userDetails', function (page) {
    GoToUserDetailsPage(page);
}).trigger();
myApp.onPageBeforeAnimation('addAnimal', function (page) {
    GoToAddAnimalPage(page);
}).trigger();
myApp.onPageBeforeAnimation('lostAnimals', function (page) {
    GoTolostAnimalsPage(page);
}).trigger();
myApp.onPageBeforeAnimation('needCareAnimals', function (page) {
    GoToneedCareAnimalsPage(page);
}).trigger();
myApp.onPageBeforeAnimation('successStory', function (page) {
    GoTosuccessStoryPage(page);
}).trigger();
myApp.onPageBeforeAnimation('storyDetails', function (page) {
    GoTostoryDetailsPage(page);
}).trigger();
myApp.onPageBeforeAnimation('mapPage', function (page) {
    GoToMapPage(page);
}).trigger();
myApp.onPageBeforeAnimation('chat', function (page) {
    GoTochatPage(page);
}).trigger();
myApp.onPageBeforeAnimation('messages', function (page) {
    GoTomessagesPage(page);
}).trigger();
myApp.onPageBeforeAnimation('forgetPass', function (page) {
    GoToforgetPassPage(page);
}).trigger();
myApp.onPageBeforeAnimation('changePassword', function (page) {
    GoTochangePasswordPage(page);
}).trigger();
myApp.onPageBeforeAnimation('resetPassword', function (page) {
    GoToresetPasswordPage(page);
}).trigger();
myApp.onPageBeforeAnimation('addSuccessStory', function (page) {
    GoToaddSuccessStoryPage(page);
}).trigger();
myApp.onPageBeforeAnimation('searchPage', function (page) {
    GoTosearchPage(page);
}).trigger();
myApp.onPageBeforeAnimation('notification', function (page) {
    GoToNotificationPage(page);
}).trigger();
myApp.onPageBeforeAnimation('AdoptPopup', function (page) {
    GoToAdoptPopupPage(page);
}).trigger();
myApp.onPageBeforeAnimation('AdoptRequestPopup', function (page) {
    GoToAdoptRequestPopupPage();
}).trigger();
myApp.onPageBeforeAnimation('VolunteerPopup', function (page) {
    GoToVolunteerPopupPage(page);
}).trigger();
myApp.onPageBeforeAnimation('EditProfilePopup', function (page) {
    GoToEditProfilePage(page);
}).trigger();
myApp.onPageBeforeAnimation('LeaveRequest', function (page) {
    GoToLeaveRequestPage(page);
}).trigger();
myApp.onPageBeforeAnimation('searchResult', function (page) {
    GoTosearchResultPage(page);
}).trigger();
myApp.onPageBeforeAnimation('adoptedRequestPopup', function (page) {
    GoToadoptedRequestPopup(page);
}).trigger();
myApp.onPageBeforeAnimation('adoptedRequesteDetails', function (page) {
    GoToadoptedRequesteDetails(page);
}).trigger();


myApp.onPageBeforeAnimation('Mychat', function (page) {
    GoToMychatPage(page);
}).trigger();
myApp.onPageBeforeAnimation('Mymessages', function (page) {
    GoToMymessagesPage(page);
}).trigger();

//$$('#AdoptPopupPage').on('opened', function () {
//    GoToAdoptPopupPage();
//});
//$$('#AdoptRequestPopupPage').on('opened', function () {
//    GoToAdoptRequestPopupPage();
//});
//$$('#VolunteerPopupPage').on('opened', function (page) {
//    GoToVolunteerPopupPage(page);
//});
//$$('#EditProfilePopupPage').on('opened', function (page) {
//    GoToEditProfilePage(page);
//});
//$$('#LeaveRequestPage').on('opened', function (page) {
//    GoToLeaveRequestPage(page);
//});
//$$('#adoptedRequestPopupPage').on('opened', function (page) {
//    GoToadoptedRequestPopup(page);
//});
$$('#reportPopupPage').on('opened', function (page) {
    GoToreportPopupPage(page);
});

$$('#commentPopupPage').on('opened', function (page) {
    GoToCommentPopupPage(page);
});

$$('#commentStoryPopupPage').on('opened', function (page) {
    GoToCommentStoryPopupPage(page);
});


myApp.init();
