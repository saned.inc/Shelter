﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
var handleOpenURL;
(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );
    function onPause() {
        // TODO: This application has been suspended. Save application state here.
       // alert('pause');
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
        //alert('resume');
    };

    function keyboardHideHandler(e) {
       // alert('hidekeyboard');
  
        document.getElementById("AddanimalContent").style.height = '95%';
        document.getElementById("LeaveRequestContent").style.height = '95%';
        document.getElementById("addSuccessStoryContent").style.height = '95%';
        document.getElementById("adoptedRequestPagecontent").style.height = '95%';
        

    }
    function keyboardShowHandler(e) {
      //  alert('showkeyboard');
    
      
        document.getElementById("AddanimalContent").style.height = '80%';
        document.getElementById("LeaveRequestContent").style.height = '80%';
        document.getElementById("addSuccessStoryContent").style.height = '80%';
        document.getElementById("adoptedRequestPagecontent").style.height = '80%';

    }
    function onDeviceReady() {    
        document.addEventListener( 'pause', onPause.bind( this ), false );
        document.addEventListener( 'resume', onResume.bind( this ), false );
        window.addEventListener('native.showkeyboard', keyboardShowHandler);
       // window.addEventListener('native.keyboardshow', keyboardShowHandler);

        //function keyboardShowHandler(e) {
        //    alert('Keyboard height is: ' + e.keyboardHeight);
        //}
        window.addEventListener('native.hidekeyboard', keyboardHideHandler);
       // window.addEventListener('native.keyboardhide', keyboardHideHandler);

        //function keyboardHideHandler(e) {
        //    alert('Goodnight, sweet prince');
        //}
    };
    

    
})();



