﻿

var accessToken;
var UserData = null;
var serviceURL = 'http://asnanapi.saned-projects.com/';

function ShowLoader(pageName) {
    var divPage = document.getElementById(pageName + 'Page');
    var divLoader = document.createElement('div');
    var divCube = document.createElement('div');
    var divCubeP1 = document.createElement('div');
    var divCubeP2 = document.createElement('div');
    var divCubeP3 = document.createElement('div');
    var divCubeP4 = document.createElement('div');
    var hdrWait = document.createElement('h3');

    divLoader.className += 'loader divLoader';
    divCube.className += 'cssload-thecube';
    divCubeP1.className += 'cssload-cube cssload-c1';
    divCubeP2.className += 'cssload-cube cssload-c2';
    divCubeP3.className += 'cssload-cube cssload-c3';
    divCubeP4.className += 'cssload-cube cssload-c4';
    hdrWait.innerHTML = 'برجاء الإنتظار';

    divCube.appendChild(divCubeP1);
    divCube.appendChild(divCubeP2);
    divCube.appendChild(divCubeP3);
    divCube.appendChild(divCubeP4);
    divLoader.appendChild(divCube);
    divLoader.appendChild(hdrWait);
    divPage.appendChild(divLoader);
    $(".loader").fadeIn("slow");
}

function HideLoader() {
    $(".loader").fadeOut("slow");
    $('div').each(function () {
        var div = $(this);
        if ($(this).hasClass('loader divLoader')) {
            $(this).remove();
        }
    });
}



var googleapi = {
    authorize: function (options) {
        var deferred = $.Deferred();
        var authUrl = 'https://accounts.google.com/o/oauth2/auth?' + $.param({
            client_id: options.client_id,
            redirect_uri: options.redirect_uri,
            response_type: 'code',
            scope: options.scope
        });

        var authWindow = window.open(authUrl, '_blank', 'location=no,toolbar=no');

        $(authWindow).on('loadstart', function (e) {
            var url = e.originalEvent.url;
            var code = /\?code=(.+)$/.exec(url);
            var error = /\?error=(.+)$/.exec(url);

            if (code || error) {
                authWindow.close();
            }

            if (code) {
                var x = code[1];

                $.post('https://accounts.google.com/o/oauth2/token', {
                    code: code[1],
                    client_id: options.client_id,
                    client_secret: options.client_secret,
                    redirect_uri: options.redirect_uri,
                    grant_type: 'authorization_code'
                }).done(function (data) {
                    deferred.resolve(data);
                }).fail(function (response) {
                    console.log(response.responseJSON);
                });
            } else if (error) {
                //The user denied access to the app
                deferred.reject({
                    error: error[1]
                });
            }
        });

        return deferred.promise();
    }
};

function CallService1(pageName, CallType, MethodName, dataVariables, callback) {
       ShowLoader(pageName);
    
    var token = localStorage.getItem('appToken');
    var contentType = "application/json";

    //if (MethodName.indexOf('api/Doctor/ViewUser') > -1 || MethodName.indexOf('api/Comment/DeleteComment') > -1 ||
    //    MethodName.indexOf('api/Doctor/GetNewFacilityDoctors') > -1 || MethodName.indexOf('api/Doctor/ViewInfo') > -1) {
    //    contentType = "application/x-www-form-urlencoded";
    //}

    //if (dataVariables != '' && dataVariables != null) {
    //    dataVariables = JSON.stringify(dataVariables);
    //}

    $.ajax({
        type: CallType,
        url: serviceURL + MethodName,
        headers: {
            "content-type":"application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "authorization": "bearer " + token
        },
        data: dataVariables,
        async: true,
        crossDomain: true,
        success: function (result) {

        },
        error: function (error) {

        }
    }).done(function (result) {
        var res = result;
        if (MethodName != 'api/Comment/GetDoctorComment' && MethodName != 'api/trip/GetUserTrip' && MethodName != 'api/home') {
            HideLoader();
        }
        callback(res);
    })
        .fail(function (error, textStatus) {
            if (MethodName != 'api/Comment/GetDoctorComment' && MethodName != 'api/trip/GetUserTrip' && MethodName != 'api/home') {
                HideLoader();
            }
            var pp = pageName;
            console.log("Error in (" + MethodName + ") , Error text is : [" + error.responseText + "] , Error json is : [" + error.responseJSON + "] .");
            if (typeof error.responseText != 'undefined') {
                var responseText = JSON.parse(error.responseText);

                if (error.status === 401) {
                    //RefreshToken(pageName, CallType, 'Token', function (tokenAfterRefresh) {
                    //    localStorage.setItem('appToken', tokenAfterRefresh);
                    //    CallService(pageName, CallType, MethodName, function (rrrrr) { });
                    //});
                    myApp.alert('مدة صلاحية رمز التحقق الخاص بك قد انتهت من فضلك قم بتسجيل الدخول مرة أخري .', 'خطأ', function () {
                        localStorage.removeItem('appToken');
                        localStorage.removeItem('USName');
                        localStorage.removeItem('refreshToken');
                        localStorage.removeItem('userLoggedIn');
                        localStorage.removeItem('Visitor');
                        //initLoginPage = true;
                        mainView.router.loadPage({ pageName: 'login' });
                    });
                }
                else {

                    if (typeof responseText.error_description != 'undefined') {
                        var error_description = responseText.error_description;
                        if (error_description === 'The user name or password is incorrect.') {
                            myApp.alert('خطأ في البريد الإليكتروني أو كلمة المرور .', 'خطأ', function () { });
                        }
                        else if (error_description === '1-User are Arhieve') {
                            myApp.alert('تمت أرشفة حسابك, من فضلك اتصل بإدارة التطبيق .', 'خطأ', function () { });
                        }
                        else if (error_description === '2-Email Need To Confirm') {
                            myApp.alert('حسابك غير مفعل...من فضلك فعل حسابك من خلال إدخال الكود الخاص ببريدك الإليكتروني .', 'خطأ', function () {
                                localStorage.setItem('UserEntersCode', 'false');
                                mainView.router.loadPage({ pageName: 'userCode' });
                            });
                        }
                        else if (error_description === '3-Wating To Approve') {
                            myApp.alert('يجب تأكيدك من خلال المندوب .', 'خطأ', function () { });
                        }
                        else {
                            myApp.alert('يوجد خطأ في عملية التسجيل .', 'خطأ', function () { });
                        }
                    }
                    else if (typeof responseText.message != 'undefined' && responseText.message != "The request is invalid.") {
                        var message = responseText.message;
                        myApp.alert(message, 'خطأ', function () { });
                    }
                    else if (typeof responseText.modelState != 'undefined') {
                        var messages = responseText.modelState[""];
                        var message = "";
                        if (messages.length > 0) {
                            if (messages[0] === 'Incorrect password.') {
                                myApp.alert('كلمة السر القديمة غير صحيحة .', 'خطأ', function () { });
                            }
                            else {
                                for (var m = 0; m < messages.length; m++) {
                                    message += "- [" + messages[m] + " ] - ";
                                }

                                myApp.alert(message, 'خطأ', function () { });
                            }
                        }
                        else {
                            callback(null);
                        }

                    }
                    else {
                        callback(null);
                    }
                }
            }
            else {
                myApp.alert('خطأ في الخدمة .', 'خطأ', function () { });
                callback(null);
            }

        });
}


function callGoogle() {
    googleapi.authorize({
        client_id: '141775318346-e08ivfs2spg2akh2dajldb6tf5phahja.apps.googleusercontent.com',
        client_secret: 'ocOrhaL19bD2mBVmlZZgFzve',
        redirect_uri: 'http://localhost',
        scope: 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email'
    }).done(function (data) {
        accessToken = data.access_token;
        localStorage.setItem('ACST', accessToken);
        var res = getDataProfile();
        return res;
    });
}

//function getDataProfile() {
//    var term = null;
//    var token = localStorage.getItem('ACST');
//    var accessToken = token;
//    CallService1('login', "POST", "api/Account/RegisterExternal", { "Provider": "Google", "ExternalAccessToken": accessToken }, function (res) {
//        if (res != null) {
//            localStorage.setItem('appToken', res.access_token);        
//            localStorage.setItem('refreshToken', res.refresh_token);
//            localStorage.setItem('userTypeId', res.AccountTypeId);
//            localStorage.setItem('Visitor', false);
//            CallService1('login', "POST", "/api/Account/GetUserInfo/", "", function (res) {
//                if (res != null) {
//                    if (typeof res.responseText != 'undefined' && typeof res.responseText != undefined) {
//                        mainView.router.loadPage({ pageName: 'index' });
//                    } else {
//                        localStorage.setItem('UserId', res.Id);
//                        localStorage.setItem("isSocial", true);
//                        localStorage.setItem('userLoggedIn', JSON.stringify(res));
//                        window.plugins.OneSignal
//                            .startInit(pushAppId)
//                            .handleNotificationOpened(notificationOpenedCallback)
//                            .handleNotificationReceived(notificationReceivedCallback)
//                            .endInit();
//                        window.plugins.OneSignal.getIds(function (ids) {
//                            console.log('getIds: ' + JSON.stringify(ids));
//                            var obj = { "DeviceId": ids.userId, "IsOnline": true };
//                            CallService('', "POST", "/api/NotificationSetting/SaveDeviceSetting/", obj, function (res) {
//                                if (res != null) {
//                                    if (typeof res.responseText != 'undefined' && typeof res.responseText != undefined) {
//                                    } else {
//                                    }
//                                }
//                            });
//                        });
//                        mainView.router.loadPage({ pageName: 'index' });
//                    }
//                }
//            });
//          //  mainView.router.loadPage({ pageName: 'index' });
//        }
//    });
//    disconnectUser();
//}



function getDataProfile() {
    var term = null;
    var token = localStorage.getItem('ACST');

    var accessToken = token;
    var grantUrl = 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=' + accessToken;

    $.ajax({
        type: 'GET',
        url: grantUrl,
        async: false,
        contentType: "application/json",
        dataType: 'jsonp',
        success: function (nullResponse) {
            var response = nullResponse;
            CallService1('login', "POST", "api/Account/RegisterExternal", { "Provider": "Google", "ExternalAccessToken": accessToken, "userId": response.user_id }, function (res) {
                if (res != null) {

                    localStorage.setItem('appToken', res.access_token);
                    localStorage.setItem('refreshToken', res.refresh_token);
                    localStorage.setItem('userTypeId', res.AccountTypeId);
                    localStorage.setItem('Visitor', false);
                    CallService1('login', "POST", "/api/Account/GetUserInfo/", "", function (res) {
                        if (res != null) {
                            if (typeof res.responseText != 'undefined' && typeof res.responseText != undefined) {
                                mainView.router.loadPage({ pageName: 'index' });
                            } else {
                                localStorage.setItem('UserId', res.Id);
                                localStorage.setItem("isSocial", true);
                                localStorage.setItem('userLoggedIn', JSON.stringify(res));
                                window.plugins.OneSignal
                                    .startInit(pushAppId)
                                    .handleNotificationOpened(notificationOpenedCallback)
                                    .handleNotificationReceived(notificationReceivedCallback)
                                    .endInit();
                                window.plugins.OneSignal.getIds(function (ids) {
                                    console.log('getIds: ' + JSON.stringify(ids));

                                    var obj = { "DeviceId": ids.userId, "IsOnline": true };
                                    CallService('', "POST", "/api/NotificationSetting/SaveDeviceSetting/", obj, function (res) {
                                        if (res != null) {
                                            if (typeof res.responseText != 'undefined' && typeof res.responseText != undefined) {
                                            } else {
                                            }

                                        }
                                    });
                                });
                                mainView.router.loadPage({ pageName: 'index' });
                            }
                        }
                    });
                    //  mainView.router.loadPage({ pageName: 'index' });
                }
            });
        },
        error: function (e) {
            //Handle the error
            console.log(e);
        }
    });



    disconnectUser();
}

function disconnectUser() {
    var token = localStorage.getItem('ACST');
    var revokeUrl = 'https://accounts.google.com/o/oauth2/revoke?token=' + token;

    $.ajax({
        type: 'GET',
        url: revokeUrl,
        async: false,
        contentType: "application/json",
        dataType: 'jsonp',
        success: function (nullResponse) {
            accessToken = null;
            //console.log(JSON.stringify(nullResponse));
            //console.log("-----signed out..!!----" + accessToken);
        },
        error: function (e) {
            //Handle the error
            console.log(e);
        }
    });
}