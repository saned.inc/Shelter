﻿var dict = [];
var fieldsAllowedLengths = [];
var fieldsNotRequired = [];
var fieldsCompare = [];
var fieldsFormat = [];
var allFieldsAreCorrect = [];
var fieldsErrors = [];

var FValidation = {
    ValidateAll: function (typeOfUser, callback) {
        dict = [];
        fieldsAllowedLengths = [];
        fieldsCompare = [];
        fieldsFormat = [];
        allFieldsAreCorrect = [];
        fieldsErrors = [];


        FillArrays(typeOfUser, function (arraysAreFilled) {
            if (arraysAreFilled == true) {
                CheckFields(dict, fieldsAllowedLengths, fieldsNotRequired, function (errors) {
                    $('.lblError').css('display', 'none');
                    var allFieldsValid = true;
                    if (errors != null && errors.length > 0) {
                        var allErrors = errors;

                        for (var i = 0; i < dict.length; i++) {
                            var fieldError = allErrors.filter(function (obj) {
                                return obj.field === dict[i].key;
                            })[0];

                            var errorsFound = fieldError.errors.split(',');

                            if (errorsFound.length > 0) {
                                for (var e = 0; e < errorsFound.length; e++) {
                                    var error = errorsFound[e];
                                    if (error != '') {
                                        $('#lbl' + fieldError.field + error).css('display', '');
                                        var fieldCorrect = allFieldsAreCorrect.filter(function (obj) {
                                            return obj.Field === dict[i].key;
                                        })[0];
                                        fieldCorrect.IsCorrect = false;
                                    }
                                    else {
                                        $('#lbl' + fieldError.field + 'Required').css('display', 'none');
                                        $('#lbl' + fieldError.field + 'NumberOnly').css('display', 'none');
                                        $('#lbl' + fieldError.field + 'AllowedLength').css('display', 'none');
                                        $('#lbl' + fieldError.field + 'Format').css('display', 'none');
                                        $('#lbl' + fieldError.field + 'Compare').css('display', 'none');
                                        var fieldCorrect = allFieldsAreCorrect.filter(function (obj) {
                                            return obj.Field === dict[i].key;
                                        })[0];
                                        fieldCorrect.IsCorrect = true;
                                    }
                                }
                            }
                            else {
                                $('#lbl' + fieldError.field + 'Required').css('display', 'none');
                                $('#lbl' + fieldError.field + 'NumberOnly').css('display', 'none');
                                $('#lbl' + fieldError.field + 'AllowedLength').css('display', 'none');
                                $('#lbl' + fieldError.field + 'Format').css('display', 'none');
                                $('#lbl' + fieldError.field + 'Compare').css('display', 'none');
                                var fieldCorrect = allFieldsAreCorrect.filter(function (obj) {
                                    return obj.Field === dict[i].key;
                                })[0];
                                fieldCorrect.IsCorrect = true;
                            }

                        }
                    }

                    for (var fc = 0; fc < allFieldsAreCorrect.length; fc++) {
                        if (allFieldsAreCorrect[fc].IsCorrect == false) {
                            allFieldsValid = false;
                            break;
                        }
                    }
                    callback(allFieldsValid);
                });
            }
        });



    }
};


function CheckFields(fieldsValues, fieldsAllowedLengths, fieldsNotRequired, callbackError) {
    if (fieldsValues.length > 0) {
        for (var i = 0; i < fieldsValues.length; i++) {
            var fieldName = fieldsValues[i].key;
            var fieldValue = fieldsValues[i].value;

            var allFieldErros = fieldsErrors.filter(function (obj) {
                return obj.field === fieldName;
            })[0];
            var fieldRequired = fieldsNotRequired.filter(function (obj) {
                return obj.field === fieldName;
            })[0];
         //   
            if (fieldValue == null || typeof fieldValue == 'undefined' || fieldValue == '') {
                if (typeof fieldRequired == 'undefined') {
                    if (allFieldErros.errors == "") {
                        allFieldErros.errors = "Required";
                    }
                    else {
                        allFieldErros.errors += ",Required";
                    }
                }
                }
            //}
            else {
                var fieldAllowedLength = fieldsAllowedLengths.filter(function (obj) {
                    return obj.field === fieldName;
                });

                if (fieldAllowedLength.length > 0) {
                    if (fieldName == fieldAllowedLength[0].field) {
                        if (fieldAllowedLength[0].ValidationOnCount == true) {
                            if (fieldAllowedLength[0].CheckEquality == true) {
                                if (fieldValue.length > fieldAllowedLength[0].allowedLength) {
                                    if (allFieldErros.errors == "") {
                                        allFieldErros.errors = "AllowedLength";
                                    } else {
                                        allFieldErros.errors += ",AllowedLength";
                                    }
                                }
                            } else {
                                if (fieldValue.length < fieldAllowedLength[0].allowedLength) {
                                    if (allFieldErros.errors == "") {
                                        allFieldErros.errors = "AllowedLength";
                                    } else {
                                        allFieldErros.errors += ",AllowedLength";
                                    }
                                }
                            }
                        }
                        else {
                            if (parseInt(fieldValue) > parseInt(fieldAllowedLength[0].allowedLength)) {
                                if (allFieldErros.errors == "") {
                                    allFieldErros.errors = "AllowedLength";
                                }
                                else {
                                    allFieldErros.errors += ",AllowedLength";
                                }
                            }
                        }

                    }

                    if (isNaN(fieldValue) || !isFinite(fieldValue)) {
                        if (allFieldErros.errors == "") {
                            allFieldErros.errors = "NumberOnly";
                        }
                        else {
                            allFieldErros.errors += ",NumberOnly";
                        }
                    }

                }

                var fieldFormat = fieldsFormat.filter(function (obj) {
                    return obj.field === fieldName;
                });

                if (fieldFormat.length > 0) {
                    var re = '';
                    if (fieldFormat[0].type == 'Email') {
                        re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    }
                    if (!re.test(fieldValue)) {
                        if (allFieldErros.errors == "") {
                            allFieldErros.errors = "Format";
                        }
                        else {
                            allFieldErros.errors += ",Format";
                        }
                    }
                }

                if (fieldName == 'ConfirmPassword'  ) {
                    var fieldToCompare = fieldsValues.filter(function (obj) {
                        return obj.key === 'Password';
                    })[0].value;

                    var fieldToCompareWith = fieldsValues.filter(function (obj) {
                        return obj.key === 'ConfirmPassword';
                    })[0].value;

                    if (fieldToCompare != fieldToCompareWith) {
                        if (allFieldErros.errors == "") {
                            allFieldErros.errors = "Compare";
                        }
                        else {
                            allFieldErros.errors += ",Compare";
                        }
                    }
                }
                else if (fieldName == 'ChangeConfirmNewPassword') {
                    var fieldToCompare = fieldsValues.filter(function (obj) {
                        return obj.key === 'ChangeNewPassword';
                    })[0].value;

                    var fieldToCompareWith = fieldsValues.filter(function (obj) {
                        return obj.key === 'ChangeConfirmNewPassword';
                    })[0].value;

                    if (fieldToCompare != fieldToCompareWith) {
                        if (allFieldErros.errors == "") {
                            allFieldErros.errors = "Compare";
                        }
                        else {
                            allFieldErros.errors += ",Compare";
                        }
                    }
                }
                else if (fieldName == 'resetConfirmPassword') {
                    var fieldToCompare = fieldsValues.filter(function (obj) {
                        return obj.key === 'resetPassword';
                    })[0].value;

                    var fieldToCompareWith = fieldsValues.filter(function (obj) {
                        return obj.key === 'resetConfirmPassword';
                    })[0].value;

                    if (fieldToCompare != fieldToCompareWith) {
                        if (allFieldErros.errors == "") {
                            allFieldErros.errors = "Compare";
                        }
                        else {
                            allFieldErros.errors += ",Compare";
                        }
                    }
                }

               

            }
        }

        callbackError(fieldsErrors);
    }

}

function FillArrays(userType, arraysAreFilled) {
    if (userType == 'User') {
        allFieldsAreCorrect = [
        { Field: "Name", IsCorrect: false },
        { Field: "UserName", IsCorrect: false },
        { Field: "Mobile", IsCorrect: false },
        { Field: "Email", IsCorrect: false },
        { Field: "City", IsCorrect: false },
        { Field: "UserAge", IsCorrect: false },
        { Field: "Password", IsCorrect: false },
        { Field: "ConfirmPassword", IsCorrect: false }
        ];

        fieldsErrors.push(
            { field: "Name", errors: "" },
            { field: "UserName", errors: "" },
            { field: "Mobile", errors: "" },
            { field: "Email", errors: "" },
            { field: "City", errors: "" },
            { field: "UserAge", errors: "" },
            { field: "Password", errors: "" },
            { field: "ConfirmPassword", errors: "" });

        dict.push(
            { key: "Name", value: $('#txtName').val() },
            { key: "Mobile", value: $('#txtMobile').val() },
            { key: "Email", value: $('#txtEmail').val() },
            { key: "Password", value: $('#txtPassword').val() },
            { key: "ConfirmPassword", value: $('#txtConfirmPassword').val() },
            { key: "City", value: $('#CitiesDll option:selected').val() },
            { key: "UserName", value: $('#txtUserName').val() },
             { key: "UserAge", value: $('#txtUserAge').val() });

        fieldsAllowedLengths.push(
            { field: "Mobile", allowedLength: 10, ValidationOnCount: true, CheckEquality: true});
        fieldsAllowedLengths.push(
        { field: "UserAge", allowedLength: 3, ValidationOnCount: true, CheckEquality: true });
        fieldsAllowedLengths.push(
            { field: "Password", allowedLength: 6, ValidationOnCount: true, CheckEquality: false });

        fieldsCompare.push(
            { fieldToCompare: "Password", fieldToCompareWith: "ConfirmPassword" });

        fieldsFormat.push(
            { field: "Email", type: 'Email' });

        arraysAreFilled(true);
    }
    else if (userType == 'AddAnimal') {
        allFieldsAreCorrect = [
        { Field: "AnimalType", IsCorrect: false },
        { Field: "Age", IsCorrect: false },
        { Field: "AnimalCity", IsCorrect: false },
        { Field: "AnimalGender", IsCorrect: false },
        //{ Field: "AnimalName", IsCorrect: false },
        { Field: "AnimalAddress", IsCorrect: false }];
        
        fieldsErrors.push(
            { field: "AnimalType", errors: "" },
            { field: "Age", errors: "" },
            { field: "AnimalCity", errors: "" },
            { field: "AnimalGender", errors: "" },
         //   { field: "AnimalName", errors: "" },
            { field: "AnimalAddress", errors: "" });

        dict.push(
            { key: "AnimalType", value: $('#ddlAnimalType').val() },
            { key: "Age", value: $('#txtAnimalAge').val() },
            { key: "AnimalCity", value: $('#ddlAnimalCity').val() },
            { key: "AnimalGender", value: $('#ddlAnimalGender').val() },
           // { key: "AnimalName", value: $('#txtAnimalName').val() },
            { key: "AnimalAddress", value: $('#divAnimalAddress').val() });
        fieldsNotRequired.push({field: "Age", NotRequired: true });
        fieldsAllowedLengths.push(
         { field: "Age", allowedLength: 3, ValidationOnCount: true, CheckEquality: true});
           

        
        arraysAreFilled(true);
    }
    else if (userType == 'LeaveAnimal') {
        allFieldsAreCorrect = [
        { Field: "LeaveAnimalType", IsCorrect: false },
        { Field: "LeaveAnimalCity", IsCorrect: false },
        { Field: "LeaveAnimalGender", IsCorrect: false },
      //  { Field: "LeaveAnimalName", IsCorrect: false },
        { Field: "LeaveAnimalAge", IsCorrect: false },
        { Field: "LeaveAnimalLocation", IsCorrect: false },
        { Field: "leaveFor", IsCorrect: false }];

        fieldsErrors.push(
            { field: "LeaveAnimalType", errors: "" },
            { field: "LeaveAnimalCity", errors: "" },
            { field: "LeaveAnimalGender", errors: "" },
         //   { field: "LeaveAnimalName", errors: "" },
            { field: "LeaveAnimalAge", errors: "" },
            { field: "LeaveAnimalLocation", errors: "" },
            { field: "leaveFor", errors: "" });

        dict.push(
            { key: "LeaveAnimalType",     value: $('#ddlLeaveAnimalType').val() },
            { key: "LeaveAnimalCity",     value: $('#ddlLeaveAnimalCity').val() },
            { key: "LeaveAnimalGender",   value: $('#ddlLeaveAnimalGender').val() },
         //   { key: "LeaveAnimalName",     value: $('#txtLeaveAnimalName').val() },
            { key: "LeaveAnimalAge",      value: $('#txtLeaveAnimalAge').val() },
            { key: "LeaveAnimalLocation", value: $('#divLeaveAnimalAddress').val() },
            { key: "leaveFor", value: $('#DDLleaveFor').val() });

        fieldsNotRequired.push({ field: "LeaveAnimalAge", NotRequired: true });
        fieldsAllowedLengths.push(
           { field: "LeaveAnimalAge", allowedLength: 3, ValidationOnCount: true, CheckEquality: true });


        arraysAreFilled(true);
    }
    else if (userType == 'changePassword') {
        allFieldsAreCorrect = [
           { Field: "ChangeOldPassword", IsCorrect: false },
           { Field: "ChangeNewPassword", IsCorrect: false },
           { Field: "ChangeConfirmNewPassword", IsCorrect: false }];

        fieldsErrors.push(
            { field: "ChangeOldPassword", errors: "" },
            { field: "ChangeNewPassword", errors: "" },
            { field: "ChangeConfirmNewPassword", errors: "" });

        dict.push(
            { key: "ChangeOldPassword", value: $('#txtChangeOldPassword').val() },
            { key: "ChangeNewPassword", value: $('#txtChangeNewPassword').val() },
            { key: "ChangeConfirmNewPassword", value: $('#txtChangeConfirmNewPassword').val() });

        fieldsAllowedLengths.push(
            { field: "ChangeNewPassword", allowedLength: 6, ValidationOnCount: true, CheckEquality: false });

        fieldsCompare.push(
            { fieldToCompare: "ChangeNewPassword", fieldToCompareWith: "ChangeConfirmNewPassword" });

        //fieldsFormat.push(
        //    { field: "Email", type: 'Email' });

        arraysAreFilled(true);
    }
    else if (userType == 'resetPassword') {
        allFieldsAreCorrect = [
           { Field: "resetCode", IsCorrect: false },
           { Field: "resetPassword", IsCorrect: false },
           { Field: "resetConfirmPassword", IsCorrect: false }];

        fieldsErrors.push(
            { field: "resetCode", errors: "" },
            { field: "resetPassword", errors: "" },
            { field: "resetConfirmPassword", errors: "" });

        dict.push(
            { key: "resetCode", value: $('#txtResetCode').val() },
            { key: "resetPassword", value: $('#txtResetPassword').val() },
            { key: "resetConfirmPassword", value: $('#txtResetConfirmPassword').val() });

        fieldsAllowedLengths.push(
            { field: "resetPassword", allowedLength: 6, ValidationOnCount: true, CheckEquality: false });

        fieldsCompare.push(
            { fieldToCompare: "resetPassword", fieldToCompareWith: "resetConfirmPassword" });

       

        arraysAreFilled(true);
    }
    else if (userType == 'adoptedRequest') {
       allFieldsAreCorrect = [
           { Field: "adoptedRequestFullName", IsCorrect: false },
           { Field: "adoptedRequestAddress", IsCorrect: false },
           { Field: "adoptedRequestMobile", IsCorrect: false },
           { Field: "adoptedRequestAge", IsCorrect: false },
           { Field: "adoptedRequestEmail", IsCorrect: false },
           { Field: "adoptedRequestSocialStatus", IsCorrect: false },
           { Field: "adoptedRequestQuestion1", IsCorrect: false },
           { Field: "adoptedRequestQuestion2", IsCorrect: false },
           { Field: "adoptedRequestQuestion6", IsCorrect: false },
           { Field: "adoptedRequestQuestion8", IsCorrect: false },
           { Field: "adoptedRequestQuestion10", IsCorrect: false },
           { Field: "adoptedRequestQuestion11", IsCorrect: false },
           { Field: "adoptedRequestQuestion12", IsCorrect: false },
           { Field: "adoptedRequestQuestion25", IsCorrect: false },
           { Field: "adoptedRequestQuestion27", IsCorrect: false }
       ];

        fieldsErrors.push(
            { field: "adoptedRequestFullName", errors: "" },
            { field: "adoptedRequestAddress", errors: "" },
            { field: "adoptedRequestMobile", errors: "" },
            { field: "adoptedRequestAge", errors: "" },
            { field: "adoptedRequestEmail", errors: "" },
            { field: "adoptedRequestSocialStatus", errors: "" },
            { field: "adoptedRequestQuestion1", errors: "" },
            { field: "adoptedRequestQuestion2", errors: "" },
            //{ field: "adoptedRequestQuestion3", errors: "" },
            //{ field: "adoptedRequestQuestion4", errors: "" },
            //{ field: "adoptedRequestQuestion5", errors: "" },
            { field: "adoptedRequestQuestion6", errors: "" },
            //{ field: "adoptedRequestQuestion7", errors: "" },
            { field: "adoptedRequestQuestion8", errors: "" },
            //{ field: "adoptedRequestQuestion9", errors: "" },
            { field: "adoptedRequestQuestion10", errors: "" },
            { field: "adoptedRequestQuestion11", errors: "" },
            { field: "adoptedRequestQuestion12", errors: "" },
            //{ field: "adoptedRequestQuestion13", errors: "" },
            //{ field: "adoptedRequestQuestion21", errors: "" },
            //{ field: "adoptedRequestQuestion22", errors: "" },
            //{ field: "adoptedRequestQuestion23", errors: "" },
            //{ field: "adoptedRequestQuestion24", errors: "" },
            { field: "adoptedRequestQuestion25", errors: "" },
            //{ field: "adoptedRequestQuestion26", errors: "" },
            { field: "adoptedRequestQuestion27", errors: "" });

        dict.push(
            { key: "adoptedRequestFullName", value: $('#adoptedRequestFullName').val() },
            { key: "adoptedRequestAddress", value: $('#adoptedRequestAddress').val() },
            { key: "adoptedRequestMobile", value: $('#adoptedRequestMobile').val() },
            { key: "adoptedRequestAge", value: $('#adoptedRequestAge').val() },
            { key: "adoptedRequestEmail", value: $('#adoptedRequestEmail').val() },
            { key: "adoptedRequestSocialStatus", value: $('#adoptedRequestSocialStatus').val() },
            { key: "adoptedRequestQuestion1", value: $('#adoptedRequestQuestion1').val() },
            { key: "adoptedRequestQuestion2", value: $('#adoptedRequestQuestion2').val() },
            //{ key: "adoptedRequestQuestion3", value: $('#adoptedRequestQuestion3').val() },
            //{ key: "adoptedRequestQuestion4", value: $('#adoptedRequestQuestion4').val() },
            //{ key: "adoptedRequestQuestion5", value: $('#adoptedRequestQuestion5').val() },
            { key: "adoptedRequestQuestion6", value: $('#adoptedRequestQuestion6').val() },
            //{ key: "adoptedRequestQuestion7", value: $('#adoptedRequestQuestion7').val() },
            { key: "adoptedRequestQuestion8", value: $('#adoptedRequestQuestion8').val() },
            //{ key: "adoptedRequestQuestion9", value: $('#adoptedRequestQuestion9').val() },
            { key: "adoptedRequestQuestion10", value: $('#adoptedRequestQuestion10').val() },
            { key: "adoptedRequestQuestion11", value: $('#adoptedRequestQuestion11').val() },
            { key: "adoptedRequestQuestion12", value: $('#adoptedRequestQuestion12').val() },
            //{ key: "adoptedRequestQuestion13", value: $('#adoptedRequestQuestion13').val() },
            //{ key: "adoptedRequestQuestion21", value: $('#adoptedRequestQuestion21').val() },
            //{ key: "adoptedRequestQuestion22", value: $('#adoptedRequestQuestion22').val() },
            //{ key: "adoptedRequestQuestion23", value: $('#adoptedRequestQuestion23').val() },
            //{ key: "adoptedRequestQuestion24", value: $('#adoptedRequestQuestion24').val() },
            { key: "adoptedRequestQuestion25", value: $('#adoptedRequestQuestion25').val() },
            //{ key: "adoptedRequestQuestion26", value: $('#adoptedRequestQuestion26').val() },
            { key: "adoptedRequestQuestion27", value: $('#adoptedRequestQuestion27').val() });

           fieldsAllowedLengths.push({ field: "adoptedRequestMobile", allowedLength: 10, ValidationOnCount: true, CheckEquality: true, NotRequired: true });
           fieldsAllowedLengths.push( { field: "adoptedRequestAge", allowedLength: 3, ValidationOnCount: true, CheckEquality: true });
           fieldsFormat.push({ field: "adoptedRequestEmail", type: 'Email' });
        arraysAreFilled(true);
    }
    else if (userType == 'addSuccessStory') {
        allFieldsAreCorrect = [
            { Field: "addSuccessStoryTitle", IsCorrect: false },
            { Field: "addSuccessStoryDetails", IsCorrect: false }
        ];
     
        fieldsErrors.push(
            { field: "addSuccessStoryTitle", errors: "" },
            { field: "addSuccessStoryDetails", errors: "" });

        dict.push(
            { key: "addSuccessStoryTitle", value: $('#txt_SucessTitle').val() },
            { key: "addSuccessStoryDetails", value: $('#txt_SucessDetails').val() });




        arraysAreFilled(true);
    }
    else if (userType == 'EditPRofile') {
        allFieldsAreCorrect = [
           { Field: "txtEditUserName", IsCorrect: false },
           { Field: "EditUserCitiesDll", IsCorrect: false },
           { Field: "EditUserGender", IsCorrect: false },
           { Field: "EditUserAge", IsCorrect: false },
           { Field: "TxtEditUserMobile", IsCorrect: false }];

        fieldsErrors.push(
            { field: "txtEditUserName", errors: "" },
            { field: "EditUserCitiesDll", errors: "" },
            { field: "EditUserGender", errors: "" },
            { field: "EditUserAge", errors: "" },
            { field: "TxtEditUserMobile", errors: "" });

        dict.push(
            { key: "txtEditUserName", value: $('#txtEditUserName').val() },
            { key: "EditUserCitiesDll", value: $('#EditUserCitiesDll').val() },
            { key: "EditUserGender", value: $('#EditUserGender').val() },
            { key: "EditUserAge", value: $('#txtEditUserAge').val() }
            , { key: "TxtEditUserMobile", value: $('#TxtEditUserMobile').val() });

        fieldsAllowedLengths.push(
     { field: "EditUserAge", allowedLength: 3, ValidationOnCount: true, CheckEquality: true });
        fieldsAllowedLengths.push(
            { field: "TxtEditUserMobile", allowedLength: 10, ValidationOnCount: true, CheckEquality: true, NotRequired: true });
        arraysAreFilled(true);
    }
    else if(userType === 'ReportPopup')
    {
        allFieldsAreCorrect = [
           { Field: "ReportDescription", IsCorrect: false }];

        fieldsErrors.push(
            { field: "ReportDescription", errors: "" });

        dict.push(
            { key: "ReportDescription", value: $('#ReportDescription').val() });


        arraysAreFilled(true);
    }
    else {
        arraysAreFilled(false);
    }
}

function parseArabic(str) {
    if (str.trim() !== "") {
     var yas = str;
    yas = yas.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function (d) {
        return d.charCodeAt(0) - 1632;
    }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function (d) {
        return d.charCodeAt(0) - 1776;
    });

    str = yas;

    return str;
    }
    return str;
}